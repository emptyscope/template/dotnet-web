using System.Globalization;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.LogControl;
using Serilog;
using Serilog.Core;
using Serilog.Enrichers.Sensitive;
using Serilog.Events;
using Serilog.Sinks.MicrosoftTeams.Alternative;
using ILogger = Serilog.ILogger;

namespace DotnetWeb.WebApi;

internal static class LoggerSetup
{
    internal static ILogger Initialize(string logLocation, IConfiguration? configuration = null)
    {
        if (configuration != null && !string.IsNullOrWhiteSpace(logLocation))
        {
            return Log.Logger = CreateDefaultLogger(logLocation, configuration);
        }

        return Log.Logger = CreateDefaultLogger(logLocation, configuration);
    }

    internal static ILoggingBuilder Configure(ILoggingBuilder loggingBuilder, IConfiguration configuration) => loggingBuilder.AddSerilog(CreateDefaultLogger(configuration));

    // ReSharper disable once UnusedMember.Local
    private static Logger CreateDefaultLogger(IConfiguration configuration)
    {
        var defaultLogLocation = new LogLocationUtility(new EnvironmentVariableUtility())
            .Fetch(DotnetWebConstants.DefaultLogLocation);

        return CreateDefaultLogger(defaultLogLocation, configuration);
    }

    // ReSharper disable once UnusedParameter.Local
    private static Logger CreateDefaultLogger(string logLocation, IConfiguration? configuration = null)
    {
        if (!ValidLogLocation(logLocation))
        {
            throw new ArgumentException(logLocation);
        }

        return new LoggerConfiguration()
            .MinimumLevel.Debug()
            .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            .MinimumLevel.Override("System", LogEventLevel.Warning)
            .MinimumLevel.Override("Worker", LogEventLevel.Warning)
            .MinimumLevel.Override("Host", LogEventLevel.Warning)
            .MinimumLevel.Override("Function", LogEventLevel.Warning)
            .MinimumLevel.Override("Azure", LogEventLevel.Warning)
            .MinimumLevel.Override("DurableTask", LogEventLevel.Warning)
            .Enrich.FromLogContext()
            .Enrich.WithProperty("AppVersion", "")
            .Enrich.WithProperty("OperatingSystem", "")
            .Enrich.WithClientIp()
            .Enrich.WithCorrelationId()
            .Enrich.WithRequestHeader("X-Client-OperationId")
            .Enrich.WithSensitiveDataMasking(options =>
                                             {
                                                 options.MaskValue = "***MASKED***";
                                                 options.MaskingOperators = [];
                                                 options.Mode = MaskingMode.Globally;
                                                 options.MaskProperties = [];
                                                 options.Operators = [];
                                                 options.ExcludeProperties = [];
                                             })
            .WriteTo.Console(formatProvider: CultureInfo.InvariantCulture)
            // Add Sentry integration with Serilog
            // Two levels are used to configure it.
            // One sets which log level is minimally required to keep a log message as breadcrumbs
            // The other sets the minimum level for messages to be sent out as events to Sentry
            .WriteTo.Sentry(s =>
                            {
                                s.Dsn =
                                    "https://glet_681d9856ff8ff96c8b6ef4d7bc701e77@observe.gitlab.com:443/errortracking/api/v1/projects/38927327";
                                s.MinimumEventLevel = LogEventLevel.Warning;
                                s.AttachStacktrace = true;
                                s.Debug = true;
                                s.DiagnosticLevel = SentryLevel.Error;
                                s.TracesSampleRate = 1.0;
                                s.MinimumBreadcrumbLevel = LogEventLevel.Debug;
                                s.MinimumEventLevel = LogEventLevel.Error;
                            })
            .WriteTo.MicrosoftTeams(
                $"https://teamsname.webhook.office.com/webhookb2/guid@guid/IncomingWebhook/guid/guid",
                "Teams WebApi",
                formatProvider: CultureInfo.InvariantCulture,
                buttons:
                         [
                             new MicrosoftTeamsSinkOptionsButton
                             {
                                 Name = "View Log",
                                 Uri = "https://myUrl"
                             }
                         ])
            .WriteTo.File(
                logLocation,
                fileSizeLimitBytes: 1_000_000,
                rollOnFileSizeLimit: true,
                shared: true,
                rollingInterval: RollingInterval.Day,
                formatProvider: CultureInfo.InvariantCulture)
            .CreateLogger();
    }

    private static bool ValidLogLocation(string logLocation) =>
        NoExceptions(!ValidateLogLocationExists(logLocation)
            ? TestFileLocation(logLocation)
            : TestFileLocation(FetchTempFile(logLocation)));

    private static bool NoExceptions(UnauthorizedAccessException? logException)
    {
        if (logException != null)
        {
            throw logException;
        }

        return true;
    }

    private static UnauthorizedAccessException? TestFileLocation(string logLocation)
    {
        var canWriteLog = ValidateCanWriteLogLocation(logLocation);

        if (!canWriteLog)
        {
            return new UnauthorizedAccessException($"Log location {logLocation} is not writable");
        }

        var canDeleteLog = ValidateCanDeleteLogLocation(logLocation);

        return !canDeleteLog ? new UnauthorizedAccessException($"Log location {logLocation} is not deletable") : null;
    }

    private static bool ValidateCanDeleteLogLocation(string tempFile)
    {
        try
        {
            File.Delete(tempFile);
            return true;
        }
        catch
        {
            return false;
        }
    }

    private static bool ValidateCanWriteLogLocation(string tempFileLocation)
    {
        var directoryLogLocation = ValidateDirectoryLogLocation(tempFileLocation);

        if (!directoryLogLocation)
        {
            return false;
        }

        try
        {
            using (var fileStream = File.Create(Path.GetFullPath(tempFileLocation)))
            {
                return fileStream.CanWrite;
            }
        }
        catch
        {
            return false;
        }
    }

    private static bool ValidateDirectoryLogLocation(string tempFileLocation)
    {
        try
        {
            var fullPath = Path.GetFullPath(tempFileLocation);
            var directory = Directory.CreateDirectory(FetchDirectoryPath(fullPath));

            return directory.Exists;
        }
        catch
        {
            return false;
        }
    }

    private static string FetchTempFile(string logLocation) => Path.Combine(FetchDirectoryPath(logLocation), $"test_write_{DateTime.UtcNow.Ticks}.log");

    private static bool ValidateLogLocationExists(string logLocation) => Path.Exists(logLocation) || Path.Exists(FetchDirectoryPath(logLocation));

    private static string FetchDirectoryPath(string logLocation) => logLocation.Replace(Path.GetFileName(logLocation), string.Empty);
}
