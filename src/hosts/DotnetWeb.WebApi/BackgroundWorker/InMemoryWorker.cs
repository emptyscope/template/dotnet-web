using DotnetWeb.Common;
using MassTransit;

namespace DotnetWeb.WebApi.BackgroundWorker;

public class InMemoryWorker(IBus bus) : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await bus.Publish(new GettingStarted { Value = $"The time is {DateTimeOffset.UtcNow}" }, stoppingToken).ConfigureAwait(false);

            await Task.Delay(1000, stoppingToken).ConfigureAwait(false);
        }
    }
}
