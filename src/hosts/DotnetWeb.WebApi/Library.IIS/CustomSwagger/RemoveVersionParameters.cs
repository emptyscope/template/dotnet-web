using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class RemoveVersionParameters : IOperationFilter
{
    public void Apply(OpenApiOperation? operation, OperationFilterContext context)
    {
        if (operation?.Parameters == null || !operation.Parameters.Any())
        {
            return;
        }

        var hasVersion =
            operation.Parameters.FirstOrDefault(i => "apiVersion".Equals(i.Name, StringComparison.OrdinalIgnoreCase));

        if (hasVersion != null)
        {
            operation.Parameters.Remove(hasVersion);
        }
    }
}
