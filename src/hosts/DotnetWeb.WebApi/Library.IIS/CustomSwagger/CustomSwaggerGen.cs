using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class CustomSwaggerGen(string? applicationName, string? environmentName, string? authenticationScheme = null)
{
    private readonly string _applicationName = applicationName ?? string.Empty;
    private readonly string _environmentName = environmentName ?? string.Empty;

    public Action<SwaggerGenOptions> Get(IEnumerable<Tuple<int, int, int, string?>> currentProjectApiList) => options =>
                                                                                                                          {
                                                                                                                              foreach (var apiVersion in currentProjectApiList)
                                                                                                                              {
                                                                                                                                  options.SwaggerDoc($"v{apiVersion.Item1}", GetOpenApiDocument(apiVersion));
                                                                                                                                  options.IgnoreObsoleteActions();
                                                                                                                                  options.IgnoreObsoleteProperties();
                                                                                                                                  // options.UseOneOfForPolymorphism();
                                                                                                                                  options.UseAllOfToExtendReferenceSchemas();
                                                                                                                              }

                                                                                                                              ConfigureAuthentication(options);

                                                                                                                              options.ResolveConflictingActions(apiDescriptions =>
                                                                                                                                                                {
                                                                                                                                                                    var descriptions = apiDescriptions.ToList();

                                                                                                                                                                    var latest = GetCurrentApiDescription(descriptions);

                                                                                                                                                                    var parameters = descriptions
                                                                                                                                                                        .Where(d => d?.ParameterDescriptions != null)
                                                                                                                                                                        .SelectMany(d => d.ParameterDescriptions)
                                                                                                                                                                        .ToList();

                                                                                                                                                                    if (latest?.ParameterDescriptions != null &&
                                                                                                                                                                        latest.ParameterDescriptions.Any())
                                                                                                                                                                    {
                                                                                                                                                                        latest.ParameterDescriptions.Clear();
                                                                                                                                                                    }

                                                                                                                                                                    foreach (var parameter in parameters)
                                                                                                                                                                    {
                                                                                                                                                                        if (latest == null)
                                                                                                                                                                        {
                                                                                                                                                                            continue;
                                                                                                                                                                        }

                                                                                                                                                                        var alreadyExists = latest.ParameterDescriptions
                                                                                                                                                                            .Where(d => !string.IsNullOrWhiteSpace(d.Name))
                                                                                                                                                                            .Any(x => x.Name.Equals(parameter.Name,
                                                                                                                                                                                     StringComparison.OrdinalIgnoreCase));

                                                                                                                                                                        if (alreadyExists)
                                                                                                                                                                        {
                                                                                                                                                                            continue;
                                                                                                                                                                        }

                                                                                                                                                                        latest.ParameterDescriptions.Add(
                                                                                                                                                                            new ApiParameterDescription
                                                                                                                                                                            {
                                                                                                                                                                                ModelMetadata = parameter.ModelMetadata,
                                                                                                                                                                                Name = parameter.Name,
                                                                                                                                                                                ParameterDescriptor =
                                                                                                                                                                                    parameter.ParameterDescriptor,
                                                                                                                                                                                Source = parameter.Source,
                                                                                                                                                                                IsRequired = false,
                                                                                                                                                                                DefaultValue = null
                                                                                                                                                                            });
                                                                                                                                                                    }

                                                                                                                                                                    return latest;
                                                                                                                                                                });

                                                                                                                              options.DocInclusionPredicate((_, _) => true);
                                                                                                                              options.OperationFilter<FormDataOperationFilter>();
                                                                                                                              options.OperationFilter<AuthorizationOperationFilter>();
                                                                                                                              options.OperationFilter<ResponseStatusOperationFilter>();
                                                                                                                              options.DocumentFilter<SetVersionInPaths>();
                                                                                                                              options.OperationFilter<RemoveVersionParameters>();
                                                                                                                              options.DocumentFilter<FilterVersionsInPaths>();

                                                                                                                              var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml".Replace("WebApi", "Route");
                                                                                                                              var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                                                                                                                              options.IncludeXmlComments(xmlPath, true);
                                                                                                                          };

    private void ConfigureAuthentication(SwaggerGenOptions options)
    {
        if (string.IsNullOrWhiteSpace(authenticationScheme))
        {
            return;
        }

        switch (authenticationScheme)
        {
            case "Basic-Authentication":
                options.AddSecurityDefinition("Basic-Authentication", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    Scheme = "basic",
                    Description =
                                                                              "Input your username and password used in the Authorization header",
                    Name = "Authorization",
                    In = ParameterLocation.Header
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                                               {
                                                   {
                                                       new OpenApiSecurityScheme
                                                       {
                                                           Reference = new OpenApiReference
                                                                       {
                                                                           Type=ReferenceType.SecurityScheme,
                                                                           Id="Basic-Authentication"
                                                                       }
                                                       },
                                                       Array.Empty<string>()
                                                   }
                                               });
                break;
            case "Bearer-Authentication":
                options.AddSecurityDefinition("Bearer-Authentication",
                    new OpenApiSecurityScheme
                    {
                        Type = SecuritySchemeType.Http,
                        Scheme = "bearer",
                        BearerFormat = "JWT",
                        Description = "Input your token used in the Authorization header",
                        Name = "Authorization",
                        In = ParameterLocation.Header
                    });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                                               {
                                                   {
                                                       new OpenApiSecurityScheme
                                                       {
                                                           Reference = new OpenApiReference
                                                                       {
                                                                           Type=ReferenceType.SecurityScheme,
                                                                           Id="Bearer-Authentication"
                                                                       }
                                                       },
                                                       Array.Empty<string>()
                                                   }
                                               });
                break;
            case "ApiKey-Authentication":
                options.AddSecurityDefinition("ApiKey-Authentication", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.ApiKey,
                    Description =
                                                                               "Input your API key used in the Authorization header",
                    Name = "Authorization",
                    In = ParameterLocation.Header
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                                               {
                                                   {
                                                       new OpenApiSecurityScheme
                                                       {
                                                           Reference = new OpenApiReference
                                                                       {
                                                                           Type=ReferenceType.SecurityScheme,
                                                                           Id="ApiKey-Authentication"
                                                                       }
                                                       },
                                                       Array.Empty<string>()
                                                   }
                                               });
                break;
            default:
                break;
        }
    }

    private static ApiDescription? GetCurrentApiDescription(List<ApiDescription> descriptions)
    {
        var filtered = descriptions
            .Where(d => d.GetApiVersion() != null)
            .Where(d => d.GetApiVersion()?.MajorVersion != null)
            .Where(d => d.GetApiVersion()!.MajorVersion.HasValue)
            .ToList();

        var latest = filtered
            .LastOrDefault(d => "latest".Equals(d.GetApiVersion()?.Status, StringComparison.Ordinal));

        if (latest != null)
        {
            return latest;
        }

        var currentMajorVersion = GetCurrentMajorVersion(filtered);

        return filtered
            .Where(d => currentMajorVersion.Equals(d.GetApiVersion()!.MajorVersion!.Value))
            .MaxBy(d => d.GetApiVersion()?.MinorVersion);
    }

    private static int GetCurrentMajorVersion(List<ApiDescription> descriptions)
    {
        var currentMajor = descriptions
            .Where(d => d.GetApiVersion()?.MajorVersion != null)
            .MaxBy(d => d.GetApiVersion()!.MajorVersion);

        return currentMajor != null ? currentMajor.GetApiVersion()!.MajorVersion!.Value : 0;
    }

    private OpenApiInfo GetOpenApiDocument(Tuple<int, int, int, string?> apiVersion) =>
        new()
        {
            Version = $"v{apiVersion.Item1}.{apiVersion.Item2}",
            Title = _applicationName + " " + _environmentName + " " + $"v{apiVersion.Item1}.{apiVersion.Item2}"
        };
}
