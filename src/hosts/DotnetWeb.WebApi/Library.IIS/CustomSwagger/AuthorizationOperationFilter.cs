using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class AuthorizationOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation? operation, OperationFilterContext? context)
    {
        if (operation == null)
        {
            return;
        }

        operation.Parameters ??= [];

        if (context?.MethodInfo == null)
        {
            return;
        }

        var endpointsWithAuthorization = context.MethodInfo
            .GetCustomAttributes(true)
            .OfType<AuthorizeAttribute>()
            .Distinct();

        operation.Responses ??= [];

        if (!endpointsWithAuthorization.Any())
        {
            return;
        }

        if (!operation.Responses.ContainsKey("401"))
        {
            operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
        }

        if (!operation.Responses.ContainsKey("403"))
        {
            operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden" });
        }

        operation.Security ??= [];

        var basicAuthScheme = new OpenApiSecurityScheme
        {
            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Basic-Authentication" }
        };

        operation.Security = [new() { [basicAuthScheme] = [] }];
    }
}
