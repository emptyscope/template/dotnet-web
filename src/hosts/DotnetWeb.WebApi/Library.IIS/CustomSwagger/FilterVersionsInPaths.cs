using DotnetWeb.Common;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class FilterVersionsInPaths : IDocumentFilter
{
    public void Apply(OpenApiDocument? swaggerDoc, DocumentFilterContext context)
    {
        if (swaggerDoc?.Paths == null || string.IsNullOrWhiteSpace(swaggerDoc.Info?.Version))
        {
            return;
        }

        var paths = new OpenApiPaths();
        foreach (var openApiPath in swaggerDoc.Paths.Where(openApiPath => !string.IsNullOrWhiteSpace(openApiPath.Key)))
        {
            var currentVersion = DotnetWebConstants.ApiList()
                .FirstOrDefault(i => openApiPath.Key.Contains($"v{i}", StringComparison.InvariantCultureIgnoreCase));

            if (currentVersion == null)
            {
                paths.Add(openApiPath.Key.Replace("v{apiVersion}", $"v{currentVersion}"), openApiPath.Value);
            }
            else
            {
                var currentContextVersion =
                    context.ApiDescriptions
                        .FirstOrDefault(i => !string.IsNullOrWhiteSpace(i.RelativePath) &&
                                            i.RelativePath.Contains($"v{currentVersion}", StringComparison.InvariantCultureIgnoreCase))
                        ?.RelativePath;

                if (!string.IsNullOrWhiteSpace(currentContextVersion))
                {
                    paths.Add(openApiPath.Key.Replace("v{apiVersion}", $"v{currentVersion}"), openApiPath.Value);
                }
            }
        }

        swaggerDoc.Paths = paths;
    }
}
