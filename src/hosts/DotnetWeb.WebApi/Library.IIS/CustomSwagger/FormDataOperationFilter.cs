using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class FormDataOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation? operation, OperationFilterContext? context)
    {
        if (operation == null)
        {
            return;
        }

        operation.Parameters ??= [];

        if (context?.MethodInfo == null)
        {
            return;
        }

        var formData = context.MethodInfo
            .GetCustomAttributes(true)
            .OfType<ConsumesAttribute>()
            .SelectMany(attr => attr.ContentTypes)
            .Where(mediaType => mediaType is "application/x-www-form-urlencoded" or "multipart/form-data")
            .Distinct()
            .FirstOrDefault();

        if (formData != null)
        {
            operation.Parameters.Add(new OpenApiParameter
            {
                Name = "File",
                In = ParameterLocation.Query,
                Description = "File Upload",
                Required = true,
                Schema = new OpenApiSchema { Type = "object" }
            });
        }
    }
}
