using System.Globalization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class SetVersionInPaths : IDocumentFilter
{
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        if (swaggerDoc.Paths == null || string.IsNullOrWhiteSpace(swaggerDoc.Info?.Version))
        {
            return;
        }

        var paths = new OpenApiPaths();
        foreach (var openApiPath in swaggerDoc.Paths.Where(openApiPath => !string.IsNullOrWhiteSpace(openApiPath.Key)))
        {
            if (swaggerDoc.Info.Version == null)
            {
                continue;
            }

            var apiVersion = ParseApiVersionString(swaggerDoc.Info.Version);
            paths.Add(
                openApiPath.Key.Replace("{apiVersion}", apiVersion.ToString(CultureInfo.InvariantCulture)),
                openApiPath.Value);
        }

        swaggerDoc.Paths = paths;
    }

    private int ParseApiVersionString(string? infoVersion)
    {
        if (string.IsNullOrWhiteSpace(infoVersion))
        {
            return 1;
        }

        if (!infoVersion.StartsWith("v", StringComparison.InvariantCultureIgnoreCase))
        {
            if (infoVersion.Contains('.'))
            {
                return int.Parse(infoVersion.Split('.') [0], CultureInfo.InvariantCulture);
            }

            return int.Parse(infoVersion.AsSpan(0, 1), CultureInfo.InvariantCulture);
        }

        var vRemovedVersion = infoVersion.Replace("v", "", StringComparison.InvariantCultureIgnoreCase);

        if (vRemovedVersion.Contains('.'))
        {
            return int.Parse(vRemovedVersion.Split('.') [0], CultureInfo.InvariantCulture);
        }

        return int.Parse(vRemovedVersion.AsSpan(0, 1), CultureInfo.InvariantCulture);
    }
}
