using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotnetWeb.WebApi.Library.IIS.CustomSwagger;

public class ResponseStatusOperationFilter : IOperationFilter
{
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        operation.Responses ??= [];

        if (!operation.Responses.ContainsKey("424"))
        {
            operation.Responses.Add("424", new OpenApiResponse { Description = "Failed Dependency" });
        }

        if (!operation.Responses.ContainsKey("429"))
        {
            operation.Responses.Add("429", new OpenApiResponse { Description = "Too Many Requests" });
        }

        if (!operation.Responses.ContainsKey("500"))
        {
            operation.Responses.Add("500", new OpenApiResponse { Description = "Internal Server Error" });
        }

        if (!operation.Responses.ContainsKey("503"))
        {
            operation.Responses.Add("503", new OpenApiResponse { Description = "Service Unavailable" });
        }
    }
}
