using System.Security.Claims;
using System.Text;
using DotnetWeb.Bootstrapper;

namespace DotnetWeb.WebApi.Library.IIS;

public class ArchiveMiddleware(RequestDelegate next)
{
    public async Task InvokeAsync(HttpContext httpContext)
    {
        var pipeline = httpContext.RequestServices.GetService<IPipeline>();
        var claimsPrincipal = httpContext.RequestServices.GetService<ClaimsPrincipal>();
        if (pipeline != null && claimsPrincipal != null)
        {
            var requestPath = httpContext.Request.Path.HasValue ? httpContext.Request.Path.Value : null;
            var requestMethod = httpContext.Request.Method;
            var requestHeaders = httpContext.Request.Headers;

            var requestCanProceed = RequestCanProceed(httpContext) &&
                pipeline.DoBeforePipeline(requestPath, requestMethod, requestHeaders);

            if (requestCanProceed)
            {
                await BeforeRequest(httpContext.Request).ConfigureAwait(false);
            }

            var responseCanProceed = ResponseCanProceed(httpContext) &&
                pipeline.DoAfterPipeline(requestPath, requestMethod, requestHeaders);

            if (requestCanProceed && responseCanProceed)
            {
                var originalBodyStream = httpContext.Response.Body;

                using (var responseBody = new MemoryStream())
                {
                    httpContext.Response.Body = responseBody;

                    await next(httpContext).ConfigureAwait(false);

                    await AfterRequest(httpContext.Response).ConfigureAwait(false);

                    await responseBody.CopyToAsync(originalBodyStream).ConfigureAwait(false);
                }
            }
            else
            {
                await next(httpContext).ConfigureAwait(false);
            }
        }
        else
        {
            await next(httpContext).ConfigureAwait(false);
        }
    }

    public async Task<byte []> ToByteArray(Stream input)
    {
        using (var ms = new MemoryStream())
        {
            await input.CopyToAsync(ms).ConfigureAwait(false);
            return ms.ToArray();
        }
    }

    public static MemoryStream ToStream(byte [] input) => new(input);

    private static async Task AfterRequest(HttpResponse response)
    {
        var body = await GetResponseBody(response.Body).ConfigureAwait(false);

        var pipeline = response.HttpContext.RequestServices.GetService<IPipeline>();
        if (pipeline != null)
        {
            await pipeline.AfterRequest(response.Headers, body).ConfigureAwait(false);
        }
    }

    private async Task BeforeRequest(HttpRequest request)
    {
        var initialBodyBytes = await ToByteArray(request.Body).ConfigureAwait(false);

        var pipeline = request.HttpContext.RequestServices.GetService<IPipeline>();
        if (pipeline != null)
        {
            await pipeline.BeforeRequest(request.Headers, GetRequestBody(initialBodyBytes)).ConfigureAwait(false);
        }

        request.EnableBuffering();
        request.Body = ToStream(initialBodyBytes);
    }

    private static string? GetRequestBody(byte []? body)
    {
        if (body == null)
        {
            return null;
        }

        var retVal = Encoding.UTF8.GetString(body);

        return string.IsNullOrWhiteSpace(retVal) ? null : retVal;
    }

    private static async Task<string?> GetResponseBody(Stream? responseBody)
    {
        if (responseBody == null)
        {
            return null;
        }

        responseBody.Seek(0, SeekOrigin.Begin);

        var body = new StreamReader(responseBody);
        var json = await body.ReadToEndAsync().ConfigureAwait(false);

        responseBody.Seek(0, SeekOrigin.Begin);

        return string.IsNullOrWhiteSpace(json) ? null : json;
    }

    private static bool RequestCanProceed(HttpContext? httpContext) => httpContext?.Request != null;

    private static bool ResponseCanProceed(HttpContext? httpContext) => httpContext?.Response != null;
}

public static class ArchiveMiddlewareExtensions
{
    public static IApplicationBuilder UseArchiveMiddleware(this IApplicationBuilder builder) => builder.UseMiddleware<ArchiveMiddleware>();
}
