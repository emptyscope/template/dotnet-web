using DotnetWeb.Common.Library.Common.DatabaseControl;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotnetWeb.WebApi.Library.IIS.HealthChecks;

#pragma warning disable CS9113 // Parameter is unread.
public class RabbitMqHealthCheck(IConnection connection) : IHealthCheck
#pragma warning restore CS9113 // Parameter is unread.
{
    public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
        CancellationToken cancellationToken = default)
    {
        try
        {
            return Task.FromResult(HealthCheckResult.Healthy());
        }
        catch (Exception exception)
        {
            var description = context.Registration.FailureStatus.ToString();
            return Task.FromResult(HealthCheckResult.Unhealthy(description, exception));
        }
    }
}
