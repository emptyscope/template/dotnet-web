using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Environments = DotnetWeb.Common.Library.Common.EnvironmentControl.Environments;

namespace DotnetWeb.WebApi.Library.IIS;

public class CustomCors(CurrentProjectInformation currentProjectInformation)
{
    public const string PolicyName = "cors";

    private static readonly string [] _corsArray =
    [
        "https://*.auth0.com",
        "https://*.okta.com",
        "https://*.microsoftonline.com",
        "https://*.azurewebsites.net"
    ];

    public Action<CorsOptions> Get(string policyName = PolicyName) => options =>
                                                                                  {
                                                                                      options.AddPolicy(policyName,
                                                                                          GetBuilder(currentProjectInformation.CurrentEnvironment?.Environment).Build());
                                                                                      options.DefaultPolicyName = policyName;
                                                                                  };

    private CorsPolicyBuilder GetBuilder(Environments? environmentEnum)
    {
        var builder = new CorsPolicyBuilder(GetAllowedCorsOrigins(environmentEnum));
        builder.SetIsOriginAllowedToAllowWildcardSubdomains();
        builder.AllowCredentials();
        builder.AllowAnyHeader();
        builder.WithMethods(DotnetWebConstants.HttpOptions);
        return builder;
    }

    private static string [] GetAllowedCorsOrigins(Environments? environmentEnum)
    {
        if (environmentEnum == null)
        {
            throw new ArgumentOutOfRangeException(nameof(environmentEnum), environmentEnum,
                @"Allowed CORS Origins Failure");
        }

        return environmentEnum switch
        {
            Environments.Local => GetLocalAllowedCorsOrigins(),
            Environments.Development => GetDevelopmentAllowedCorsOrigins(),
            Environments.Qa => GetQaAllowedCorsOrigins(),
            Environments.Uat => GetUatAllowedCorsOrigins(),
            Environments.Demo => GetDemoAllowedCorsOrigins(),
            Environments.Staging => GetStagingAllowedCorsOrigins(),
            Environments.Production => GetProductionAllowedCorsOrigins(),
            Environments.Unknown => throw new NotImplementedException(),
            Environments.Build => throw new NotImplementedException(),
            null => throw new NotImplementedException(),
            _ => throw new ArgumentOutOfRangeException(nameof(environmentEnum), environmentEnum, @"Allowed CORS Origins"),
        };
    }

    private static string [] GetLocalAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(AppendEndingSlash(AppendBothSchemas([
            "https://localhost:3000",
            "https://localhost:3001",
            "https://localhost:5000",
            "https://localhost:5001"
        ])));
        return [.. retVal];
    }

    private static string [] GetDevelopmentAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(GetLocalAllowedCorsOrigins());
        return [.. retVal];
    }

    private static string [] GetQaAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(GetDevelopmentAllowedCorsOrigins());
        return [.. retVal];
    }

    private static string [] GetDemoAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(GetQaAllowedCorsOrigins());
        return [.. retVal];
    }

    private static string [] GetUatAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(GetDemoAllowedCorsOrigins());
        return [.. retVal];
    }

    private static string [] GetStagingAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(GetUatAllowedCorsOrigins());
        return [.. retVal];
    }

    private static string [] GetProductionAllowedCorsOrigins()
    {
        var retVal = new List<string>();
        retVal.AddRange(AppendEndingSlash(_corsArray));
        return [.. retVal];
    }

    private static string [] AppendBothSchemas(string [] list)
    {
        var retVal = new List<string>();

        foreach (var item in list)
        {
            retVal.Add(GetHttpVersion(item));
            retVal.Add(GetHttpsVersion(item));
        }

        return [.. retVal];
    }

    private static string [] AppendEndingSlash(string [] list)
    {
        var retVal = new List<string>();

        foreach (var item in list)
        {
            retVal.Add(item);
            if (!item.EndsWith("/", StringComparison.OrdinalIgnoreCase))
            {
                retVal.Add(item + "/");
            }
        }

        return [.. retVal];
    }

    private static string GetHttpsVersion(string item)
    {
        if (item.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
        {
            return item.ToLowerInvariant();
        }

        return item.StartsWith("http", StringComparison.InvariantCultureIgnoreCase)
            ? item.ToLowerInvariant().Replace("http", "https")
            : throw new ArgumentException("Invalid HTTPS URL");
    }

    private static string GetHttpVersion(string item)
    {
        if (item.StartsWith("https", StringComparison.InvariantCultureIgnoreCase))
        {
            return item.StartsWith("https", StringComparison.InvariantCultureIgnoreCase)
                ? item.ToLowerInvariant().Replace("https", "http")
                : throw new ArgumentException("Invalid HTTPS HTTP URL");
        }

        return item.StartsWith("http", StringComparison.InvariantCultureIgnoreCase)
            ? item.ToLowerInvariant()
            : throw new ArgumentException("Invalid HTTP URL");
    }
}
