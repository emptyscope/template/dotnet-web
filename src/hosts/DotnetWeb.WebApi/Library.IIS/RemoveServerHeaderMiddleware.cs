namespace DotnetWeb.WebApi.Library.IIS;

public class RemoveServerHeaderMiddleware(RequestDelegate next)
{
    public async Task InvokeAsync(HttpContext httpContext)
    {
        httpContext.Response.Headers.Remove("Server");
        httpContext.Response.Headers.Remove("X-AspNetWebPages-Version");
        httpContext.Response.Headers.Remove("X-AspNet-Version");
        httpContext.Response.Headers.Remove("X-Powered-By");
        httpContext.Response.Headers.Remove("X-AspNetMvc-Version");
        await next(httpContext).ConfigureAwait(false);
    }
}

public static class RemoveServerHeaderMiddlewareExtensions
{
    public static IApplicationBuilder UseRemoveServerHeaderMiddleware(this IApplicationBuilder builder) => builder.UseMiddleware<RemoveServerHeaderMiddleware>();
}
