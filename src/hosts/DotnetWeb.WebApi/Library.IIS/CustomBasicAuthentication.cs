using System.Security.Claims;
using DotnetWeb.Common.Library.Common.ProjectControl;
using idunno.Authentication.Basic;

namespace DotnetWeb.WebApi.Library.IIS;

public class CustomBasicAuthentication(CurrentProjectInformation currentProjectInformation)
{
    public Action<BasicAuthenticationOptions> Get() => options =>
                                                                   {
                                                                       options.ForwardChallenge = "Basic";
                                                                       options.Realm = currentProjectInformation.ApplicationName;
                                                                       options.Events = new BasicAuthenticationEvents
                                                                       {
                                                                           OnValidateCredentials = context =>
                                                                                                   {
                                                                                                       if (string.IsNullOrWhiteSpace(context?.Username) ||
                                                                                                           string.IsNullOrWhiteSpace(context.Password))
                                                                                                       {
                                                                                                           return Task.CompletedTask;
                                                                                                       }

                                                                                                       var credentialsAreValid =
                                                                                                           !string.IsNullOrWhiteSpace(context.Username) &&
                                                                                                           !string.IsNullOrWhiteSpace(context.Password);

                                                                                                       if (credentialsAreValid)
                                                                                                       {
                                                                                                           var claims = new []
                                                                                                               {
                                                                                new Claim(ClaimTypes.NameIdentifier,
                                                                                    context.Username,
                                                                                    ClaimValueTypes.String,
                                                                                    context.Options.ClaimsIssuer),
                                                                                new Claim(ClaimTypes.Name,
                                                                                    context.Username,
                                                                                    ClaimValueTypes.String,
                                                                                    context.Options.ClaimsIssuer)
                                                                                                               };

                                                                                                           context.Principal =
                                                                                                               new ClaimsPrincipal(
                                                                                                                   new ClaimsIdentity(claims,
                                                                                                                       context.Scheme.Name));
                                                                                                           context.Success();
                                                                                                       }

                                                                                                       return Task.CompletedTask;
                                                                                                   }
                                                                       };
                                                                   };
}
