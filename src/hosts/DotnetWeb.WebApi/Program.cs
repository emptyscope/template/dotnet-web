using System.Diagnostics;
using Autofac.Extensions.DependencyInjection;
using DotnetWeb.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.LogControl;
using Serilog;

namespace DotnetWeb.WebApi;

public static class Program
{
    private static readonly LogLocationUtility _logLocationUtility;
    private static readonly IEnvironmentVariableUtility _environmentVariableUtility;
    private static readonly string? _applicationName;
    private static readonly string _defaultLogLocation;

    static Program()
    {
        _environmentVariableUtility = new EnvironmentVariableUtility();
        _logLocationUtility = new LogLocationUtility(_environmentVariableUtility);
        _applicationName = DotnetWebConstants.ApplicationName;
        _defaultLogLocation = DotnetWebConstants.DefaultLogLocation;
    }

    public static async Task Main(string []? args)
    {
        Log.Logger = LoggerSetup.Initialize(_logLocationUtility.Fetch(_defaultLogLocation, _applicationName));

        var environment = new BootstrapperRegistration(_environmentVariableUtility, new ConfigurationBuilder()
            .AddCommandLine(args ?? [])
            .Build());

        try
        {
            Log.Information($"{DotnetWebConstants.ApplicationName} WebApi Started");

            var builder = CreateWebHostBuilder(args ?? [], environment.GetEnvironment());

            using (var host = builder.Build())
            {
                await host.RunAsync().ConfigureAwait(false);
            }
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, $"{DotnetWebConstants.ApplicationName} WebApi stopped program because of exception");
            throw;
        }
        finally
        {
            await Log.CloseAndFlushAsync().ConfigureAwait(false);
        }
    }

    private static IHostBuilder CreateWebHostBuilder(string []? args, string environment)
    {
        var passedArgs = args ?? [];
        var currentDirectory = Directory.GetCurrentDirectory();

        var settings = new ConfigurationBuilder()
            .SetBasePath(currentDirectory)
            .AddJsonFile("appsettings.json", false, true)
            .AddJsonFile($"appsettings.{environment}.json", false,
                true)
            .AddEnvironmentVariables($"DotnetWeb_{environment}".ToUpperInvariant())
            .AddCommandLine(passedArgs)
            .Build();

        if (settings.GetValue("DOTNET_DEBUGGER_LAUNCH", false))
        {
            LaunchDebugger();
        }

        return Host
            .CreateDefaultBuilder(passedArgs)
            .ConfigureServices(services => services.AddAutofac())
            .UseServiceProviderFactory(new AutofacMultitenantServiceProviderFactory(Startup.ConfigureMultitenantContainer))
            .ConfigureWebHostDefaults(hostBuilder => hostBuilder
                                          .UseConfiguration(settings)
                                          .ConfigureLogging((context, loggingBuilder) => LoggerSetup.Configure(loggingBuilder, context.Configuration))
                                          .UseStartup<Startup>());
    }

    /// <summary>
    /// Launch debugger if it's requested by environment variable "DOTNET_DEBUG_ONSTART".
    /// </summary>
    private static void LaunchDebugger()
    {
        if (Debugger.IsAttached)
        {
            return;
        }

        Debugger.Launch();

        // Sometimes easier to attach rather than deal with JIT prompt
        // Process currentProcess = Process.GetCurrentProcess();
        // Console.WriteLine($"Waiting for debugger to attach ({currentProcess.MainModule.FileName} PID {currentProcess.Id}).  Press enter to continue...");
        // Console.ReadLine();
    }
}
