using System.Net;
using System.Security.Claims;
using Asp.Versioning;
using Autofac;
using Autofac.Multitenant;
using Delta;
using DotnetWeb.Bootstrapper;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.ProjectControl;
using DotnetWeb.Repository.Library.HealthCheck;
using DotnetWeb.Route.Controllers.v1;
using DotnetWeb.WebApi.BackgroundWorker;
using DotnetWeb.WebApi.Library.IIS;
using DotnetWeb.WebApi.Library.IIS.CustomSwagger;
using MassTransit;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Unleash;
using Unleash.ClientFactory;
using Environments = DotnetWeb.Common.Library.Common.EnvironmentControl.Environments;
using IContainer = Autofac.IContainer;

namespace DotnetWeb.WebApi;

public class Startup(IConfiguration configuration, IWebHostEnvironment environment)
{
    private IConfiguration Configuration { get; } = configuration;
    private IWebHostEnvironment HostEnvironment { get; } = environment;

    // ConfigureServices is where you register dependencies. This gets
    // called by the runtime before the ConfigureContainer method, below.
    // This will all go in the ROOT CONTAINER and is NOT TENANT SPECIFIC.
    public void ConfigureServices(IServiceCollection services)
    {
        // Add services to the collection. Don't build or return
        // any IServiceProvider or the ConfigureContainer method
        // won't get called.
        var currentProjectInformation = GetCurrentProjectInformation(DotnetWebConstants.CurrentApiVersion(),
            DotnetWebConstants.CurrentProjectVersion(), HostEnvironment.EnvironmentName);
        var healthProjectInformation = new ProjectInformationMapper().MapHealth(currentProjectInformation);

        services.AddLogging(loggingBuilder => LoggerSetup.Configure(loggingBuilder, Configuration));
        services.Configure<ForwardedHeadersOptions>(options =>
                                                    {
                                                        options.ForwardLimit = 2;

                                                        if (!string.IsNullOrWhiteSpace(
                                                                DotnetWebConstants.ProxyIpAddress))
                                                        {
                                                            options.KnownProxies.Add(
                                                                IPAddress.Parse(
                                                                    $"{DotnetWebConstants.ProxyIpAddress}"));
                                                        }

                                                        options.ForwardedForHeaderName = $"X-Forwarded-For-{DotnetWebConstants.ApplicationName}";
                                                    });

        services
            .AddCors(new CustomCors(currentProjectInformation).Get())
            .AddControllers(options => options.RespectBrowserAcceptHeader = false)
            .AddControllersAsServices()
            .ConfigureApiBehaviorOptions(options =>
                                         {
                                             options.SuppressConsumesConstraintForFormFileParameters = true;
                                             options.SuppressInferBindingSourcesForParameters = true;
                                             options.SuppressModelStateInvalidFilter = true;
                                             options.SuppressMapClientErrors = false;
                                             options.ClientErrorMapping [StatusCodes.Status400BadRequest].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status400BadRequest}";
                                             options.ClientErrorMapping [StatusCodes.Status401Unauthorized].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status401Unauthorized}";
                                             options.ClientErrorMapping [StatusCodes.Status403Forbidden].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status403Forbidden}";
                                             options.ClientErrorMapping [StatusCodes.Status404NotFound].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status404NotFound}";
                                             options.ClientErrorMapping [StatusCodes.Status405MethodNotAllowed].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status405MethodNotAllowed}";
                                             options.ClientErrorMapping [StatusCodes.Status406NotAcceptable].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status406NotAcceptable}";
                                             options.ClientErrorMapping [StatusCodes.Status409Conflict].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status409Conflict}";
                                             options.ClientErrorMapping [StatusCodes.Status415UnsupportedMediaType]
                                                     .Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status415UnsupportedMediaType}";
                                             options.ClientErrorMapping [StatusCodes.Status422UnprocessableEntity].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status422UnprocessableEntity}";
                                             options.ClientErrorMapping [StatusCodes.Status500InternalServerError].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status500InternalServerError}";
                                             options.DisableImplicitFromServicesParameters = true;
                                         })
            .AddJsonOptions(options => options.JsonSerializerOptions.AllowTrailingCommas = false)
            .AddNewtonsoftJson(options =>
                               {
                                   options.SerializerSettings.Formatting = Formatting.Indented;
                                   options.SerializerSettings.ContractResolver = new DefaultContractResolver
                                   {
                                       SerializeCompilerGeneratedMembers = false,
                                       IgnoreSerializableInterface = false,
                                       IgnoreSerializableAttribute = false,
                                       IgnoreIsSpecifiedMembers = false,
                                       IgnoreShouldSerializeMembers = false,
                                       NamingStrategy = new CamelCaseNamingStrategy()
                                   };
                               })
            .AddXmlSerializerFormatters();

        services.AddOpenApiDocument();
        services
            .AddProblemDetails()
            .AddHttpLogging(logging =>
                            {
                                logging.LoggingFields = HttpLoggingFields.All;
                                logging.RequestHeaders.Add("sec-ch-ua");
                                logging.ResponseHeaders.Add($"X-{DotnetWebConstants.ApplicationName}");
                                logging.MediaTypeOptions.AddText("application/json");
                                logging.RequestBodyLogLimit = 4096;
                                logging.ResponseBodyLogLimit = 4096;
                                logging.CombineLogs = true;
                            })
            .AddOpenApi()
            .AddSwaggerGen(new CustomSwaggerGen(currentProjectInformation.ApplicationName,
                currentProjectInformation.CurrentEnvironment?.Name).Get(DotnetWebConstants.ApiList()))
            .AddSwaggerGenNewtonsoftSupport()
            .AddEndpointsApiExplorer()
            .AddApiVersioning(options =>
                              {
                                  var apiVersion = new ApiVersion(DotnetWebConstants.CurrentApiVersion().Item1,
                                      DotnetWebConstants.CurrentApiVersion().Item2);
                                  options.ApiVersionReader = ApiVersionReader.Combine(new UrlSegmentApiVersionReader(),
                                      new HeaderApiVersionReader("x-api-version"),
                                      new MediaTypeApiVersionReader("x-api-version"));
                                  options.DefaultApiVersion = apiVersion;
                                  options.ReportApiVersions = true;
                                  options.AssumeDefaultVersionWhenUnspecified = true;
                                  options.Policies.Sunset(0.9)
                                      .Effective(DateTimeOffset.UtcNow.AddDays(60))
                                      .Link("policy.html")
                                      .Title("Versioning Policy")
                                      .Type("text/html");
                              })
            .AddApiExplorer(options =>
                            {
                                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                                // note: the specified format code will format the version as "'v'major[.minor][-status]"
                                options.GroupNameFormat = "'v'VVVV";
                                options.SubstituteApiVersionInUrl = true;
                            })
            .EnableApiVersionBinding()
            .AddMvc();

        if (ShouldIncludeUnleash(currentProjectInformation))
        {
            services.AddSingleton(Unleash(currentProjectInformation));
        }

        services.AddHealthChecks()
            .AddCheck<SqlHealthCheck>("custom-sql", HealthStatus.Unhealthy);
        // .AddRabbitMQ(rabbitConnectionString)
        // .AddSqlServer(sqlConnectionString)
        // .AddRedis(redisConnectionString)
        // .AddSignalR(signalRConnectionString)
        // .AddSmtp(smtpConnectionString)
        // .AddNpgsql(npgsqlConnectionString);

        services.AddSingleton(currentProjectInformation);
        services.AddSingleton(healthProjectInformation);
        services.AddSingleton(currentProjectInformation.CurrentEnvironment?.Authentication!);

        services.AddScoped(provider =>
        {
            var context = provider.GetService<IHttpContextAccessor>()?.HttpContext;
            return context?.User == null ? new ClaimsPrincipal(new ClaimsIdentity()) : context.User;
        });

        // Configure forwarded headers
        services.Configure<ForwardedHeadersOptions>(options => options.KnownProxies.Add(IPAddress.Parse("10.0.0.100")));

        services.AddAuthentication(ConfigureOptions("Basic"))
            .AddBasic(new CustomBasicAuthentication(currentProjectInformation).Get());

        services.AddMassTransit(x =>
                                {
                                    x.AddConsumer<GettingStartedController>();
                                    x.UsingInMemory((context, cfg) => cfg.ConfigureEndpoints(context));
                                });

        services.AddHostedService<InMemoryWorker>();

        // This adds the required middleware to the ROOT CONTAINER and is required for multitenancy to work.
        services.AddAutofacMultitenantRequestServices();
    }

    // ConfigureContainer is where you can register things directly
    // with Autofac. This runs after ConfigureServices so the things
    // here will override registrations made in ConfigureServices.
    // Don't build the container; that gets done for you. If you
    // need a reference to the container, you need to use the
    // "Without ConfigureContainer" mechanism shown later.
    public void ConfigureContainer(ContainerBuilder builder)
    {
        // This will all go in the ROOT CONTAINER and is NOT TENANT SPECIFIC.
        var bootstrapperRegistration = new BootstrapperRegistration(new EnvironmentVariableUtility(), Configuration);
        bootstrapperRegistration.RegisterAutofacDi(builder);
    }

    public static MultitenantContainer ConfigureMultitenantContainer(IContainer container)
    {
        // This is the MULTITENANT PART. Set up your tenant-specific stuff here.
        var strategy = new DotnetWebTenantIdentificationStrategy();
        var mtc = new MultitenantContainer(strategy, container);
        // mtc.ConfigureTenant("a", cb => cb.RegisterType<TenantDependency>().As<IDependency>());
        return mtc;
    }

    // Configure is where you add middleware. This is called after
    // ConfigureContainer. You can use IApplicationBuilder.ApplicationServices
    // here if you need to resolve things from the container.
    public void Configure(
        IApplicationBuilder application,
        ILoggerFactory loggerFactory)
    {

        var useProductionSettings = HostEnvironment.IsProduction() ||
                                            HostEnvironment.IsStaging() ||
                                            HostEnvironment.IsEnvironment(Environments.Uat.ToString());

        var useDevelopmentSettings = !useProductionSettings;

        if (useDevelopmentSettings)
        {
            application.UseDeveloperExceptionPage();
        }
        else
        {
            application.UseExceptionHandler($"/api/v{DotnetWebConstants.CurrentApiVersion().Item1}/error");
        }

        application.UseDelta();
        application.UseCors(CustomCors.PolicyName);
        application.UseStaticFiles();
        application.UseSwagger(options => options.RouteTemplate = "api/swagger/{documentName}/swagger.json");

        if (useDevelopmentSettings)
        {
            application.UseOpenApi();
            application.UseSwaggerUI(c =>
            {
                c.OAuthUseBasicAuthenticationWithAccessCodeGrant();
                c.RoutePrefix = "ui";
                foreach (var apiVersion in DotnetWebConstants.ApiList())
                {
                    c.SwaggerEndpoint($"/swagger/v{apiVersion.Item1}/swagger.json",
                        $"DotnetWeb V{apiVersion.Item1}.{apiVersion.Item2}.{apiVersion.Item3}");
                }
            });
        }

        application.UseForwardedHeaders(new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });

        application.UseAuthentication();
        application.UseHttpLogging();
        application.UseArchiveMiddleware();
        application.UseRemoveServerHeaderMiddleware();
        application.UseRouting();
        application.UseEndpoints(endpoints =>
        {
            endpoints.MapSwagger();
            endpoints.MapControllers();
        });

        // ReSharper disable once InvertIf
        if (!useDevelopmentSettings)
        {
            application.UseHsts();
            application.UseHttpsRedirection();
        }
    }

    private static Action<AuthenticationOptions> ConfigureOptions(string authenticationScheme) => configureOptions => GetAuthenticationOptions(authenticationScheme, configureOptions);

    // ReSharper disable once UnusedMethodReturnValue.Local
    private static AuthenticationOptions GetAuthenticationOptions(string authenticationScheme,
        AuthenticationOptions options) =>
        new()
        {
            DefaultScheme = string.IsNullOrWhiteSpace(authenticationScheme)
                ? options.DefaultScheme
                : authenticationScheme,
            DefaultAuthenticateScheme = string.IsNullOrWhiteSpace(authenticationScheme)
                ? options.DefaultAuthenticateScheme
                : authenticationScheme,
            DefaultSignInScheme = options.DefaultSignInScheme,
            DefaultSignOutScheme = options.DefaultSignOutScheme,
            DefaultChallengeScheme = string.IsNullOrWhiteSpace(authenticationScheme)
                ? options.DefaultChallengeScheme
                : authenticationScheme,
            DefaultForbidScheme = options.DefaultForbidScheme,
            RequireAuthenticatedSignIn = options.RequireAuthenticatedSignIn
        };

    private CurrentProjectInformation GetCurrentProjectInformation(Tuple<int, int, int, string?> currentApiVersion,
        Tuple<int, int> currentProjectVersion, string environmentName)
    {
        var appSettings = Configuration
            .GetSection(DotnetWebConstants.CustomSettingsSectionName)
            .Get<DotnetWebAppSettings>();

        if (Enum.TryParse(environmentName, out Environments environment))
        {
            return new ProjectInformationMapper().MapCurrent(appSettings, currentApiVersion, currentProjectVersion,
                environment);
        }

        throw new ArgumentException(environmentName);

    }

    private static Func<IServiceProvider, IUnleash> Unleash(CurrentProjectInformation currentProjectInformation) =>
        _ => Unleash(currentProjectInformation.ApplicationName!,
            currentProjectInformation.CurrentEnvironment!.UnleashSettings!.ApiUrl!,
            currentProjectInformation.CurrentEnvironment.UnleashSettings.InstanceId!);

    private static bool ShouldIncludeUnleash(CurrentProjectInformation currentProjectInformation) =>
        !string.IsNullOrWhiteSpace(currentProjectInformation.CurrentEnvironment?.UnleashSettings?.ApiUrl) &&
        !string.IsNullOrWhiteSpace(currentProjectInformation.CurrentEnvironment?.UnleashSettings?.InstanceId);

    private static IUnleash Unleash(string name, string apiUrl, string instanceId)
    {
        var unleash = new UnleashClientFactory()
            .CreateClient(new UnleashSettings
            {
                AppName = name,
                UnleashApi =
                                  new Uri(apiUrl),
                InstanceTag = instanceId,
            }, true);

        unleash.ConfigureEvents(cfg =>
                                {
                                    cfg.ImpressionEvent = evt => Console.WriteLine($@"{evt.FeatureName}: {evt.Enabled}");
                                    cfg.ErrorEvent = evt =>
                                                         /* Handling code here */
                                                         Console.WriteLine($@"{evt.ErrorType} occured.");
                                    cfg.TogglesUpdatedEvent = evt =>
                                                                  /* Handling code here */
                                                                  Console.WriteLine(
                                                                      $@"Toggles updated on: {evt.UpdatedOn}");
                                });
        return unleash;
    }
}
