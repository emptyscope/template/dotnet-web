using System.Security.Claims;
using Autofac;
using Autofac.Multitenant;
using DotnetWeb.Bootstrapper;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Unleash;
using Unleash.ClientFactory;

namespace DotnetWeb.ConsoleApplication;

public class Startup(IConfiguration configuration, IWebHostEnvironment environment)
{
    private IConfiguration Configuration { get; } = configuration;
    private IWebHostEnvironment HostEnvironment { get; } = environment;

    // ConfigureServices is where you register dependencies. This gets
    // called by the runtime before the ConfigureContainer method, below.
    // This will all go in the ROOT CONTAINER and is NOT TENANT SPECIFIC.
    public void ConfigureServices(IServiceCollection services)
    {
        // Add services to the collection. Don't build or return
        // any IServiceProvider or the ConfigureContainer method
        // won't get called.
        var currentProjectInformation = GetCurrentProjectInformation(DotnetWebConstants.CurrentApiVersion(),
            DotnetWebConstants.CurrentProjectVersion(), new EnvironmentVariableUtility());
        var healthProjectInformation = new ProjectInformationMapper().MapHealth(currentProjectInformation);

        services.AddLogging(loggingBuilder => LoggerSetup.Configure(loggingBuilder, Configuration));

        services
            .AddControllers(options => options.RespectBrowserAcceptHeader = false)
            .AddControllersAsServices()
            .ConfigureApiBehaviorOptions(options =>
                                         {
                                             options.SuppressConsumesConstraintForFormFileParameters = true;
                                             options.SuppressInferBindingSourcesForParameters = true;
                                             options.SuppressModelStateInvalidFilter = true;
                                             options.SuppressMapClientErrors = false;
                                             options.ClientErrorMapping [StatusCodes.Status400BadRequest].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status400BadRequest}";
                                             options.ClientErrorMapping [StatusCodes.Status401Unauthorized].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status401Unauthorized}";
                                             options.ClientErrorMapping [StatusCodes.Status403Forbidden].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status403Forbidden}";
                                             options.ClientErrorMapping [StatusCodes.Status404NotFound].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status404NotFound}";
                                             options.ClientErrorMapping [StatusCodes.Status405MethodNotAllowed].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status405MethodNotAllowed}";
                                             options.ClientErrorMapping [StatusCodes.Status406NotAcceptable].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status406NotAcceptable}";
                                             options.ClientErrorMapping [StatusCodes.Status409Conflict].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status409Conflict}";
                                             options.ClientErrorMapping [StatusCodes.Status415UnsupportedMediaType]
                                                     .Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status415UnsupportedMediaType}";
                                             options.ClientErrorMapping [StatusCodes.Status422UnprocessableEntity].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status422UnprocessableEntity}";
                                             options.ClientErrorMapping [StatusCodes.Status500InternalServerError].Link =
                                                 $"{DotnetWebConstants.HttpStatusUrl}/${StatusCodes.Status500InternalServerError}";
                                             options.DisableImplicitFromServicesParameters = true;
                                         })
            .AddJsonOptions(options => options.JsonSerializerOptions.AllowTrailingCommas = false)
            .AddXmlSerializerFormatters();

        services
            .AddProblemDetails()
            .AddHttpLogging(logging =>
                            {
                                logging.LoggingFields = HttpLoggingFields.All;
                                logging.RequestHeaders.Add("sec-ch-ua");
                                logging.ResponseHeaders.Add($"X-{DotnetWebConstants.ApplicationName}");
                                logging.MediaTypeOptions.AddText("application/json");
                                logging.RequestBodyLogLimit = 4096;
                                logging.ResponseBodyLogLimit = 4096;
                                logging.CombineLogs = true;
                            })
            .AddEndpointsApiExplorer()
            .AddMvc();

        if (!string.IsNullOrWhiteSpace(currentProjectInformation.CurrentEnvironment?.UnleashSettings?.ApiUrl) &&
            !string.IsNullOrWhiteSpace(currentProjectInformation.CurrentEnvironment?.UnleashSettings?.InstanceId))
        {
            var unleash = new UnleashClientFactory().CreateClient(new UnleashSettings
            {
                AppName = currentProjectInformation.CurrentEnvironment!.Name,
                UnleashApi =
                                                                          new Uri(currentProjectInformation.CurrentEnvironment
                                                                              .UnleashSettings.ApiUrl),
                InstanceTag = currentProjectInformation.CurrentEnvironment
                                                                          .UnleashSettings.InstanceId,
            }, true);

            unleash.ConfigureEvents(cfg =>
                                    {
                                        cfg.ImpressionEvent = evt => Console.WriteLine(
                                                                      $@"{evt.FeatureName}: {evt.Enabled}");
                                        cfg.ErrorEvent = evt =>
                                                             /* Handling code here */
                                                             Console.WriteLine($@"{evt.ErrorType} occured.");
                                        cfg.TogglesUpdatedEvent = evt =>
                                                                      /* Handling code here */
                                                                      Console.WriteLine(
                                                                          $@"Toggles updated on: {evt.UpdatedOn}");
                                    });

            services.AddSingleton(_ => unleash);
        }

        services.AddSingleton(currentProjectInformation);
        services.AddSingleton(healthProjectInformation);
        // services.AddSingleton<CurrentProjectInformation>(currentProjectInformation);
        // services.AddSingleton<CurrentVersionInformation>(currentProjectInformation.VersionInformation!);
        // services.AddSingleton<HealthEnvironmentInformation>(healthProjectInformation.CurrentEnvironment!);
        // services.AddSingleton<CurrentEnvironmentInformation>(currentProjectInformation.CurrentEnvironment!);
        services.AddSingleton(currentProjectInformation.CurrentEnvironment
            ?.Authentication!);

        services.AddScoped(provider =>
        {
            var context = provider.GetService<IHttpContextAccessor>()?.HttpContext;
            return context?.User == null ? new ClaimsPrincipal(new ClaimsIdentity()) : context.User;
        });

        // This adds the required middleware to the ROOT CONTAINER and is required for multitenancy to work.
        services.AddAutofacMultitenantRequestServices();
    }

    // ConfigureContainer is where you can register things directly
    // with Autofac. This runs after ConfigureServices so the things
    // here will override registrations made in ConfigureServices.
    // Don't build the container; that gets done for you. If you
    // need a reference to the container, you need to use the
    // "Without ConfigureContainer" mechanism shown later.
    public void ConfigureContainer(ContainerBuilder builder)
    {
        // This will all go in the ROOT CONTAINER and is NOT TENANT SPECIFIC.
        var bootstrapperRegistration = new BootstrapperRegistration(new EnvironmentVariableUtility(), Configuration);
        bootstrapperRegistration.RegisterAutofacDi(builder);
    }

    public static MultitenantContainer ConfigureMultitenantContainer(IContainer container)
    {
        // This is the MULTITENANT PART. Set up your tenant-specific stuff here.
        var strategy = new DotnetWebTenantIdentificationStrategy();
        var mtc = new MultitenantContainer(strategy, container);
        // mtc.ConfigureTenant("a", cb => cb.RegisterType<TenantDependency>().As<IDependency>());
        return mtc;
    }

    // Configure is where you add middleware. This is called after
    // ConfigureContainer. You can use IApplicationBuilder.ApplicationServices
    // here if you need to resolve things from the container.
    public void Configure(
        IApplicationBuilder application,
        ILoggerFactory loggerFactory)
    {
        var useProductionSettings = HostEnvironment.IsProduction() ||
                                            HostEnvironment.IsStaging() ||
                                            HostEnvironment.IsEnvironment(DotnetWebConstants.Uat);

        var useDevelopmentSettings = !useProductionSettings;

        if (useDevelopmentSettings)
        {
            application.UseDeveloperExceptionPage();
        }
        else
        {
            application.UseExceptionHandler($"/api/v{DotnetWebConstants.CurrentApiVersion().Item1}/error");
        }

        application.UseStaticFiles();

        application.UseAuthentication();
        application.UseHttpLogging();
        application.UseRouting();
        application.UseEndpoints(endpoints => endpoints.MapControllers());

        if (useDevelopmentSettings)
        {
            return;
        }

        application.UseHsts();
        application.UseHttpsRedirection();
    }

    // ReSharper disable once UnusedMethodReturnValue.Local
    private static AuthenticationOptions GetAuthenticationOptions(string authenticationScheme,
        AuthenticationOptions options) =>
        new()
        {
            DefaultScheme = string.IsNullOrWhiteSpace(authenticationScheme)
                ? options.DefaultScheme
                : authenticationScheme,
            DefaultAuthenticateScheme = string.IsNullOrWhiteSpace(authenticationScheme)
                ? options.DefaultAuthenticateScheme
                : authenticationScheme,
            DefaultSignInScheme = options.DefaultSignInScheme,
            DefaultSignOutScheme = options.DefaultSignOutScheme,
            DefaultChallengeScheme = string.IsNullOrWhiteSpace(authenticationScheme)
                ? options.DefaultChallengeScheme
                : authenticationScheme,
            DefaultForbidScheme = options.DefaultForbidScheme,
            RequireAuthenticatedSignIn = options.RequireAuthenticatedSignIn
        };

    private CurrentProjectInformation GetCurrentProjectInformation(Tuple<int, int, int, string?> currentApiVersion,
        Tuple<int, int> currentProjectVersion, IEnvironmentVariableUtility environmentVariableUtility)
    {
        var appSettings = Configuration
            .GetSection(DotnetWebConstants.CustomSettingsSectionName)
            .Get<DotnetWebAppSettings>();

        var environment = new CustomHostingEnvironmentMapper(appSettings?.ApplicationName, environmentVariableUtility)
            .Fetch(DotnetWebConstants.ApplicationEnvironmentVariable);

        return new ProjectInformationMapper().MapCurrent(appSettings, currentApiVersion, currentProjectVersion,
            environment.Environment);
    }
}
