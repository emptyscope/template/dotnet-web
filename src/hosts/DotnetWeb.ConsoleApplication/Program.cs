using Autofac.Extensions.DependencyInjection;
using DotnetWeb.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.LogControl;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace DotnetWeb.ConsoleApplication;

public static class Program
{
    private static readonly LogLocationUtility _logLocationUtility;
    private static readonly IEnvironmentVariableUtility _environmentVariableUtility;
    private static readonly string? _applicationName;
    private static readonly string _defaultLogLocation;

    static Program()
    {
        _environmentVariableUtility = new EnvironmentVariableUtility();
        _logLocationUtility = new LogLocationUtility(_environmentVariableUtility);
        _applicationName = DotnetWebConstants.ApplicationName;
        _defaultLogLocation = DotnetWebConstants.DefaultLogLocation;
    }

    public static async Task Main(string []? args)
    {
        Log.Logger = LoggerSetup.Initialize(_logLocationUtility.Fetch(_defaultLogLocation, _applicationName));

        var environment = new BootstrapperRegistration(_environmentVariableUtility, new ConfigurationBuilder()
            .AddCommandLine(args ?? [])
            .Build());

        try
        {
            Log.Information("WebApi Started");

            var builder = CreateWebHostBuilder(args ?? [], environment.GetEnvironment());

            using (var host = builder.Build())
            {
                await host.RunAsync().ConfigureAwait(false);
            }
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "WebApi stopped program because of exception");
            throw;
        }
        finally
        {
            await Log.CloseAndFlushAsync().ConfigureAwait(false);
        }
    }

    private static IHostBuilder CreateWebHostBuilder(string []? args, string environment)
    {
        var passedArgs = args ?? [];
        var currentDirectory = Directory.GetCurrentDirectory();

        var settings = new ConfigurationBuilder()
            .SetBasePath(currentDirectory)
            .AddJsonFile("appsettings.json", false, true)
            .AddJsonFile($"appsettings.{environment}.json", false,
                true)
            .AddEnvironmentVariables($"DotnetWeb_{environment}".ToUpperInvariant())
            .AddCommandLine(passedArgs)
            .Build();

        return Host
            .CreateDefaultBuilder(passedArgs)
            .ConfigureServices(services => services.AddAutofac())
            .UseServiceProviderFactory(new AutofacMultitenantServiceProviderFactory(Startup.ConfigureMultitenantContainer))
            .ConfigureWebHostDefaults(hostBuilder => hostBuilder
                                          .UseConfiguration(settings)
                                          .ConfigureLogging((context, loggingBuilder) => LoggerSetup.Configure(loggingBuilder, context.Configuration))
                                          .UseStartup<Startup>());
    }
}
