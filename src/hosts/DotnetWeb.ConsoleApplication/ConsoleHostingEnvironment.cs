using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace DotnetWeb.ConsoleApplication;

public class ConsoleHostingEnvironment(CurrentProjectInformation? currentProjectInformation) : IHostEnvironment
{
    public string EnvironmentName { get; set; } = currentProjectInformation?.CurrentEnvironment?.Name ?? "Default";
    public string ApplicationName { get; set; } = currentProjectInformation?.ApplicationName ?? "Default";
    public string? WebRootPath { get; set; }
    public IFileProvider? WebRootFileProvider { get; set; }
    public string ContentRootPath { get; set; } = "";
    public IFileProvider ContentRootFileProvider { get; set; } = new NullFileProvider();
}
