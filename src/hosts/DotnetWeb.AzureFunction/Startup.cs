using Autofac;
using Autofac.Multitenant;
using DotnetWeb.AzureFunction;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Common.Library.Common.ProjectControl;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

[assembly: FunctionsStartup(typeof(Startup))]

namespace DotnetWeb.AzureFunction;

internal sealed class Startup : FunctionsStartup
{
    private readonly LogLocationUtility _logLocationUtility;
    private readonly IEnvironmentVariableUtility _environmentVariableUtility;
    private readonly string _defaultLogLocation;
    private readonly Tuple<int, int, int, string?> _currentApiVersion;
    private readonly Tuple<int, int> _currentProjectVersion;

    public Startup()
    {
        _environmentVariableUtility = new EnvironmentVariableUtility();
        _logLocationUtility = new LogLocationUtility(_environmentVariableUtility);
        _defaultLogLocation = DotnetWebConstants.DefaultLogLocation;
        _currentApiVersion = DotnetWebConstants.CurrentApiVersion();
        _currentProjectVersion = DotnetWebConstants.CurrentProjectVersion();
    }

    public override void Configure(IFunctionsHostBuilder functionsHostBuilder)
    {
        var projectInformation =
            GetCurrentProjectInformation(_currentApiVersion, _currentProjectVersion, _environmentVariableUtility);
        var healthInformation = new ProjectInformationMapper().MapHealth(projectInformation);

        LoggerSetup.Initialize(
            _logLocationUtility.Fetch(_defaultLogLocation, projectInformation.ApplicationName));

        functionsHostBuilder.Services
            .AddSingleton(projectInformation)
            .AddSingleton(healthInformation)
            .AddLogging(builder => builder.AddSerilog(Log.Logger, true));

        // var serviceCollection = BootstrapperRegistration(new ServiceCollection());

        // functionsHostBuilder.Services.Add(serviceCollection);
    }

    private CurrentProjectInformation GetCurrentProjectInformation(Tuple<int, int, int, string?> currentApiVersion,
        Tuple<int, int> currentProjectVersion, IEnvironmentVariableUtility environmentVariableUtility)
    {
        var appSettings = GetConfigurationRoot()
            .GetSection(DotnetWebConstants.CustomSettingsSectionName)
            .Get<DotnetWebAppSettings>();

        var environment = new CustomHostingEnvironmentMapper(appSettings?.ApplicationName, environmentVariableUtility)
            .Fetch(DotnetWebConstants.ApplicationEnvironmentVariable);

        return new ProjectInformationMapper().MapCurrent(appSettings, currentApiVersion, currentProjectVersion,
            environment.Environment);
    }

    private IConfiguration GetConfigurationRoot(IConfiguration? configuration = null)
    {
        var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

        if (configuration != null)
        {
            return new ConfigurationBuilder()
                .AddConfiguration(configuration)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{environmentName}.json", true)
                .AddEnvironmentVariables()
                .Build();
        }

        return new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", false, true)
            .AddJsonFile($"appsettings.{environmentName}.json", true)
            .AddEnvironmentVariables()
            .Build();
    }

    public static MultitenantContainer ConfigureMultitenantContainer(IContainer container)
    {
        // This is the MULTITENANT PART. Set up your tenant-specific stuff here.
        var strategy = new DotnetWebTenantIdentificationStrategy();
        var mtc = new MultitenantContainer(strategy, container);
        // mtc.ConfigureTenant("a", cb => cb.RegisterType<TenantDependency>().As<IDependency>());
        return mtc;
    }
}
