using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.ParserControl;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.AzureFunction;

public class HttpFunction(IDotnetWebService dotnetWebService, IArgumentParser parser, ILogger<HttpFunction> logger)
{
    private readonly IDotnetWebService _service = dotnetWebService;
    private readonly IArgumentParser _parser = parser;
    private readonly ILogger<HttpFunction> _logger = logger;

    [Function(nameof(HttpFunction))]
    public async Task<IActionResult> Post(
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest? httpRequest,
        ILogger logger)
    {
        if (!_parser.CanParseBody(httpRequest?.Body, out var parsed))
        {
            return new BadRequestObjectResult("Could not parse request");
        }

        foreach (var str in parsed)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                _logger.LogTrace("Message was null");
                continue;
            }

            if (Guid.TryParse(str, out var id))
            {
                await _service.Fetch(id).ConfigureAwait(false);
            }
        }


        return new OkResult();
    }
}
