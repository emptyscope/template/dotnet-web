using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Model;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Core;

public class DotnetWebService(IDotnetWebRepository repository, ILogger<DotnetWebService> logger)
    : IDotnetWebService
{
    public async Task<DotnetWebResponse?> Fetch(Guid id)
    {
        logger.Hit(Guid.Empty, Guid.NewGuid());

        Validate(id);

        return await repository.Fetch(id).ConfigureAwait(false);
    }

    public Task<Guid> Create(DotnetWebRequest request)
    {
        logger.Hit(Guid.Empty, Guid.NewGuid());

        return Task.FromResult(Guid.NewGuid());
    }

    public async Task<IList<DotnetWebResponse>> List()
    {
        logger.Hit(Guid.Empty, Guid.NewGuid());

        var list = await repository.List().ConfigureAwait(false);

        return list.ToList();
    }

    private void Validate(Guid? id)
    {
        if (id == null)
        {
            throw new ArgumentException("Missing id");
        }

        if (Guid.Empty.Equals(id.Value))
        {
            throw new ArgumentException("Invalid id");
        }
    }
}
