SELECT  *
FROM    [dbo].[User]
WHERE   1 = 1
        AND ( @Id IS NULL
              OR [Id] = @Id )
