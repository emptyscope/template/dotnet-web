using DotnetWeb.Common.Library.Common.DatabaseControl;

namespace DotnetWeb.Repository.Library.Repository;

public sealed class DotnetWebSqlException : Exception
{
    public DotnetWebSqlException(IConnection connection, string message, string sql) : base(message)
    {
        Data.Add("TraceId", connection.TraceId);
        Data.Add("Sql", sql);
        Data.Add("Datasource", connection.Datasource);
        Data.Add("Catalog", connection.Catalog);
        Data.Add("SqlUsername", connection.SqlUsername);
        Data.Add("IsEncrypted", connection.IsEncrypted);
        Data.Add("IsReadOnly", connection.IsReadOnly);
        Data.Add("TrustServerCertificate", connection.TrustServerCertificate);
    }

    public DotnetWebSqlException(IConnection connection, string message, string sql, Exception innerException) : base(message, innerException)
    {
        Data.Add("TraceId", connection.TraceId);
        Data.Add("Sql", sql);
        Data.Add("Datasource", connection.Datasource);
        Data.Add("Catalog", connection.Catalog);
        Data.Add("SqlUsername", connection.SqlUsername);
        Data.Add("IsEncrypted", connection.IsEncrypted);
        Data.Add("IsReadOnly", connection.IsReadOnly);
        Data.Add("TrustServerCertificate", connection.TrustServerCertificate);
    }
}
