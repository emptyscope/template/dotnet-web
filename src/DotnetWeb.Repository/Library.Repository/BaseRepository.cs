using System.Data;
using Dapper;
using DotnetWeb.Common.Library.Common.DatabaseControl;
using Microsoft.Data.SqlClient;

namespace DotnetWeb.Repository.Library.Repository;

public abstract class BaseRepository(IConnection connection)
{
    private const int DefaultCommandTimeoutInSeconds = 5;

    protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData, string? sql = null)
    {
        ValidateConnection();

        try
        {
            return await ExecuteWithoutBuffer(getData).ConfigureAwait(false);
        }
        catch (TimeoutException ex)
        {
            throw HandleTimeoutException(ex, sql);
        }
        catch (SqlException ex)
        {
            throw HandleSqlException("WithConnection()", sql, ex);
        }
    }

    protected T WithSynchronousConnection<T>(Func<IDbConnection, T> getData, string? sql = null)
    {
        ValidateConnection();
        try
        {
            return ExecuteSynchronousWithoutBuffer(getData);
        }
        catch (TimeoutException ex)
        {
            throw HandleTimeoutException(ex, sql);
        }
        catch (SqlException ex)
        {
            throw HandleSqlException("WithSynchronousConnection()", sql, ex);
        }
    }

    protected async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData,
        Func<TRead, Task<TResult>> process, string? sql = null)
    {
        ValidateConnection();
        try
        {
            return await ExecuteWithBuffer(getData, process).ConfigureAwait(false);
        }
        catch (TimeoutException ex)
        {
            throw HandleTimeoutException(ex, sql);
        }
        catch (SqlException ex)
        {
            throw HandleSqlException("WithConnection()", sql, ex);
        }
    }

    protected string? WrapForLike(string? val)
    {
        if (!string.IsNullOrWhiteSpace(val))
        {
            return "%" + EncodeForLike(val) + "%";
        }

        return null;
    }

    protected string EncodeForLike(string? val)
    {
        if (!string.IsNullOrWhiteSpace(val))
        {
            return val.Replace("[", "[[]").Replace("%", "[%]");
        }

        return "";
    }

    protected async Task<T?> Get<T>(string? sql, object? param = null, int timeout = DefaultCommandTimeoutInSeconds)
    {
        if (string.IsNullOrWhiteSpace(sql))
        {
            return default;
        }

        param ??= new { };

        return await WithConnection(async c =>
                                    {
                                        var response =
                                            await c.QueryAsync<T>(
                                                sql, param, null, timeout).ConfigureAwait(false);

                                        return response.FirstOrDefault();
                                    }).ConfigureAwait(false);
    }

    protected async Task<IEnumerable<T>> List<T>(string? sql, object? param = null,
        int timeout = DefaultCommandTimeoutInSeconds)
    {
        if (string.IsNullOrWhiteSpace(sql))
        {
            return [];
        }

        param ??= new { };

        return await WithConnection(async c => await c.QueryAsync<T>(sql, param, null, timeout)
                                        .ConfigureAwait(false))
            .ConfigureAwait(false);
    }

    protected async Task<T?> Insert<T>(string? sql, object? param = null, int timeout = DefaultCommandTimeoutInSeconds)
    {
        if (string.IsNullOrWhiteSpace(sql))
        {
            return default;
        }

        param ??= new { };

        return await WithConnection(async c =>
                                    {
                                        var response =
                                            await c.QueryAsync<T>(
                                                sql, param, null, timeout)
                                                .ConfigureAwait(false);

                                        return response.FirstOrDefault();
                                    }).ConfigureAwait(false);
    }

    protected async Task Execute(string? sql, object? param = null, int timeout = DefaultCommandTimeoutInSeconds)
    {
        if (string.IsNullOrWhiteSpace(sql))
        {
            return;
        }

        param ??= new { };

        await WithConnection(async c => await c.ExecuteAsync(sql, param, null, timeout).ConfigureAwait(false))
            .ConfigureAwait(false);
    }

    private DotnetWebSqlException HandleSqlException(string withConnection, string? sql, SqlException ex)
    {
        if (string.IsNullOrWhiteSpace(sql))
        {
            return new DotnetWebSqlException(connection,
                $"{GetType().FullName}.{withConnection} experienced a SQL exception (not a timeout)", "Unknown", ex);
        }

        return new DotnetWebSqlException(connection,
            $"{GetType().FullName}.{withConnection} experienced a SQL exception (not a timeout)", sql, ex);
    }

    private DotnetWebSqlException HandleTimeoutException(Exception ex, string? sql)
    {
        if (string.IsNullOrWhiteSpace(sql))
        {
            return new DotnetWebSqlException(connection, $"{GetType().FullName}.WithConnection() experienced a SQL timeout", "Unknown",
                ex);
        }

        return new DotnetWebSqlException(connection, $"{GetType().FullName}.WithConnection() experienced a SQL timeout", sql,
            ex);
    }

    private T ExecuteSynchronousWithoutBuffer<T>(Func<IDbConnection, T> getData)
    {
        using (var c = new SqlConnection(connection.ConnectionString))
        {
            c.Open();
            return getData(c);
        }
    }

    private async Task<T> ExecuteWithoutBuffer<T>(Func<IDbConnection, Task<T>> getData)
    {
        using (var connection1 = new SqlConnection(connection.ConnectionString))
        {
            await connection1.OpenAsync().ConfigureAwait(false);
            return await getData(connection1).ConfigureAwait(false);
        }
    }

    private async Task<TResult> ExecuteWithBuffer<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData,
        Func<TRead, Task<TResult>> process)
    {
        using (var connection1 = new SqlConnection(connection.ConnectionString))
        {
            await connection1.OpenAsync().ConfigureAwait(false);
            var data = await getData(connection1).ConfigureAwait(false);
            return await process(data).ConfigureAwait(false);
        }
    }

    private void ValidateConnection()
    {
        if (string.IsNullOrWhiteSpace(connection.ConnectionString))
        {
            throw new DotnetWebSqlParamException("MissingDatabaseConnection");
        }

        var builder = new SqlConnectionStringBuilder(connection.ConnectionString);

        if (string.IsNullOrWhiteSpace(builder.DataSource))
        {
            throw new DotnetWebSqlParamException("MissingDatabaseConnection-Url");
        }

        if (string.IsNullOrWhiteSpace(builder.UserID))
        {
            throw new DotnetWebSqlParamException("MissingDatabaseConnection-Username");
        }

        if (string.IsNullOrWhiteSpace(builder.Password))
        {
            throw new DotnetWebSqlParamException("MissingDatabaseConnection-Password");
        }

        if (string.IsNullOrWhiteSpace(builder.InitialCatalog))
        {
            throw new DotnetWebSqlParamException("MissingDatabaseConnection-Catalog");
        }
    }
}
