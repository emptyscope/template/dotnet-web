using DotnetWeb.Common.Library.Common.DatabaseControl;
using Microsoft.Data.SqlClient;

namespace DotnetWeb.Repository.Library.Repository;

public class SqlAzureConnection : IConnection
{
    public SqlAzureConnection(string connectionString, Guid traceId)
    {
        TraceId = traceId;
        var builder = Builder(connectionString);
        ConnectionString = builder.ConnectionString;
        Datasource = builder.DataSource;
        SqlUsername = builder.UserID;
        Catalog = builder.InitialCatalog;
        IsReadOnly = builder.ApplicationIntent == ApplicationIntent.ReadOnly;
        IsEncrypted = builder.Encrypt;
    }

    public SqlAzureConnection(IConnection connection)
    {
        TraceId = connection.TraceId;
        var builder = Builder(connection.ConnectionString);
        ConnectionString = builder.ConnectionString;
        Datasource = builder.DataSource;
        SqlUsername = builder.UserID;
        Catalog = builder.InitialCatalog;
        IsReadOnly = builder.ApplicationIntent == ApplicationIntent.ReadOnly;
        IsEncrypted = builder.Encrypt;
        TrustServerCertificate = builder.TrustServerCertificate;
    }

    public SqlAzureConnection(string? datasource, string? username, string? password, string? catalog, bool isReadOnly = true,
        bool encrypt = true)
    {
        var builder = Builder(datasource, username, password, catalog, isReadOnly, encrypt);
        ConnectionString = builder.ConnectionString;
        Datasource = builder.DataSource;
        SqlUsername = builder.UserID;
        Catalog = builder.InitialCatalog;
        IsReadOnly = builder.ApplicationIntent == ApplicationIntent.ReadOnly;
        IsEncrypted = builder.Encrypt;
        TrustServerCertificate = builder.TrustServerCertificate;
    }

    public string ConnectionString { get; init; }
    public Guid TraceId { get; init; }
    public string? Datasource { get; init; }
    public string? SqlUsername { get; init; }
    public string? Catalog { get; init; }
    public bool? IsReadOnly { get; init; }
    public bool? IsEncrypted { get; init; }
    public bool? TrustServerCertificate { get; init; }

    private static SqlConnectionStringBuilder Builder(string? connectionString) => new(connectionString);

    private static SqlConnectionStringBuilder Builder(string? datasource, string? username, string? password, string? catalog, bool isReadOnly = true, bool encrypt = true, bool trustServerCertificate = false) =>
        new()
        {
            DataSource = datasource,
            UserID = username,
            Password = password,
            InitialCatalog = catalog,
            Encrypt = encrypt,
            TrustServerCertificate = trustServerCertificate,
            ApplicationIntent = isReadOnly ? ApplicationIntent.ReadOnly : ApplicationIntent.ReadWrite
        };
}
