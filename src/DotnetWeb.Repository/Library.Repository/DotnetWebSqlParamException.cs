namespace DotnetWeb.Repository.Library.Repository;

public class DotnetWebSqlParamException(string message) : ArgumentException(message);
