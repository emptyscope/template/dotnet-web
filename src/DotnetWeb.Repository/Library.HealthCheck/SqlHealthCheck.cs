using DotnetWeb.Common.Library.Common.DatabaseControl;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotnetWeb.Repository.Library.HealthCheck;

public class SqlHealthCheck(IConnection connection) : IHealthCheck
{
    private readonly string? _connectionString = connection.ConnectionString;

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
        CancellationToken cancellationToken = default)
    {
        try
        {
            await using var sqlConnection = new SqlConnection(_connectionString);

            await sqlConnection.OpenAsync(cancellationToken).ConfigureAwait(false);

            await using var command = sqlConnection.CreateCommand();
            command.CommandText = "SELECT 1";

            await command.ExecuteScalarAsync(cancellationToken).ConfigureAwait(false);

            return HealthCheckResult.Healthy();
        }
        catch (Exception exception)
        {
            var description = context.Registration.FailureStatus.ToString();
            return HealthCheckResult.Unhealthy(description, exception);
        }
    }
}
