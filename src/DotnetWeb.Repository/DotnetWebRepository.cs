using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.DatabaseControl;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Model;
using DotnetWeb.Repository.Library.Repository;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Repository;

public class DotnetWebRepository(IConnection connection, ILogger<DotnetWebRepository> logger)
    : BaseRepository(connection), IDotnetWebRepository
{
    private readonly IConnection _connection = connection;

    public async Task<DotnetWebResponse?> Fetch(Guid id)
    {
        logger.Hit(_connection.TraceId, Guid.NewGuid());
        try
        {
            return await Get<DotnetWebResponse>(sql.SelectById, new
            {
                Key = id
            }).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            logger.Fail(e, null, _connection.TraceId);
            return null;
        }
    }

    public async Task<IEnumerable<DotnetWebResponse>> List()
    {
        logger.Hit(_connection.TraceId, Guid.NewGuid());

        try
        {
            return await List<DotnetWebResponse>(sql.Select).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            logger.Fail(e, null, _connection.TraceId);
            return [];
        }
    }
}
