using System.Collections.Concurrent;
using System.Collections.Immutable;
using System.Globalization;

namespace DotnetWeb.Common.Library.Common.CacheControl;
public class InstanceCache(IDictionary<string, Tuple<DateTime, object, Type>> cacheList, IList<string>? invalidList)
    : IInstanceCache
{
    private const int DefaultHoursToLive = 730;
    private readonly IDictionary<string, Tuple<DateTime, object, Type>> _cacheList = cacheList;
    private readonly IList<string> _invalidList = invalidList ?? [];

    public InstanceCache() : this(new ConcurrentDictionary<string, Tuple<DateTime, object, Type>>(), [])
    {
    }

    public bool Exists(string? key) => !string.IsNullOrWhiteSpace(key) && _cacheList.ContainsKey(key);

    public T? Fetch<T>(string? key)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            return default;
        }

        if (!_cacheList.TryGetValue(key, out var tuple))
        {
            return default;
        }

        if (tuple.Item1 <= DateTime.UtcNow)
        {
            Remove(key);
            return default;
        }

        return ConvertObject<T>(tuple.Item2, tuple.Item3);
    }

    public IImmutableDictionary<string, Tuple<object, Type>> All() => _cacheList
            .Select(i => new KeyValuePair<string, Tuple<object, Type>>(i.Key, new Tuple<object, Type>(i.Value.Item2, i.Value.Item3)))
            .ToImmutableDictionary();

    public IImmutableDictionary<string, Type> FindKeys(string? key)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            return new Dictionary<string, Type>().ToImmutableDictionary();
        }

        return _cacheList.Where(i => i.Key.Equals(key, StringComparison.OrdinalIgnoreCase)
                                    || i.Key.Contains(key, StringComparison.OrdinalIgnoreCase))
            .Select(i => new KeyValuePair<string, Type>(i.Key, i.Value.Item3))
            .ToImmutableDictionary();
    }

    public IImmutableDictionary<string, Type> FindValues<T>(string? val)
    {
        if (string.IsNullOrWhiteSpace(val))
        {
            return new Dictionary<string, Type>().ToImmutableDictionary();
        }

        var retVal = new List<KeyValuePair<string, Type>>();
        foreach (var keyValuePair in _cacheList)
        {
            var item = ConvertObject<T>(keyValuePair.Value.Item2, keyValuePair.Value.Item3);

            if (item == null)
            {
                continue;
            }

            if (item.Equals(val) || item.ToString()!.Contains(val))
            {
                retVal.Add(new KeyValuePair<string, Type>(keyValuePair.Key, keyValuePair.Value.Item3));
            }
        }

        return retVal.ToImmutableDictionary();
    }

    public void Add<T>(string? key, T? val, TimeSpan? expires = null)
    {
        if (string.IsNullOrWhiteSpace(key) || val == null)
        {
            return;
        }

        if (Exists(key))
        {
            _invalidList.Add(key);
        }
        else
        {
            _cacheList.Add(key,
                new Tuple<DateTime, object, Type>(
                    DateTime.UtcNow.Add(expires ?? TimeSpan.FromHours(DefaultHoursToLive)),
                    val,
                    typeof(T)));
        }
    }

    public void Remove(string? key)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            return;
        }

        try
        {
            _cacheList.Remove(key);
        }
        catch
        {
            // ignored
        }
    }

    public static InstanceCache Init(IEnumerable<KeyValuePair<string, string?>>? configuration = null)
    {
        if (configuration == null)
        {
            return new InstanceCache();
        }

        var cache = new InstanceCache();
        var invalidKeys = new List<string>();

        foreach (var keyValuePair in configuration)
        {
            if (string.IsNullOrWhiteSpace(keyValuePair.Key))
            {
                continue;
            }

            if (cache.Exists(keyValuePair.Key))
            {
                invalidKeys.Add(keyValuePair.Key);
            }
            else
            {
                cache.Add(keyValuePair.Key, keyValuePair.Value);
            }
        }

        return new InstanceCache(cache._cacheList, invalidKeys);
    }

    private static T? CastObject<T>(object? input)
    {
        if (input == null)
        {
            return default;
        }

        return (T) input;
    }

    private static T? ConvertObject<T>(object? input)
    {
        if (input == null)
        {
            return default;
        }

        return (T) Convert.ChangeType(input, typeof(T), CultureInfo.InvariantCulture);
    }

    private static T? ConvertObject<T>(object? input, Type? type)
    {
        if (type == null)
        {
            return default;
        }

        if (type == typeof(T))
        {
            return CastObject<T>(input);
        }

        try
        {
            return ConvertObject<T>(input);
        }
        catch
        {
            return default;
        }
    }
}

