using System.Collections.Immutable;

namespace DotnetWeb.Common.Library.Common.CacheControl;

public interface IInstanceCache
{
    bool Exists(string key);
    T? Fetch<T>(string key);
    IImmutableDictionary<string, Tuple<object, Type>> All();
    IImmutableDictionary<string, Type> FindKeys(string key);
    IImmutableDictionary<string, Type> FindValues<T>(string val);
    void Add<T>(string key, T val, TimeSpan? expires = null);
    void Remove(string key);
}
