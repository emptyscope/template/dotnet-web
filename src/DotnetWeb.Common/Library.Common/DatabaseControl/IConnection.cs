namespace DotnetWeb.Common.Library.Common.DatabaseControl;

public interface IConnection
{
    string ConnectionString { get; init; }
    Guid TraceId { get; init; }
    string? Datasource { get; init; }
    string? SqlUsername { get; init; }
    string? Catalog { get; init; }
    bool? IsReadOnly { get; init; }
    bool? IsEncrypted { get; init; }
    bool? TrustServerCertificate { get; init; }

}
