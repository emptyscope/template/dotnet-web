namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public interface ICustomHostingEnvironmentMapper
{
    ICustomHostingEnvironment Fetch(string environmentVariableKey);
}
