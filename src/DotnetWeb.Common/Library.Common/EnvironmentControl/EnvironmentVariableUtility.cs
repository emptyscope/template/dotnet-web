namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public class EnvironmentVariableUtility : IEnvironmentVariableUtility
{
    public string Fetch(string key)
    {
        var machineVariable = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Machine);

        if (!string.IsNullOrWhiteSpace(machineVariable))
        {
            return machineVariable;
        }

        var userVariable = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.User);

        if (!string.IsNullOrWhiteSpace(userVariable))
        {
            return userVariable;
        }

        var processVariable = Environment.GetEnvironmentVariable(key, EnvironmentVariableTarget.Process);

        if (!string.IsNullOrWhiteSpace(processVariable))
        {
            return processVariable;
        }

        throw new KeyNotFoundException($"Missing Key: {key}");
    }

    public string Fetch(Environments environment)
    {
        var currentEnvironment = Enum.GetName(environment);

        if (!string.IsNullOrWhiteSpace(currentEnvironment))
        {
            return currentEnvironment;
        }

        var unknownEnvironment = Enum.GetName(Environments.Unknown);

        if (!string.IsNullOrWhiteSpace(unknownEnvironment))
        {
            return unknownEnvironment;
        }

        throw new ArgumentException("Environment Argument Not Found");
    }

    public string SafeFetch(string key, string? defaultValue = null)
    {
        try
        {
            return Fetch(key);
        }
        catch (KeyNotFoundException)
        {
            return defaultValue ?? string.Empty;
        }
    }

    public void Create(string key, string value) => Environment.SetEnvironmentVariable(key, null,
            EnvironmentVariableTarget.Process);
}
