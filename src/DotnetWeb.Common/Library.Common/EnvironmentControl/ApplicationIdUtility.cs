namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public class ApplicationIdUtility : IApplicationIdUtility
{
    public string GetApplicationId(string applicationId)
    {
        if (string.IsNullOrWhiteSpace(applicationId))
        {
            return applicationId;
        }

        applicationId = applicationId.ToLowerInvariant();

        if (applicationId.Contains("production", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("production", string.Empty);
        }

        if (applicationId.Contains("prod", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("prod", string.Empty);
        }

        if (applicationId.Contains("staging", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("staging", string.Empty);
        }

        if (applicationId.Contains("qa", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("qa", string.Empty);
        }

        if (applicationId.Contains("build", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("build", string.Empty);
        }

        if (applicationId.Contains("local", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("local", string.Empty);
        }

        if (applicationId.Contains("test", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("test", string.Empty);
        }

        if (applicationId.Contains("demo", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("demo", string.Empty);
        }

        if (applicationId.Contains("uat", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("uat", string.Empty);
        }

        if (applicationId.Contains("development", StringComparison.OrdinalIgnoreCase))
        {
            return applicationId.Replace("development", string.Empty);
        }

        return applicationId.Contains("dev", StringComparison.OrdinalIgnoreCase) ? applicationId.Replace("dev", string.Empty) : applicationId;
    }
}
