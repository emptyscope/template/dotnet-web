namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public class CustomHostingEnvironment(string? applicationName, Environments environmentName)
    : ICustomHostingEnvironment
{
    public Environments Environment { get; init; } = environmentName;

    public string ApplicationName { get; init; } = applicationName ?? string.Empty;
}
