namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public class CustomHostingEnvironmentMapper(
    string? applicationName,
    IEnvironmentVariableUtility environmentVariableUtility)
    : ICustomHostingEnvironmentMapper
{
    private readonly string _applicationName = applicationName ?? string.Empty;

    public ICustomHostingEnvironment Fetch(string environmentVariableKey)
    {
        var environmentName = environmentVariableUtility.SafeFetch(environmentVariableKey);

        if (string.IsNullOrWhiteSpace(environmentName))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Unknown);
        }

        return GetCustomHostingEnvironment(environmentName.ToLowerInvariant().Trim());
    }

    private CustomHostingEnvironment GetCustomHostingEnvironment(string environmentName)
    {
        if (DotnetWebConstants.Local.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Local);
        }

        if (DotnetWebConstants.Build.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Build);
        }

        if (DotnetWebConstants.Development.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Development);
        }

        if (DotnetWebConstants.Qa.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Qa);
        }

        if (DotnetWebConstants.Demo.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Demo);
        }

        if (DotnetWebConstants.Uat.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Uat);
        }

        if (DotnetWebConstants.Staging.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Staging);
        }

        if (DotnetWebConstants.Production.Equals(environmentName, StringComparison.OrdinalIgnoreCase))
        {
            return new CustomHostingEnvironment(_applicationName, Environments.Production);
        }

        return new CustomHostingEnvironment(_applicationName, Environments.Unknown);
    }
}
