using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public enum Environments
{
    [Display(Name = "Unknown")]
    [Description("")]
    Unknown = 0,

    [Display(Name = "Production")]
    [Description("")]
    Production = 1,

    [Display(Name = "Staging")]
    [Description("")]
    Staging = 2,

    [Display(Name = "UAT")]
    [Description("")]
    Uat = 3,

    [Display(Name = "Demo")]
    [Description("")]
    Demo = 4,

    [Display(Name = "QA")]
    [Description("")]
    Qa = 5,

    [Display(Name = "Development")]
    [Description("")]
    Development = 6,

    [Display(Name = "Build")]
    [Description("")]
    Build = 7,

    [Display(Name = "Local")]
    [Description("")]
    Local = 8
}
