namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public interface IEnvironmentVariableUtility
{
    string Fetch(string key);
    string Fetch(Environments environment);
    string? SafeFetch(string key, string? defaultValue = null);
    void Create(string key, string value);
}
