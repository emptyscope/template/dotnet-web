namespace DotnetWeb.Common.Library.Common.EnvironmentControl;

public interface ICustomHostingEnvironment
{
    Environments Environment { get; init; }
    string ApplicationName { get; init; }
}
