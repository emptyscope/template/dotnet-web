namespace DotnetWeb.Common.Library.Common.MessageControl;

public record PaginationResponse
{
    private readonly int _index;
    private readonly int _pageSize;
    private readonly int _count;

    public PaginationResponse(int index, int pageSize, int count)
    {
        _index = index;
        _pageSize = pageSize;
        _count = count;
    }

    public int PageIndex => _index == 0 ? 1 : _index;

    public int PageSize => _pageSize == 0 ? 1 : _pageSize;

    public int TotalCount => _count == 0 ? 1 : _count;

    public int TotalPages
    {
        get
        {
            var totalPages = TotalCount / PageSize;
            if (totalPages % PageSize > 0)
            {
                return totalPages + 1;
            }

            return totalPages;
        }
    }

    public bool HasPreviousPage => PageIndex > 1;

    public bool HasNextPage => PageIndex + 1 <= TotalPages;
}
