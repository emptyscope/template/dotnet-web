namespace DotnetWeb.Common.Library.Common.MessageControl;

public enum MessageServiceTypes
{
    Unknown,
    Api,
    Portal,
    Batch
}
