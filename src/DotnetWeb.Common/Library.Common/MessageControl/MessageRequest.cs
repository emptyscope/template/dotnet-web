namespace DotnetWeb.Common.Library.Common.MessageControl;

public sealed record MessageRequest
{
    private readonly Guid? _messageId;
    private readonly Guid? _transactionId;
    private readonly Guid? _callerId;
    private readonly string? _clientMessageId;
    private readonly string? _callerAppId;
    private readonly string? _appId;
    private readonly string? _ipAddress;
    private readonly DateTimeOffset? _startTime;
    private readonly bool _noCache;
    private readonly bool _isCached;
    private readonly string? _groupId;
    private readonly MessageServiceTypes? _serviceType;
    private string? _searchKey;
    private readonly long? _batchId;
    private readonly bool _excludeFromBilling;

    public MessageRequest(Guid? messageId, Guid? callerId, string? clientMessageId,
        string? callerAppId, string? appId, string? ipAddress, DateTimeOffset? startTime, bool noCache, bool isCached,
        string? groupId, MessageServiceTypes? serviceType, string? searchKey, bool excludeFromBilling,
        Guid? transactionId, long? batchId)
    {
        _messageId = messageId;
        _callerId = callerId;
        _clientMessageId = clientMessageId;
        _callerAppId = callerAppId;
        _appId = appId;
        _ipAddress = ipAddress;
        _startTime = startTime;
        _noCache = noCache;
        _isCached = isCached;
        _groupId = groupId;
        _serviceType = serviceType;
        _searchKey = searchKey;
        _excludeFromBilling = excludeFromBilling;
        _transactionId = transactionId;
        _batchId = batchId;
    }

    public bool NoCache() => _noCache;

    public bool IsCached() => _isCached;

    public Guid? MessageId() => _messageId;

    public Guid? TransactionId() => _transactionId;

    public Guid? CallerId() => _callerId;

    public string? ClientMessageId() => _clientMessageId;

    public string? CallerAppId() => _callerAppId;

    public string? AppId() => _appId;

    public string? IpAddress() => _ipAddress;

    public DateTimeOffset? StartTime() => _startTime;

    public string? GroupId() => _groupId;

    public string ServiceType() => Enum.GetName(_serviceType ?? MessageServiceTypes.Unknown)?.ToLowerInvariant() ?? DotnetWebConstants.Unknown;

    public MessageServiceTypes ServiceTypeAsEnum() => _serviceType ?? MessageServiceTypes.Unknown;

    public bool IsPortal() => _serviceType == MessageServiceTypes.Portal;

    public bool IsApi() => _serviceType == MessageServiceTypes.Api;

    public bool IsBatch() => _serviceType == MessageServiceTypes.Batch;

    public string? SearchKey() => _searchKey;

    public void SearchKey(string searchKey) => _searchKey = searchKey;

    public bool ExcludeFromBilling() => _excludeFromBilling;

    public long? BatchId() => _batchId;
}
