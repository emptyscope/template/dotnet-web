namespace DotnetWeb.Common.Library.Common.MessageControl;

public record PaginationRequest
{
    private readonly int _index;
    private readonly int _size;

    public PaginationRequest(int index, int size)
    {
        _index = index;
        _size = size;
    }

    public int PageIndex => _index == 0 ? 1 : _index;

    public int PageSize => _size == 0 ? 1 : _size;
}
