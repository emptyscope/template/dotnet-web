using System.Diagnostics.CodeAnalysis;

namespace DotnetWeb.Common.Library.Common.FileControl;

[SuppressMessage("ReSharper", "InconsistentNaming")]
public enum FileTypeExtensions
{
    UNKNOWN,
    AAC,
    ABW,
    APNG,
    ARC,
    ASTC,
    AVI,
    AVIF,
    AZW,
    BIN,
    BMP,
    BZ,
    BZ2,
    CDA,
    CSH,
    CSS,
    CSV,
    DNG,
    DOC,
    DOCX,
    EOT,
    EPUB,
    GIF,
    GZ,
    HEIF,
    HTM,
    HTML,
    ICO,
    ICS,
    JAR,
    JPEG,
    JPG,
    JS,
    JSON,
    JSONLD,
    KTX,
    MID,
    MIDI,
    MJS,
    MP3,
    MP4,
    MPEG,
    MPKG,
    ODP,
    ODS,
    ODT,
    OGA,
    OGV,
    OGX,
    OPUS,
    OTF,
    PDF,
    PHP,
    PKM,
    PNG,
    PPT,
    PPTX,
    QR,
    RAR,
    RTF,
    SEVENZ,
    SH,
    SVG,
    TAR,
    THREEG2,
    THREEGP,
    TIF,
    TIFF,
    TS,
    TTF,
    TXT,
    VSD,
    WAV,
    WBMP,
    WEBA,
    WEBM,
    WEBP,
    WOFF,
    WOFF2,
    XHTML,
    XLS,
    XLSX,
    XML,
    XUL,
    ZIP
}
