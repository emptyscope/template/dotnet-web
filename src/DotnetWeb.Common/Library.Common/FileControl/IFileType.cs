namespace DotnetWeb.Common.Library.Common.FileControl;

public interface IFileType
{
    Guid Id { get; init; }
    string Extension { get; init; }
    string MimeType { get; init; }
    string Description { get; init; }
    FileTypeExtensions Extensions { get; init; }
}
