namespace DotnetWeb.Common.Library.Common.FileControl;

public class FileTypes : IFileTypes
{
    public static FileTypes Instance() => new();

    public FileType ById(Guid? id, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN)
    {
        if (id == null)
        {
            return FileTypeConstants.Unknown;
        }

        var byId = All()
            .FirstOrDefault(x => x.Id.Equals(id)) ?? FileTypeConstants.Unknown;

        return byId.Extensions != FileTypeExtensions.UNKNOWN ? byId : ByEnum(defaultFileType);
    }

    public FileType ByUri(Uri? uri, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN)
    {
        if (uri == null)
        {
            return FileTypeConstants.Unknown;
        }

        var fixedUri = RemoveTrailingSlash(uri).AbsoluteUri;

        var byUri = All()
            .FirstOrDefault(x => fixedUri.EndsWith(x.Extension, StringComparison.OrdinalIgnoreCase)) ?? FileTypeConstants.Unknown;

        return byUri.Extensions != FileTypeExtensions.UNKNOWN ? byUri : ByEnum(defaultFileType);
    }

    public FileType ByExtension(string? extension, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN)
    {
        if (string.IsNullOrWhiteSpace(extension))
        {
            return FileTypeConstants.Unknown;
        }

        var byExtension = All()
            .FirstOrDefault(x =>
                x.Extension.Equals(Dot(extension), StringComparison.OrdinalIgnoreCase)) ?? FileTypeConstants.Unknown;

        return byExtension.Extensions != FileTypeExtensions.UNKNOWN ? byExtension : ByEnum(defaultFileType);
    }

    public FileType ByMimeType(string? mimeType, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN)
    {
        if (string.IsNullOrWhiteSpace(mimeType))
        {
            return FileTypeConstants.Unknown;
        }

        var byMimeType = All()
            .FirstOrDefault(x => mimeType.Equals(x.MimeType, StringComparison.OrdinalIgnoreCase)) ?? FileTypeConstants.Unknown;

        return byMimeType.Extensions != FileTypeExtensions.UNKNOWN ? byMimeType : ByEnum(defaultFileType);
    }

    public FileType ByEnum(FileTypeExtensions? fileType, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN)
    {
        if (fileType == null)
        {
            return FileTypeConstants.Unknown;
        }

        var byEnum = All()
            .FirstOrDefault(x => x.Extensions == fileType) ?? FileTypeConstants.Unknown;

        return byEnum.Extensions != FileTypeExtensions.UNKNOWN ? byEnum : ByEnum(defaultFileType);
    }

    public FileType Fetch(FileTypeExtensions? fileTypeEnum, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN)
    {
        if (fileTypeEnum == null)
        {
            return ByEnum(defaultFileType);
        }

        var byEnum = ByEnum(fileTypeEnum);
        return byEnum.Extensions != FileTypeExtensions.UNKNOWN ? byEnum : ByEnum(defaultFileType);
    }

    public FileType Fetch(FileType? fileType, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN) => Fetch(fileType?.Extensions, defaultFileType);

    private static Uri RemoveTrailingSlash(Uri uri)
    {
        var str = uri.ToString();
        return str.EndsWith('/') ? new Uri(str [..^1]) : uri;
    }

    private static string? Dot(string extension)
    {
        if (string.IsNullOrWhiteSpace(extension) || extension.Equals(".", StringComparison.OrdinalIgnoreCase))
        {
            return null;
        }

        if (extension.StartsWith('.'))
        {
            return extension;
        }

        return "." + extension;
    }

    private static List<FileType> All() =>
    [
        FileTypeConstants.Unknown,
        FileTypeConstants.Aac,
        FileTypeConstants.Abw,
        FileTypeConstants.Apng,
        FileTypeConstants.Arc,
        FileTypeConstants.Avif,
        FileTypeConstants.Avi,
        FileTypeConstants.Azw,
        FileTypeConstants.Bin,
        FileTypeConstants.Bmp,
        FileTypeConstants.Bz,
        FileTypeConstants.Bz2,
        FileTypeConstants.Cda,
        FileTypeConstants.Csh,
        FileTypeConstants.Css,
        FileTypeConstants.Csv,
        FileTypeConstants.Doc,
        FileTypeConstants.Docx,
        FileTypeConstants.Eot,
        FileTypeConstants.Epub,
        FileTypeConstants.Gz,
        FileTypeConstants.Gif,
        FileTypeConstants.Htm,
        FileTypeConstants.Html,
        FileTypeConstants.Ico,
        FileTypeConstants.Ics,
        FileTypeConstants.Jar,
        FileTypeConstants.Jpeg,
        FileTypeConstants.Jpg,
        FileTypeConstants.Js,
        FileTypeConstants.Json,
        FileTypeConstants.Jsonld,
        FileTypeConstants.Mid,
        FileTypeConstants.Midi,
        FileTypeConstants.Mjs,
        FileTypeConstants.Mp3,
        FileTypeConstants.Mp4,
        FileTypeConstants.Mpeg,
        FileTypeConstants.Mpkg,
        FileTypeConstants.Odp,
        FileTypeConstants.Ods,
        FileTypeConstants.Odt,
        FileTypeConstants.Oga,
        FileTypeConstants.Ogv,
        FileTypeConstants.Ogx,
        FileTypeConstants.Opus,
        FileTypeConstants.Otf,
        FileTypeConstants.Png,
        FileTypeConstants.Pdf,
        FileTypeConstants.Php,
        FileTypeConstants.Ppt,
        FileTypeConstants.Pptx,
        FileTypeConstants.Qr,
        FileTypeConstants.Rar,
        FileTypeConstants.Rtf,
        FileTypeConstants.Sh,
        FileTypeConstants.Svg,
        FileTypeConstants.Tar,
        FileTypeConstants.Tif,
        FileTypeConstants.Tiff,
        FileTypeConstants.Ts,
        FileTypeConstants.Ttf,
        FileTypeConstants.Txt,
        FileTypeConstants.Vsd,
        FileTypeConstants.Wav,
        FileTypeConstants.Weba,
        FileTypeConstants.Webm,
        FileTypeConstants.Webp,
        FileTypeConstants.Woff,
        FileTypeConstants.Woff2,
        FileTypeConstants.Xhtml,
        FileTypeConstants.Xls,
        FileTypeConstants.Xlsx,
        FileTypeConstants.Xml,
        FileTypeConstants.Xul,
        FileTypeConstants.Zip,
        FileTypeConstants.Threegp,
        FileTypeConstants.Threeg2,
        FileTypeConstants.Sevenz
    ];

    internal static class FileTypeConstants
    {
        #region Public-FileType-Constants-Region

        public static FileType Unknown { get; }

        static FileTypeConstants() =>
            Unknown = new FileType(new Guid("00000000-0000-0000-0000-000000000000"), ".unknown",
                "application/octet-stream", "Unknown File Type");

        public static FileType Aac => new(new Guid("076ceb56-b701-405b-bc3c-2b07af926230"), ".aac",
            "audio/aac", "AAC audio", FileTypeExtensions.AAC);

        public static FileType Abw => new(new Guid("71ae006c-eb83-429e-b466-a987826499f3"), ".abw",
            "application/x-abiword", "AbiWord document", FileTypeExtensions.ABW);

        public static FileType Apng => new(new Guid("6c5e0364-e6cd-43a0-bce0-bfdf506ed0cf"), ".apng",
            "image/apng", "Animated Portable Network Graphics (APNG) image", FileTypeExtensions.APNG);

        public static FileType Arc => new(new Guid("2667a3fa-1ee9-4f83-9848-e60d7329ba68"), ".arc",
            "application/x-freearc", "Archive document (multiple files embedded)", FileTypeExtensions.ARC);

        public static FileType Avif => new(new Guid("01b1e3b4-e69c-45de-8e5b-24db98e12c6c"), ".avif",
            "image/avif", "AVIF image", FileTypeExtensions.AVIF);

        public static FileType Avi => new(new Guid("cea9f294-dc2b-4e49-96e4-61f31ba68fa3"), ".avi",
            "video/x-msvideo", "AVI Audio Video Interleave", FileTypeExtensions.AVI);

        public static FileType Azw => new(new Guid("cd6cbe1e-9f53-45db-b8fa-9a31dd718e27"), ".azw",
            "application/vnd.amazon.ebook", "Amazon Kindle eBook format", FileTypeExtensions.AZW);

        public static FileType Bin => new(new Guid("52bc5bfc-ed5d-4260-a919-c670f889b391"), ".bin",
            "application/octet-stream", "Any kind of binary data", FileTypeExtensions.BIN);

        public static FileType Bmp => new(new Guid("fdd5d07c-077d-4757-b309-b3f8b177aff6"), ".bmp",
            "image/bmp", "Windows OS/2 Bitmap Graphics", FileTypeExtensions.BMP);

        public static FileType Bz => new(new Guid("12dc0338-4f56-4a57-9fc9-8a6afa6cc203"), ".bz",
            "application/x-bzip", "BZip archive", FileTypeExtensions.BZ);

        public static FileType Bz2 => new(new Guid("0b50c9c6-471b-45e6-b600-6f6b05ee9802"), ".bz2",
            "application/x-bzip2", "BZip2 archive", FileTypeExtensions.BZ2);

        public static FileType Cda => new(new Guid("8f44250e-a780-41cc-a2c7-4abfa466c8ff"), ".cda",
            "application/x-cdf", "CD audio", FileTypeExtensions.CDA);

        public static FileType Csh => new(new Guid("90dda346-8c10-4d88-b455-01a3ae40f85d"), ".csh",
            "application/x-csh", "C-Shell script", FileTypeExtensions.CSH);

        public static FileType Css => new(new Guid("d92d12cb-d7ac-4b30-be6d-2837bc15a210"), ".css", "text/css",
            "Cascading Style Sheets (CSS)", FileTypeExtensions.CSS);

        public static FileType Csv => new(new Guid("54beac2d-2119-49df-a66b-d572122986c7"), ".csv", "text/csv",
            "Comma-separated values (CSV)", FileTypeExtensions.CSV);

        public static FileType Doc => new(new Guid("37403a51-bcc3-4c9f-9343-8c110feb523e"), ".doc",
            "application/msword", "Microsoft Word", FileTypeExtensions.DOC);

        public static FileType Docx => new(new Guid("f102d411-cedd-4f52-b128-d14c81a2dd4d"), ".docx",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "Microsoft Word (OpenXML)",
            FileTypeExtensions.DOCX);

        public static FileType Eot => new(new Guid("d00f28fd-d265-498b-81b5-a2ead342cc2e"), ".eot",
            "application/vnd.ms-fontobject", "MS Embedded OpenType fonts", FileTypeExtensions.EOT);

        public static FileType Epub => new(new Guid("38b69698-dd13-482d-bde4-bd24a4a7b7b6"), ".epub",
            "application/epub+zip", "Electronic publication (EPUB)", FileTypeExtensions.EPUB);

        public static FileType Gz => new(new Guid("4ebd6cbc-07bd-4743-8a9a-00b2496d8f02"), ".gz",
            "application/gzip", "GZip Compressed Archive", FileTypeExtensions.GZ);

        public static FileType Gif => new(new Guid("b074649d-b00d-4e1d-aae3-05a30897eb9e"), ".gif",
            "image/gif", "Graphics Interchange Format (GIF)", FileTypeExtensions.GIF);

        public static FileType Htm => new(new Guid("e9dcf3b8-8f56-44e6-87d9-d51c9a561ac2"), ".htm",
            "text/html", "HyperText Markup Language (HTML)", FileTypeExtensions.HTM);

        public static FileType Html => new(new Guid("06bb5ad9-7c78-45fc-8d94-b95b1851fe96"), ".html",
            "text/html", "HyperText Markup Language (HTML)", FileTypeExtensions.HTML);

        public static FileType Ico => new(new Guid("ffa97b08-eccc-4e22-a982-d53a68767ed4"), ".ico",
            "image/vnd.microsoft.icon", "Icon format", FileTypeExtensions.ICO);

        public static FileType Ics => new(new Guid("1851bbf1-218b-4a79-b744-57467dcb783a"), ".ics",
            "text/calendar", "iCalendar format", FileTypeExtensions.ICS);

        public static FileType Jar => new(new Guid("9309f5c4-32c7-48f2-a0d5-5d818e2500fa"), ".jar",
            "application/java-archive", "Java Archive (JAR)", FileTypeExtensions.JAR);

        public static FileType Jpeg => new(new Guid("7e5a945d-2cbb-47b2-a7ce-23c849e0d165"), ".jpeg",
            "image/jpeg", "JPEG images", FileTypeExtensions.JPEG);

        public static FileType Jpg => new(new Guid("9bb92b7a-f991-4fc1-8297-332e2c88c8d2"), ".jpg",
            "image/jpg", "JPEG images", FileTypeExtensions.JPG);

        public static FileType Js => new(new Guid("1727ce93-0fff-485f-b2b4-669172ed3de0"), ".js",
            "text/javascript (Specifications: HTML and RFC 9239)", "JavaScript", FileTypeExtensions.JS);

        public static FileType Json => new(new Guid("b90854af-5bfa-4521-a6cf-760589a4d159"), ".json",
            "application/json", "JSON format", FileTypeExtensions.JSON);

        public static FileType Jsonld => new(new Guid("33041ddf-bc37-46e4-b157-73f6abc29e31"), ".jsonld",
            "application/ld+json", "JSON-LD format", FileTypeExtensions.JSONLD);

        public static FileType Mid => new(new Guid("1db64a62-fa53-40a7-8a3d-d896e175fc11"), ".mid",
            "audio/midi, audio/x-midi", "Musical Instrument Digital Interface (MIDI)", FileTypeExtensions.MID);

        public static FileType Midi => new(new Guid("c8598249-4fc3-48a7-8fd2-794da396380d"), ".midi",
            "audio/midi, audio/x-midi", "Musical Instrument Digital Interface (MIDI)", FileTypeExtensions.MIDI);

        public static FileType Mjs => new(new Guid("dc85f5a4-5d3a-4ca7-aa91-f7cb8e7c7fd6"), ".mjs",
            "text/javascript", "JavaScript module", FileTypeExtensions.MJS);

        public static FileType Mp3 => new(new Guid("1620a3ab-1c6a-4810-959f-b47cfbfe8c12"), ".mp3",
            "audio/mpeg", "MP3 audio", FileTypeExtensions.MP3);

        public static FileType Mp4 => new(new Guid("7f0aed66-46a4-4d1e-ae75-77cb13ffd778"), ".mp4",
            "video/mp4", "MP4 video", FileTypeExtensions.MP4);

        public static FileType Mpeg => new(new Guid("382e8fa8-2c60-4a11-8497-fe6a44279b9c"), ".mpeg",
            "video/mpeg", "MPEG Video", FileTypeExtensions.MPEG);

        public static FileType Mpkg => new(new Guid("c5911325-d06f-4441-8131-9cfb4c70c5a5"), ".mpkg",
            "application/vnd.apple.installer+xml", "Apple Installer Package", FileTypeExtensions.MPKG);

        public static FileType Odp => new(new Guid("ef2ce7f9-075b-429c-8369-f7112fa2b6b5"), ".odp",
            "application/vnd.oasis.opendocument.presentation", "OpenDocument presentation document", FileTypeExtensions.ODP);

        public static FileType Ods => new(new Guid("42c902df-4c48-4837-86b6-cb4d86ed289f"), ".ods",
            "application/vnd.oasis.opendocument.spreadsheet", "OpenDocument spreadsheet document", FileTypeExtensions.ODS);

        public static FileType Odt => new(new Guid("0a0a893c-3f15-43f5-94b2-04dacd3e8fd2"), ".odt",
            "application/vnd.oasis.opendocument.text", "OpenDocument text document", FileTypeExtensions.ODT);

        public static FileType Oga => new(new Guid("c0ae4a32-d670-4fba-977d-776518381f6f"), ".oga",
            "audio/ogg", "OGG audio", FileTypeExtensions.OGA);

        public static FileType Ogv => new(new Guid("c712eff1-ccc7-4074-9d7e-9c944081443f"), ".ogv",
            "video/ogg", "OGG video", FileTypeExtensions.OGV);

        public static FileType Ogx => new(new Guid("b207d19e-b022-4e0a-bbc4-19d1beecef9a"), ".ogx",
            "application/ogg", "OGG", FileTypeExtensions.OGX);

        public static FileType Opus => new(new Guid("16219453-677e-4651-bdb3-e8565eb62356"), ".opus",
            "audio/opus", "Opus audio", FileTypeExtensions.OPUS);

        public static FileType Otf => new(new Guid("a38ed34f-f25e-496e-9ad1-b848bfddfc35"), ".otf", "font/otf",
            "OpenType font", FileTypeExtensions.OTF);

        public static FileType Png => new(new Guid("3e0e6e05-a09a-4f06-89a0-06acd14b3af3"), ".png",
            "image/png", "Portable Network Graphics", FileTypeExtensions.PNG);

        public static FileType Pdf => new(new Guid("d6bf16d4-01c9-4d5d-bc4a-2ce0b96d6cc9"), ".pdf",
            "application/pdf", "Adobe Portable Document Format (PDF)", FileTypeExtensions.PDF);

        public static FileType Php => new(new Guid("3e2812df-207b-461d-9c6d-84b229b731de"), ".php",
            "application/x-httpd-php", "Hypertext Preprocessor (Personal Home Page)", FileTypeExtensions.PHP);

        public static FileType Ppt => new(new Guid("cf77f017-abbd-4241-b93a-f2dfd82e1136"), ".ppt",
            "application/vnd.ms-powerpoint", "Microsoft PowerPoint", FileTypeExtensions.PPT);

        public static FileType Pptx => new(new Guid("7ca914da-7960-48ad-a865-26049d3b3bd4"), ".pptx",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "Microsoft PowerPoint (OpenXML)", FileTypeExtensions.PPTX);

        public static FileType Qr => new(new Guid("2b4186d4-3798-4168-bf92-1377d28f417d"), ".png", "image/qr",
            "QR code", FileTypeExtensions.QR);

        public static FileType Rar => new(new Guid("5080917f-f877-4360-82f9-0e08eb1aed79"), ".rar",
            "application/vnd.rar", "RAR archive", FileTypeExtensions.RAR);

        public static FileType Rtf => new(new Guid("2fd90c79-5915-4e37-ba4c-d609d5c02b97"), ".rtf",
            "application/rtf", "Rich Text Format (RTF)", FileTypeExtensions.RTF);

        public static FileType Sh => new(new Guid("5af6af56-0955-4a2b-ab6c-7373baa653e7"), ".sh",
            "application/x-sh", "Bourne shell script", FileTypeExtensions.SH);

        public static FileType Svg => new(new Guid("a0954cd6-248b-4cbc-85a9-42605d82cb41"), ".svg",
            "image/svg+xml", "Scalable Vector Graphics (SVG)", FileTypeExtensions.SVG);

        public static FileType Tar => new(new Guid("7c6f46f0-7a5c-45ae-b200-0d10bd3d90a6"), ".tar",
            "application/x-tar", "Tape Archive (TAR", FileTypeExtensions.TAR);

        public static FileType Tif => new(new Guid("6aa98abf-4336-4e20-a30b-0d3c7ef95c30"), ".tif",
            "image/tiff", "Tagged Image File Format (TIFF)", FileTypeExtensions.TIF);

        public static FileType Tiff => new(new Guid("4129a757-c22e-48f1-abcb-5987b0da23e9"), ".tiff",
            "image/tiff", "Tagged Image File Format (TIFF)", FileTypeExtensions.TIFF);

        public static FileType Ts => new(new Guid("73efebbf-9da7-469e-bb81-c001da8278dd"), ".ts", "video/mp2t",
            "MPEG transport stream", FileTypeExtensions.TS);

        public static FileType Ttf => new(new Guid("97b4274e-c575-47b8-9843-368b85eca6ed"), ".ttf", "font/ttf",
            "TrueType Font", FileTypeExtensions.TTF);

        public static FileType Txt => new(new Guid("e1b562bc-3480-4b38-b0a8-91ca1884e774"), ".txt",
            "text/plain", "Text, (generally ASCII or ISO 8859-n)", FileTypeExtensions.TXT);

        public static FileType Vsd => new(new Guid("0a718449-2162-46e6-b918-0138657b3503"), ".vsd",
            "application/vnd.visio", "Microsoft Visio", FileTypeExtensions.VSD);

        public static FileType Wav => new(new Guid("b5a9a2e9-d73b-473e-91b1-565aeaa07280"), ".wav",
            "audio/wav", "Waveform Audio Format", FileTypeExtensions.WAV);

        public static FileType Weba => new(new Guid("51346611-a765-46a7-9f20-a4113aa84181"), ".weba",
            "audio/webm", "WEBM audio", FileTypeExtensions.WEBA);

        public static FileType Webm => new(new Guid("28d85695-44a2-4bbd-90a0-65c9fa26c738"), ".webm",
            "video/webm", "WEBM video", FileTypeExtensions.WEBM);

        public static FileType Webp => new(new Guid("d69366b4-e21a-487d-88de-d3d3e2525384"), ".webp",
            "image/webp", "WEBP image", FileTypeExtensions.WEBP);

        public static FileType Woff => new(new Guid("331c476f-9880-4a14-a616-6613e1151aaa"), ".woff",
            "font/woff", "Web Open Font Format (WOFF)", FileTypeExtensions.WOFF);

        public static FileType Woff2 => new(new Guid("55fb5489-1d13-4769-93df-82d4df21c4be"), ".woff2",
            "font/woff2", "Web Open Font Format (WOFF)", FileTypeExtensions.WOFF2);

        public static FileType Xhtml => new(new Guid("84598458-6a2b-40af-9bd6-bdab883fdb60"), ".xhtml",
            "application/xhtml+xml", "XHTML", FileTypeExtensions.XHTML);

        public static FileType Xls => new(new Guid("187f675f-cdd2-4c3a-9a7f-114d1fdbf452"), ".xls",
            "application/vnd.ms-excel", "Microsoft Excel", FileTypeExtensions.XLS);

        public static FileType Xlsx => new(new Guid("ababe1d3-9d18-43e7-9a67-40715e1ad9c7"), ".xlsx",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Microsoft Excel (OpenXML)",
            FileTypeExtensions.XLSX);

        public static FileType Xml => new(new Guid("f86ad8a1-f78e-4729-b5c2-68a4cdcf68c1"), ".xml",
            "application/xml",
            "XML contents are meant to be interpreted. For instance, an Atom feed is application/atom+xml, but application/xml serves as a valid default.",
            FileTypeExtensions.XML);

        public static FileType Xul => new(new Guid("8bf047d5-dbec-4fc7-b150-2917ecbc99fc"), ".xul",
            "application/vnd.mozilla.xul+xml", "XUL", FileTypeExtensions.XUL);

        public static FileType Zip => new(new Guid("4b2c955b-2554-498d-b7ac-c84c84176936"), ".zip",
            "application/zip is the standard, but beware that Windows uploads .zip with MIME type application/x-zip-compressed.",
            "ZIP archive", FileTypeExtensions.ZIP);

        public static FileType Threegp => new(new Guid("2aa054d6-e562-4080-a260-b3e13b7af1af"), ".3gp",
            "video/3gpp; audio/3gpp if it doesn't contain video", "3GPP audio/video container", FileTypeExtensions.THREEGP);

        public static FileType Threeg2 => new(new Guid("322193dd-f2b0-43a3-b351-be2c9edfb82d"), ".3g2",
            "video/3gpp2; audio/3gpp2 if it doesn't contain video", "3GPP2 audio/video container",
            FileTypeExtensions.THREEG2);

        public static FileType Sevenz => new(new Guid("710cd3c5-71e7-421e-ba81-04f396e7cc7b"), ".7z",
            "application/x-7z-compressed", "7-zip archive", FileTypeExtensions.SEVENZ);

        #endregion
    }
}
