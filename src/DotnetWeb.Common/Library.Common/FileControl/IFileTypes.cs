namespace DotnetWeb.Common.Library.Common.FileControl;

public interface IFileTypes
{
    FileType ById(Guid? id, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
    FileType ByUri(Uri? uri, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
    FileType ByExtension(string? extension, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
    FileType ByMimeType(string? mimeType, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
    FileType ByEnum(FileTypeExtensions? fileType, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
    FileType Fetch(FileTypeExtensions? fileTypeEnum, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
    FileType Fetch(FileType? fileType, FileTypeExtensions? defaultFileType = FileTypeExtensions.UNKNOWN);
}
