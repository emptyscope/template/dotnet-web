namespace DotnetWeb.Common.Library.Common.FileControl;

public class FileType(Guid id, string extension, string mimeType, string description, FileTypeExtensions fileType = FileTypeExtensions.UNKNOWN) : IFileType
{
    public Guid Id { get; init; } = id;
    public string Extension { get; init; } = extension;
    public string MimeType { get; init; } = mimeType;
    public string Description { get; init; } = description;
    public FileTypeExtensions Extensions { get; init; } = fileType;
}
