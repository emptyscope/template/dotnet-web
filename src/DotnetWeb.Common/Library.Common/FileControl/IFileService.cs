namespace DotnetWeb.Common.Library.Common.FileControl;

public interface IFileService
{
    Task<object?> Upload(byte []? bytes, Guid? fileId, string? fileName, FileType fileType, bool inBrowser, bool b1, bool b2);
}
