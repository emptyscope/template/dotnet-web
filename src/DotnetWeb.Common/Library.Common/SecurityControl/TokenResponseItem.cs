namespace DotnetWeb.Common.Library.Common.SecurityControl;

public record TokenResponseItem
{
    public string? TokenId { get; init; }
    public string? TokenType { get; init; }
    public DateTime Issued { get; init; }
    public DateTime Expires { get; init; }
}
