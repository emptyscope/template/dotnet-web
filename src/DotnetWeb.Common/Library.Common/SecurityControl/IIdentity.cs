namespace DotnetWeb.Common.Library.Common.SecurityControl;

public interface IIdentity
{
    string ApplicationId { get; set; }
    UserResponseItem User { get; set; }
    ClientResponseItem Client { get; set; }
    TokenResponseItem Token { get; set; }
    PermissionResponse Permissions { get; set; }

    bool HasPermission(string permissionName);

    bool UserExists();

    bool IsLocked();

    bool IsDisabled();

    bool IsDeleted();
}
