namespace DotnetWeb.Common.Library.Common.SecurityControl;

public record TokenResponse
{
    public IList<TokenResponseItem>? Responses { get; init; }
}
