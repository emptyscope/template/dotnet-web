namespace DotnetWeb.Common.Library.Common.SecurityControl;

public record PermissionResponseItem
{
    public string? PermissionName { get; init; }
    public string? Description { get; init; }
    public DateTime Created { get; init; }
    public bool? Deleted { get; init; }
}
