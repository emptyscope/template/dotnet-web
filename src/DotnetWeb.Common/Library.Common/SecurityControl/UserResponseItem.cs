namespace DotnetWeb.Common.Library.Common.SecurityControl;

public record UserResponseItem
{
    public string? Username { get; init; }
    public string? UserId { get; init; }
    public string? EmailAddress { get; init; }
    public bool? EmailVerified { get; init; }
    public bool? PasswordRequiresReset { get; init; }
    public bool? Disabled { get; init; }
    public bool? Deleted { get; init; }
    public bool? Locked { get; init; }
    public int FailedAttempts { get; init; }
    public DateTime? LastLoginAttempt { get; init; }
    public DateTime PasswordSetDate { get; init; }
}
