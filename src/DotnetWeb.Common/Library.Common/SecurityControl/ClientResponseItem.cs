namespace DotnetWeb.Common.Library.Common.SecurityControl;

public record ClientResponseItem
{
    public string? ClientName { get; init; }
    public string? ClientId { get; init; }
    public string? Domain { get; init; }
    public DateTime Created { get; init; }
    public bool? Deleted { get; init; }
}
