namespace DotnetWeb.Common.Library.Common.SecurityControl;

public record PermissionResponse
{
    public IList<PermissionResponseItem>? Responses { get; init; }
    public int Total { get; init; }
    public int Index { get; init; }
    public int PageSize { get; init; }
    public string? Next { get; init; }
    public string? Previous { get; init; }
}
