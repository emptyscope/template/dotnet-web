namespace DotnetWeb.Common.Library.Common.ReflectionControl;

public interface ITypeUtility
{
    IList<T> GetConstants<T>();
    IList<T> GetConstants<T>(Type? type);
    IList<T> GetEnums<T>();
    IList<T> GetEnums<T>(Type? type);
    T? ConvertObject<T>(object? input, Type? type);
}
