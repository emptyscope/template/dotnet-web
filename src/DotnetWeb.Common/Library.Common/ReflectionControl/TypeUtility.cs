using System.Globalization;
using System.Reflection;

namespace DotnetWeb.Common.Library.Common.ReflectionControl;

public class TypeUtility : ITypeUtility
{
    public IList<T> GetConstants<T>() => GetConstants<T>(typeof(T));

    public IList<T> GetConstants<T>(Type? type)
    {
        if (type == null)
        {
            return Array.Empty<T>();
        }

        var constants = type
            .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
            .Where(fi => fi is { IsLiteral: true, IsInitOnly: false } && fi.FieldType == typeof(T))
            .Where(x => x.GetRawConstantValue() != null);

        return constants
            .Select(x => (T) x.GetRawConstantValue()!)
            .ToList();
    }

    public IList<T> GetEnums<T>() => GetEnums<T>(typeof(T));

    public IList<T> GetEnums<T>(Type? type)
    {
        if (type == null)
        {
            return Array.Empty<T>();
        }

        var names = type.GetEnumNames();

        if (names.Length <= 0)
        {
            return Array.Empty<T>();
        }

        return names
            .Select(ConvertObject<T>)
            .Where(i => i != null)
            .Select(i => i!)
            .ToList();
    }

    public T? ConvertObject<T>(object? input, Type? type)
    {
        if (type == null)
        {
            return default;
        }

        if (type == typeof(T))
        {
            return CastObject<T>(input);
        }

        try
        {
            return ConvertObject<T>(input);
        }
        catch
        {
            return default;
        }
    }

    private static T? CastObject<T>(object? input)
    {
        if (input == null)
        {
            return default;
        }

        return (T) input;
    }

    private static T? ConvertObject<T>(object? input)
    {
        if (input == null)
        {
            return default;
        }

        return (T) Convert.ChangeType(input, typeof(T), CultureInfo.InvariantCulture);
    }
}
