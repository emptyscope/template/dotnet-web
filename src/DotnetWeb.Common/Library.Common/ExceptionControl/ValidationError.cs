using System.Text;

namespace DotnetWeb.Common.Library.Common.ExceptionControl;

public sealed class ValidationError
{
    private ValidationError(string? fieldName, string? description, string? subCode)
    {
        FieldName = fieldName;
        Description = description;
        SubCode = subCode;
    }

    public string? FieldName { get; }

    public string? Description { get; }

    public string? SubCode { get; }

    public override string ToString()
    {
        var stringBuilder = new StringBuilder();
        stringBuilder.Append("FieldName: ");
        stringBuilder.Append(FieldName);
        stringBuilder.Append(" Description: ");
        stringBuilder.Append(Description);
        stringBuilder.Append(" SubCode: ");
        stringBuilder.Append(SubCode);
        return stringBuilder.ToString();
    }

    public sealed class ValidationErrorBuilder
    {
        private ValidationError _validationError;

        public ValidationErrorBuilder() => _validationError = new ValidationError(null, null, null);

        public ValidationError Build() =>
            new(_validationError.FieldName, _validationError.Description,
                _validationError.SubCode);

        public ValidationErrorBuilder WithFieldName(string fieldName)
        {
            _validationError = new ValidationError(fieldName, _validationError.Description, _validationError.SubCode);
            return this;
        }

        public ValidationErrorBuilder WithDescription(string description)
        {
            _validationError = new ValidationError(_validationError.FieldName, description, _validationError.SubCode);
            return this;
        }

        public ValidationErrorBuilder WithSubCode(string subCode)
        {
            _validationError = new ValidationError(_validationError.FieldName, _validationError.Description, subCode);
            return this;
        }
    }
}
