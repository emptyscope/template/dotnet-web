using System.Net;

namespace DotnetWeb.Common.Library.Common.ExceptionControl;

public interface IExceptionValidation
{
    void Add(ValidationError? validationError);
    void Add(IEnumerable<ValidationError>? validationErrors);
    bool IsValid();
    ApiException Toss(HttpStatusCode httpStatusCode, string myId);
    ApiException Toss(HttpStatusCode httpStatusCode, string myId, string? clientMessageId);
    List<ValidationError> List();
}
