using System.Net;

namespace DotnetWeb.Common.Library.Common.ExceptionControl;

public class ExceptionValidation : IExceptionValidation
{
    private readonly List<ValidationError> _validationErrors;

    public ExceptionValidation() => _validationErrors = [];

    public void Add(ValidationError? validationError)
    {
        if (validationError != null)
        {
            _validationErrors.Add(validationError);
        }
    }

    public void Add(IEnumerable<ValidationError>? validationErrors)
    {
        if (validationErrors != null)
        {
            _validationErrors.AddRange(validationErrors);
        }
    }

    public bool IsValid() => _validationErrors.Count == 0;

    public List<ValidationError> List() => _validationErrors;

    public ApiException Toss(HttpStatusCode httpStatusCode, string myId) => new(httpStatusCode, myId, _validationErrors);

    public ApiException Toss(HttpStatusCode httpStatusCode, string myId, string? clientMessageId) => new(httpStatusCode, myId, _validationErrors, clientMessageId);
}
