using System.Net;
using System.Text;

namespace DotnetWeb.Common.Library.Common.ExceptionControl;

public class ApiException : Exception
{
    public ApiException(HttpStatusCode code, string myId, string? clientMessageId = null)
        : base(myId)
    {
        Code = code;
        MyId = myId;
        Errors = [];
        ClientMessageId = clientMessageId;
    }

    public ApiException(HttpStatusCode code, string myId, List<ValidationError> errors,
        string? clientMessageId = null)
        : base(ToCsv(myId, errors))
    {
        Code = code;
        MyId = myId;
        Errors = errors;
        ClientMessageId = clientMessageId;
    }

    public HttpStatusCode Code { get; }

    public string MyId { get; }

    public string? ClientMessageId { get; }

    public List<ValidationError> Errors { get; }

    private static string ToCsv(string myId, List<ValidationError> errors)
    {
        var stringBuilder = new StringBuilder(myId);
        stringBuilder.Append(string.Join(",", errors));
        return stringBuilder.ToString();
    }
}
