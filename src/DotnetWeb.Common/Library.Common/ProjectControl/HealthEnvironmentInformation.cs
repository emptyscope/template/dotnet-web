using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record HealthEnvironmentInformation
{
    public string? Url { get; init; }
    public Environments Environment { get; init; }
    public string? Name { get; init; }
}
