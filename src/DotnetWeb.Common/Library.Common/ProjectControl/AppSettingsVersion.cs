namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record AppSettingsVersion
{
    public string ApiVersion => $"{ApiMajorVersion}.{ApiMinorVersion}.{ApiBugVersion}";

    public int ApiMajorVersion { get; init; }
    public int ApiMinorVersion { get; init; }
    public int ApiBugVersion { get; init; }

    public string ProjectVersion => $"{ProjectMajorVersion}.{ProjectMinorVersion}";

    public int ProjectMajorVersion { get; init; }
    public int ProjectMinorVersion { get; init; }
    public string? GitCommitHash { get; init; }
}
