using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record AppSettingsCurrentEnvironment
{
    public string? Url { get; init; }
    public Environments Environment { get; init; }
    public string? Name { get; init; }
    public string? AuthenticationScheme { get; init; }
    public AppSettingsThirdParty? Authentication { get; init; }
    public AppSettingsUnleash? UnleashSettings { get; init; }
    public AppSettingsNetwork? NetworkSettings { get; init; }
}
