using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl;

public interface IProjectInformationMapper
{
    CurrentProjectInformation MapCurrent(DotnetWebAppSettings? appSettings, Tuple<int, int, int, string?> currentApiVersion,
        Tuple<int, int> currentProjectVersion,
        Environments environment = Environments.Unknown);

    HealthProjectInformation GetByApi(int version, HealthProjectInformation healthProjectInformation);
    HealthProjectInformation GetByProject(int version, HealthProjectInformation healthProjectInformation);
    HealthProjectInformation MapHealth(CurrentProjectInformation currentProjectInformation);
}
