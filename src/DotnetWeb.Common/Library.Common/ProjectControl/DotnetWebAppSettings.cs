namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record DotnetWebAppSettings
{
    public string? ApplicationName { get; init; }
    public AppSettingsVersion? VersionInformation { get; init; }
    public AppSettingsEnvironments? EnvironmentInformation { get; init; }
}


