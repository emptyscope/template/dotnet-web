namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record AppSettingsThirdParty
{
    public string? Audience { get; init; }
    public string? Issuer { get; init; }
    public string? Key { get; init; }
    public string? Domain { get; init; }
    public string? TenantId { get; init; }
    public string? ClientId { get; init; }
}
