namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record HealthProjectInformation
{
    public string? ApplicationName { get; init; }
    public AppSettingsVersion? VersionInformation { get; init; }
    public HealthEnvironmentInformation? CurrentEnvironment { get; init; }
}
