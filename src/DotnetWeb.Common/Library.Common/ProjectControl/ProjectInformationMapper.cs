using System.Net;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.ExceptionControl;

namespace DotnetWeb.Common.Library.Common.ProjectControl;

public class ProjectInformationMapper : IProjectInformationMapper
{
    public CurrentProjectInformation MapCurrent(DotnetWebAppSettings? appSettings,
        Tuple<int, int, int, string?> currentApiVersion, Tuple<int, int> currentProjectVersion,
        Environments environment = Environments.Unknown)
    {
        if (appSettings == null)
        {
            return DefaultCurrentProjectInformation();
        }

        return new CurrentProjectInformation
        {
            ApplicationName = appSettings.ApplicationName,
            VersionInformation = FetchVersionInformation(appSettings, currentApiVersion, currentProjectVersion),
            CurrentEnvironment = FetchEnvironment(appSettings.EnvironmentInformation, environment)
        };
    }

    public HealthProjectInformation GetByApi(int version, HealthProjectInformation healthProjectInformation)
    {
        var foundVersion = DotnetWebConstants.ApiList().FirstOrDefault(i => i.Item1.Equals(version));

        if (foundVersion == null)
        {
            return healthProjectInformation;
        }

        return new HealthProjectInformation
        {
            ApplicationName = healthProjectInformation.ApplicationName,
            VersionInformation =
                       FetchVersionInformation(null, foundVersion,
                           new Tuple<int, int>(healthProjectInformation.VersionInformation!.ProjectMajorVersion,
                               healthProjectInformation.VersionInformation.ProjectMinorVersion)),
            CurrentEnvironment = MapHealthCurrentEnvironment(healthProjectInformation.CurrentEnvironment)
        };
    }

    public HealthProjectInformation GetByProject(int version, HealthProjectInformation healthProjectInformation)
    {
        var foundVersion = DotnetWebConstants.ProjectList().FirstOrDefault(i => i.Item1.Equals(version));

        if (foundVersion == null)
        {
            return healthProjectInformation;
        }

        return new HealthProjectInformation
        {
            ApplicationName = healthProjectInformation.ApplicationName,
            VersionInformation =
                       FetchVersionInformation(null,
                           new Tuple<int, int, int, string?>(
                               healthProjectInformation.VersionInformation!.ApiMajorVersion
                               , healthProjectInformation.VersionInformation!.ApiMinorVersion
                               , healthProjectInformation.VersionInformation!.ApiBugVersion, null), foundVersion),
            CurrentEnvironment = MapHealthCurrentEnvironment(healthProjectInformation.CurrentEnvironment)
        };
    }

    public HealthProjectInformation MapHealth(CurrentProjectInformation? currentProjectInformation)
    {
        if (currentProjectInformation == null)
        {
            return DefaultHealthProjectInformation();
        }

        return new HealthProjectInformation
        {
            ApplicationName = currentProjectInformation.ApplicationName,
            VersionInformation = currentProjectInformation.VersionInformation,
            CurrentEnvironment = MapHealthCurrentEnvironment(currentProjectInformation.CurrentEnvironment)
        };
    }

    public HealthProjectInformation MapHealth(AppSettingsVersion? currentVersionInformation,
        HealthProjectInformation currentProjectInformation)
    {
        if (currentVersionInformation == null)
        {
            return DefaultHealthProjectInformation();
        }

        return new HealthProjectInformation
        {
            ApplicationName = currentProjectInformation.ApplicationName,
            VersionInformation = currentVersionInformation,
            CurrentEnvironment = MapHealthCurrentEnvironment(currentProjectInformation.CurrentEnvironment)
        };
    }

    private AppSettingsVersion FetchVersionInformation(DotnetWebAppSettings? appSettings,
        Tuple<int, int, int, string?> currentApiVersion, Tuple<int, int> currentProjectVersion) =>
        new()
        {
            ApiMajorVersion = currentApiVersion.Item1,
            ApiMinorVersion = currentApiVersion.Item2,
            ApiBugVersion = 0,
            GitCommitHash = appSettings?.VersionInformation?.GitCommitHash,
            ProjectMajorVersion = currentProjectVersion.Item1,
            ProjectMinorVersion = currentProjectVersion.Item2
        };


    private HealthProjectInformation DefaultHealthProjectInformation() =>
        new()
        {
            ApplicationName = DotnetWebConstants.Unknown,
            VersionInformation = DefaultCurrentVersionInformation(),
            CurrentEnvironment = DefaultHealthEnvironmentInformation()
        };

    private static CurrentProjectInformation DefaultCurrentProjectInformation() =>
        new()
        {
            ApplicationName = DotnetWebConstants.Unknown,
            VersionInformation = DefaultCurrentVersionInformation(),
            CurrentEnvironment = new AppSettingsCurrentEnvironment
            {
                Url = DotnetWebConstants.EnvironmentApiUrl,
                Environment = Environments.Unknown,
                Name = Enum.GetName(Environments.Unknown),
                Authentication = new AppSettingsThirdParty(),
                UnleashSettings = new AppSettingsUnleash(),
                NetworkSettings = new AppSettingsNetwork()
            }
        };

    private HealthEnvironmentInformation MapHealthCurrentEnvironment(AppSettingsCurrentEnvironment? currentEnvironment)
    {
        if (currentEnvironment == null)
        {
            return DefaultHealthEnvironmentInformation();
        }

        return new HealthEnvironmentInformation
        {
            Url = currentEnvironment.Url,
            Environment = currentEnvironment.Environment,
            Name = Enum.GetName(currentEnvironment.Environment)
        };
    }


    private HealthEnvironmentInformation MapHealthCurrentEnvironment(HealthEnvironmentInformation? currentEnvironment)
    {
        if (currentEnvironment == null)
        {
            return DefaultHealthEnvironmentInformation();
        }

        return new HealthEnvironmentInformation
        {
            Url = currentEnvironment.Url,
            Environment = currentEnvironment.Environment,
            Name = Enum.GetName(currentEnvironment.Environment)
        };
    }

    private HealthEnvironmentInformation DefaultHealthEnvironmentInformation() =>
        new()
        {
            Url = null,
            Environment = Environments.Unknown,
            Name = Enum.GetName(Environments.Unknown)
        };

    private static AppSettingsVersion DefaultCurrentVersionInformation() =>
        new()
        {
            ApiMajorVersion = 0,
            ApiMinorVersion = 0,
            ApiBugVersion = 0,
            ProjectMajorVersion = 0,
            ProjectMinorVersion = 0,
            GitCommitHash = "0000000000000000000000000000000000000000"
        };

    private AppSettingsCurrentEnvironment FetchEnvironment(AppSettingsEnvironments? environmentInformation,
        Environments environment) => environment switch
        {
            Environments.Production => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Production?.Url,
                Environment = Environments.Production,
                Name = Enum.GetName(Environments.Production),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Production),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Staging => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Staging?.Url,
                Environment = Environments.Staging,
                Name = Enum.GetName(Environments.Staging),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Staging),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Uat => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Uat?.Url,
                Environment = Environments.Uat,
                Name = Enum.GetName(Environments.Uat),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Uat),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Demo => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Demo?.Url,
                Environment = Environments.Demo,
                Name = Enum.GetName(Environments.Demo),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Demo),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Qa => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Qa?.Url,
                Environment = Environments.Qa,
                Name = Enum.GetName(Environments.Qa),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Qa),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Development => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Development?.Url,
                Environment = Environments.Development,
                Name = Enum.GetName(Environments.Development),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Development),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Local => new AppSettingsCurrentEnvironment
            {
                Url = environmentInformation?.Local?.Url,
                Environment = Environments.Local,
                Name = Enum.GetName(Environments.Local),
                Authentication = FetchThirdPartyInformation(environmentInformation?.Local),
                UnleashSettings = FetchUnleashSettings(),
                NetworkSettings = FetchNetworkSettings()
            },
            Environments.Unknown => throw new ApiException(HttpStatusCode.InternalServerError, Guid.Empty.ToString(), "Environment Unknown"),
            Environments.Build => throw new NotImplementedException(),
            _ => throw new ApiException(HttpStatusCode.InternalServerError, Guid.Empty.ToString(), "Environment Unknown"),
        };

    private static AppSettingsThirdParty FetchThirdPartyInformation(
        AppSettingsCurrentEnvironment? environmentInformation) =>
        new()
        {
            Audience = environmentInformation?.Authentication?.Audience,
            ClientId = environmentInformation?.Authentication?.ClientId,
            Domain = environmentInformation?.Authentication?.Domain,
            Issuer = environmentInformation?.Authentication?.Issuer,
            Key = environmentInformation?.Authentication?.Key,
            TenantId = environmentInformation?.Authentication?.TenantId
        };

    private static AppSettingsUnleash FetchUnleashSettings() => new();

    private static AppSettingsNetwork FetchNetworkSettings() => new();
}
