namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record AppSettingsEnvironments
{
    public AppSettingsCurrentEnvironment? Unknown { get; init; }
    public AppSettingsCurrentEnvironment? Production { get; init; }
    public AppSettingsCurrentEnvironment? Staging { get; init; }
    public AppSettingsCurrentEnvironment? Uat { get; init; }
    public AppSettingsCurrentEnvironment? Demo { get; init; }
    public AppSettingsCurrentEnvironment? Qa { get; init; }
    public AppSettingsCurrentEnvironment? Development { get; init; }
    public AppSettingsCurrentEnvironment? Local { get; init; }
}
