namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record AppSettingsNetwork
{
    public List<string>? KnownProxies { get; init; }
}
