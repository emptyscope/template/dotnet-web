namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record CurrentProjectInformation
{
    public string? ApplicationName { get; init; }
    public AppSettingsVersion? VersionInformation { get; init; }
    public AppSettingsCurrentEnvironment? CurrentEnvironment { get; init; }
}
