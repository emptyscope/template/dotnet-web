namespace DotnetWeb.Common.Library.Common.ProjectControl;

public record AppSettingsUnleash
{
    public string? ApiUrl { get; init; }
    public string? InstanceId { get; init; }
}
