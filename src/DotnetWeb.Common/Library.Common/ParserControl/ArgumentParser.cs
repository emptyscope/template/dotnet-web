namespace DotnetWeb.Common.Library.Common.ParserControl;

public class ArgumentParser : IArgumentParser
{
    public bool CanParseBody(string arg, out IList<string> val)
    {
        if (string.IsNullOrWhiteSpace(arg))
        {
            val = [];
            return false;
        }

        return CanParseBody([arg], out val);
    }

    public bool CanParseBody(IList<string>? args, out IList<string> val)
    {
        try
        {
            if (args != null && args.Any())
            {
                val = args;
                return true;
            }

            val = [];
            return false;
        }
        catch (Exception)
        {
            val = [];
            return false;
        }
    }

    public bool CanParseBody(Stream? request, out IList<string?> val)
    {
        try
        {
            if (request == null)
            {
                val = [];
                return false;
            }

            var requestBody = new StreamReader(request).ReadToEnd();
            if (!string.IsNullOrWhiteSpace(requestBody))
            {
                var escaped = System.Text.RegularExpressions.Regex.Unescape(requestBody);
                val =
                      [
                          escaped
                      ];
                return true;
            }

            val = [];
            return false;
        }
        catch (Exception)
        {
            val = [];
            return false;
        }
    }

    public bool Contains(string? arg, IList<string> val)
    {
        if (string.IsNullOrWhiteSpace(arg))
        {
            return false;
        }

        return val.Any(i => i.Equals(arg, StringComparison.OrdinalIgnoreCase));
    }
}
