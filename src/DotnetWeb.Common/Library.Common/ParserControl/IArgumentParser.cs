namespace DotnetWeb.Common.Library.Common.ParserControl;

public interface IArgumentParser
{
    bool Contains(string arg, IList<string> val);
    bool CanParseBody(string arg, out IList<string> val);
    bool CanParseBody(IList<string> args, out IList<string> val);
    bool CanParseBody(Stream? request, out IList<string?> val);
}
