using Microsoft.Extensions.Logging;

namespace DotnetWeb.Common.Library.Common.LogControl;

public static class DotnetWebLogEvents
{
    internal static EventId UpdateEventId = new(Update, "Update");
    internal static EventId CreateEventId = new(Create, "Create");
    internal static EventId DeleteEventId = new(Delete, "Delete");
    internal static EventId ReadEventId = new(Read, "Read");
    internal static EventId UpdateNotFoundEventId = new(UpdateNotFound, "UpdateNotFound");
    internal static EventId CreateNotFoundEventId = new(CreateNotFound, "CreateNotFound");
    internal static EventId DeleteNotFoundEventId = new(DeleteNotFound, "DeleteNotFound");
    internal static EventId ReadNotFoundEventId = new(ReadNotFound, "ReadNotFound");
    internal static EventId BeforeRequestEventId = new(BeforeRequest, "BeforeRequest");
    internal static EventId BeforePipelineEventId = new(BeforePipeline, "BeforePipeline");
    internal static EventId AfterPipelineEventId = new(AfterPipeline, "AfterPipeline");
    internal static EventId AfterRequestEventId = new(AfterRequest, "AfterRequest");
    internal static EventId ErrorEventId = new(Error, "Error");

    public const int Update = 2000;
    public const int Create = 2010;
    public const int Delete = 2020;
    public const int Read = 2040;
    public const int UpdateNotFound = 4040;
    public const int CreateNotFound = 4040;
    public const int DeleteNotFound = 4040;
    public const int ReadNotFound = 4040;
    public const int BeforeRequest = 1000;
    public const int BeforePipeline = 1020;
    public const int AfterPipeline = 1021;
    public const int AfterRequest = 1001;
    public const int Error = 5000;
}
