namespace DotnetWeb.Common.Library.Common.LogControl;

public interface ILogLocationUtility
{
    string Fetch(string defaultLogLocation, string? logName = DotnetWebConstants.ApplicationName,
        string defaultLogLocationEnvironmentVariableName = DotnetWebConstants.EnvironmentVariableLogLocation);
}
