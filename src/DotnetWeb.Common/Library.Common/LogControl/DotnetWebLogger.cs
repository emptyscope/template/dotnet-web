using Microsoft.Extensions.Logging;

namespace DotnetWeb.Common.Library.Common.LogControl;

public static partial class DotnetWebLogger
{
    [LoggerMessage(EventId = DotnetWebLogEvents.Read, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Hit by userId: {UserId}")]
    public static partial void Hit(
        this ILogger logger,
        Guid? traceId,
        Guid? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.Update, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Hit by userId: {UserId}| message: {Message}| ")]
    public static partial void Hit(
        this ILogger logger,
        string? message,
        string? traceId,
        string? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.Update, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Hit by userId: {UserId}| message: {Message}| ")]
    public static partial void Hit(
        this ILogger logger,
        string? message,
        Guid? traceId,
        Guid? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.Update, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Failure by userId: {UserId}| message: {Message}| ")]
    public static partial void Hit(
        this ILogger logger,
        Exception? exception,
        string? message,
        Guid? traceId,
        Guid? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.Create, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Failure by userId: {UserId}| message: {Message}| ")]
    public static partial void Hit(
        this ILogger logger,
        Exception? exception,
        string? message,
        string? traceId,
        Guid? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.Delete, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Failure by userId: {UserId}| message: {Message}| ")]
    public static partial void Hit(
        this ILogger logger,
        Exception? exception,
        string? message,
        Guid? traceId,
        string? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.ReadNotFound, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Hit by userId: {UserId}")]
    public static partial void Hit(
        this ILogger logger,
        string? traceId,
        string? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.CreateNotFound, Level = LogLevel.Information, Message = "|traceId: {TraceId}| Failure by userId: {UserId}| message: {Message}| ")]
    public static partial void Hit(
        this ILogger logger,
        Exception? exception,
        string? message,
        string? traceId,
        string? userId);

    [LoggerMessage(EventId = DotnetWebLogEvents.Error, Level = LogLevel.Error, Message = "|traceId: {TraceId}| Failure by userId: {UserId}| message: {Message}| ")]
    public static partial void Fail(
        this ILogger logger,
        Exception? exception,
        string? message,
        Guid? traceId = null,
        Guid? userId = null);
}
