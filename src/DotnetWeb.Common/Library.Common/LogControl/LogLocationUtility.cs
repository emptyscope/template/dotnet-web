using DotnetWeb.Common.Library.Common.EnvironmentControl;

namespace DotnetWeb.Common.Library.Common.LogControl;

public class LogLocationUtility(IEnvironmentVariableUtility environmentVariableUtility) : ILogLocationUtility
{
    public string Fetch(string defaultLogLocation, string? logName = DotnetWebConstants.ApplicationName,
        string defaultLogLocationEnvironmentVariableName = DotnetWebConstants.EnvironmentVariableLogLocation)
    {
        if (string.IsNullOrWhiteSpace(defaultLogLocation))
        {
            throw new ArgumentNullException(defaultLogLocation, "Missing Default Log Location");
        }

        logName ??= DotnetWebConstants.ApplicationName;

        var location =
            environmentVariableUtility.SafeFetch(defaultLogLocationEnvironmentVariableName, defaultLogLocation) ??
            defaultLogLocation;

        var logNameWithExtension =
            logName!.EndsWith(".log", StringComparison.OrdinalIgnoreCase) ? logName : logName + ".log";

        try
        {
            return Path.Combine(GetRoot().FullName, Path.Combine(location, logNameWithExtension));
        }
        catch (Exception ex)
        {
            return logNameWithExtension;
        }
    }

    private static DirectoryInfo GetRoot(string? anchorFile = null)
    {
        var directory = new DirectoryInfo(Directory.GetCurrentDirectory());

        while (directory != null && directory.GetFiles(anchorFile ?? "README.md").Length == 0)
        {
            directory = directory.Parent;
        }

        return directory ?? throw new InvalidOperationException();
    }
}
