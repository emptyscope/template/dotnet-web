using System.Text;

namespace DotnetWeb.Common.Library.Common.HashControl;

public class Base64Utility : IBase64Utility
{
    public bool IsBase64Encoded(string val)
    {
        try
        {
            _ = Convert.FromBase64String(val);
            return val.Replace(" ", "").Length % 4 == 0;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public string Base64Encode(string val)
    {
        var plainTextBytes = Encoding.UTF8.GetBytes(val);
        return Convert.ToBase64String(plainTextBytes);
    }

    public string Base64Decode(string val)
    {
        var base64EncodedBytes = Convert.FromBase64String(val);
        return Encoding.UTF8.GetString(base64EncodedBytes);
    }
}
