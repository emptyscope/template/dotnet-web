namespace DotnetWeb.Common;

public record GettingStarted
{
    public string? Value { get; init; }
}
