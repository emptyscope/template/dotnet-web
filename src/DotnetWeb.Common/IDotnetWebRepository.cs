using DotnetWeb.Model;

namespace DotnetWeb.Common;

public interface IDotnetWebRepository
{
    Task<DotnetWebResponse?> Fetch(Guid id);
    Task<IEnumerable<DotnetWebResponse>> List();
}
