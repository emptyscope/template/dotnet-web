namespace DotnetWeb.Common;

public static class DotnetWebConstants
{
    public const string? ApplicationName = "DOTNETWEB";
    public const string EnvironmentVariableName = "ENVIRONMENT";
    public const string ApplicationEnvironmentVariable = $"{ApplicationName}_{EnvironmentVariableName}";
    private const string Dotnet = "DOTNET";
    private const string DotnetPrefix = $"{Dotnet}_";
    private const string AspnetCore = "ASPNETCORE";
    private const string AspnetCorePrefix = $"{AspnetCore}_";
    private const string EnvironmentVariablePrefix = $"{ApplicationName}_";

    public const string AspnetCoreEnvironmentVariableName = $"{AspnetCorePrefix}{EnvironmentVariableName}";
    public const string DotnetEnvironmentVariableName = $"{DotnetPrefix}{EnvironmentVariableName}";
    public const string DebuggerLaunchVariableName = $"{DotnetPrefix}DEBUGGER_LAUNCH";
    public const string CustomSettingsSectionName = "CurrentProjectInformation";
    public const string DefaultLogLocation = $"output/logs/{ApplicationName}/";

    public const string LogEnvironmentVariableLocation = $"{EnvironmentVariablePrefix}LOG_LOCATION";
    public const string DatabaseEnvironmentVariableUrl = $"{EnvironmentVariablePrefix}DATABASE_URL";
    public const string DatabaseEnvironmentVariableUsername = $"{EnvironmentVariablePrefix}DATABASE_USERNAME";
    public const string DatabaseEnvironmentVariablePassword = $"{EnvironmentVariablePrefix}DATABASE_PASSWORD";
    public const string DatabaseEnvironmentVariableCatalog = $"{EnvironmentVariablePrefix}DATABASE_CATALOG";

    public const string EnvironmentVariableLogLocation = $"{EnvironmentVariablePrefix}LOG_LOCATION";
    public const string EnvironmentApiUrl = $"{EnvironmentVariablePrefix}API_URL";
    public const string EnvironmentAuthApiUrl = $"{EnvironmentVariablePrefix}AUTH_API_URL";
    public const string EnvironmentPortalUrl = $"{EnvironmentVariablePrefix}PORTAL_URL";
    public const string EnvironmentAuthPortalUrl = $"{EnvironmentVariablePrefix}AUTH_PORTAL_URL";
    public const string EnvironmentQueue = $"{EnvironmentVariablePrefix}QUEUE";

    public static readonly string [] HttpOptions = ["GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"];

    public const string Unknown = "Unknown";
    public const string Production = "Production";
    public const string Staging = "Staging";
    public const string Uat = "UAT";
    public const string Demo = "Demo";
    public const string Qa = "QA";
    public const string Development = "Development";
    public const string Build = "Build";
    public const string Local = "Local";

    public const int DefaultPageSize = 100;
    public const int DefaultMinimumPageSize = 1;
    public const int DefaultMaximumPageSize = 1000;

    public const int DefaultPageIndex = 1;
    public const int DefaultMinimumPageIndex = 1;
    public const int DefaultMaximumPageIndex = 1000;

    private const int LatestProjectMajorVersion1 = 1;
    private const int LatestProjectMinorVersion1 = 0;

    private const int LatestApiMajorVersion1 = 1;
    private const int LatestApiMinorVersion1 = 0;
    private const int LatestApiBugVersion1 = 0;

    private const int LatestApiMajorVersion2 = 2;
    private const int LatestApiMinorVersion2 = 0;
    private const int LatestApiBugVersion2 = 0;

    public const string HttpStatusUrl = "https://httpstatuses.io";
    public const string ProxyIpAddress = "127.0.10.1";


    public static readonly Tuple<int, int>
        ProjectVersion1 = new(LatestProjectMajorVersion1, LatestProjectMinorVersion1);

    public static readonly Tuple<int, int, int> ApiVersion1 = new(LatestApiMajorVersion1, LatestApiMinorVersion1,
        LatestApiBugVersion1);

    public static readonly Tuple<int, int, int> ApiVersion2 = new(LatestApiMajorVersion1, LatestApiMinorVersion1,
        LatestApiBugVersion1);

    public static List<Tuple<int, int>> ProjectList() => [new Tuple<int, int>(LatestProjectMajorVersion1, LatestProjectMinorVersion1)];

    public static List<Tuple<int, int, int, string?>> ApiList() =>
    [
        new Tuple<int, int, int, string?>(LatestApiMajorVersion1, LatestApiMinorVersion1, LatestApiBugVersion1,
            null),
        new Tuple<int, int, int, string?>(LatestApiMajorVersion2, LatestApiMinorVersion2, LatestApiBugVersion2,
            null)
    ];

    public static Tuple<int, int, int, string?> CurrentApiVersion() =>
        new(LatestApiMajorVersion2, LatestApiMinorVersion2, LatestApiBugVersion2,
            null);

    public static Tuple<int, int> CurrentProjectVersion() => new(LatestProjectMajorVersion1, LatestProjectMinorVersion1);
}
