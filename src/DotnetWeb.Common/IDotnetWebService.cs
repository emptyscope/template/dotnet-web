using DotnetWeb.Model;

namespace DotnetWeb.Common;

public interface IDotnetWebService
{
    Task<DotnetWebResponse?> Fetch(Guid id);
    Task<Guid> Create(DotnetWebRequest request);
    Task<IList<DotnetWebResponse>> List();
}
