using System.Net;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.FileControl;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Library.Route;

/// <inheritdoc />
[ApiConventionType(typeof(DefaultApiConventions))]
[ApiController]
[Route("api/v{apiVersion}/[controller]")]
public abstract class BaseApiController : Controller
{
    /// <summary>
    /// Handles the OPTIONS request method and returns an HTTP response message.
    /// </summary>
    /// <returns>An HTTP response message with status code OK.</returns>
    [HttpOptions]
    public HttpResponseMessage Options([FromRoute] string? apiVersion)
    {
        var response = new HttpResponseMessage(HttpStatusCode.OK)
        {
            Content = new StringContent(string.Empty)
        };
        response.Content.Headers.Add("Allow", DotnetWebConstants.HttpOptions);
        response.Content.Headers.ContentType = null;

        return response;
    }

    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected virtual IActionResult ForbidWithModel<T>() =>
        Forbid(ModelWithHeaders(new AuthenticationProperties
        {
            IsPersistent = false,
            RedirectUri = null,
            IssuedUtc = DateTimeOffset.UtcNow,
            ExpiresUtc = DateTimeOffset.UtcNow,
            AllowRefresh = false
        }));

    /// <summary>
    ///
    /// </summary>
    /// <param name="model"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    protected static T ModelWithHeaders<T>(T model) => model;

    /// <summary>
    ///
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    protected static async Task<byte []?> UploadedBytes(IFormFile? file)
    {
        if (file == null)
        {
            return null;
        }

        await using (var fileStream = file.OpenReadStream())
        {
            var bytes = new byte [file.Length];
            var fileBytesRead = await fileStream.ReadAsync(bytes.AsMemory(0, (int) file.Length)).ConfigureAwait(false);

            if (fileBytesRead > 0)
            {
                return bytes;
            }
        }

        return null;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="fileName"></param>
    /// <param name="inBrowser"></param>
    /// <param name="mimeType"></param>
    /// <returns></returns>
    protected FileStreamResult? ReturnFile(Stream? stream, string? fileName = null, bool inBrowser = false, string? mimeType = null)
    {
        if (stream == null)
        {
            return null;
        }

        var contentType = ContentType(fileName, mimeType);

        if (!inBrowser)
        {
            return new FileStreamResult(stream, contentType) { FileDownloadName = fileName };
        }

        var file = File(stream, contentType);
        file.FileDownloadName = fileName;
        return file;
    }

    private static string ContentType(string? fileName, string? mimeType)
    {
        if (string.IsNullOrWhiteSpace(mimeType) && !string.IsNullOrWhiteSpace(fileName))
        {
            return FileTypes.Instance().ByExtension(fileName).MimeType;
        }

        return string.IsNullOrWhiteSpace(mimeType) ? FileTypes.Instance().ByEnum(FileTypeExtensions.UNKNOWN).MimeType : mimeType;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="item"></param>
    /// <param name="defaultMin"></param>
    /// <param name="defaultMax"></param>
    /// <returns></returns>
    protected virtual int FixPaging(int? item, int defaultMin = DotnetWebConstants.DefaultMinimumPageSize, int defaultMax = DotnetWebConstants.DefaultPageSize) => FixMinimum(FixMaximum(item, defaultMax), defaultMin);

    /// <summary>
    ///
    /// </summary>
    /// <param name="item"></param>
    /// <param name="defaultMin"></param>
    /// <param name="defaultMax"></param>
    /// <returns></returns>
    protected virtual int FixIndex(int? item, int defaultMin = DotnetWebConstants.DefaultMinimumPageIndex, int defaultMax = DotnetWebConstants.DefaultPageIndex) => FixMaximum(FixMinimum(item, defaultMin), defaultMax);

    private static int FixMinimum(int? item, int defaultMin)
    {
        if (item.HasValue)
        {
            return item.Value < defaultMin ? defaultMin : item.Value;
        }

        if (defaultMin <= 0)
        {
            return 1;
        }

        return item!.Value < defaultMin ? defaultMin : item.Value;
    }

    private static int FixMaximum(int? item, int defaultMax)
    {
        if (item.HasValue)
        {
            return item.Value > defaultMax ? defaultMax : item.Value;
        }

        return defaultMax switch
        {
            <= 0 => DotnetWebConstants.DefaultMinimumPageSize,
            > DotnetWebConstants.DefaultMaximumPageSize => DotnetWebConstants.DefaultPageSize,
            _ => item!.Value > defaultMax ? defaultMax : item.Value,
        };
    }
}
