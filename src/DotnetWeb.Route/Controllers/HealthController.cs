using System.Net.Mime;
using Asp.Versioning;
using DotnetWeb.Common.Library.Common.ProjectControl;
using DotnetWeb.Route.Library.Route;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Controllers;

/// <summary>
/// The <see cref="HealthController"/> class represents a controller for handling health check requests.
/// </summary>
/// <remarks>
/// Constructs a new instance of the HealthController class.
/// </remarks>
/// <param name="healthProjectInformation">The implementation of the IHealthProjectInformation interface that provides access to health project information.</param>
[ApiVersion(1.0)]
[ApiVersion(2.0, "latest")]
[ApiVersionNeutral]
public class HealthController(HealthProjectInformation healthProjectInformation) : BaseApiController
{
    private readonly HealthProjectInformation _healthProjectInformation = healthProjectInformation;

    /// <summary>
    /// Health Check Endpoint.
    /// </summary>
    /// <returns>Returns a success statement</returns>
    /// <response code="200">All Good</response>
    [ApiVersion(1.0)]
    [MapToApiVersion(1)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpGet]
    [AllowAnonymous]
    public IActionResult GetV1() => Ok(_healthProjectInformation);
}
