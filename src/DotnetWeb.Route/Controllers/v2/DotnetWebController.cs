using System.Net.Mime;
using Asp.Versioning;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Model;
using DotnetWeb.Route.Library.Route;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Controllers.v2;

/// DotnetWebController Class
/// <p>
/// This class is a controller class that handles HTTP requests related to the DotnetWeb API.
/// It provides CRUD operations for managing items. </p> <p>
/// To use this controller, you need to inject an instance of IDotnetWebService and ILogger into its constructor.
/// The IDotnetWebService interface provides methods for fetching, creating, updating, and deleting items.
/// The ILogger interface provides logging functionality. </p> <p>
/// This controller is versioned using the [ApiVersion] attribute with version 2.0 and "latest" as the version label. </p>
/// /
/// <summary>
/// Represents a controller for the DotnetWeb application.
/// </summary>
/// <param name="logger">An instance of the ILogger used for logging messages.</param>
/// <param name="service">An instance of the IDotnetWebService used for accessing the application's web service.</param>
[ApiVersion(2.0, "latest")]
[HttpLogging(HttpLoggingFields.All)]
public class DotnetWebController(ILogger<DotnetWebController> logger, IDotnetWebService service) : BaseApiController
{
    private readonly IDotnetWebService _service = service;
    private readonly ILogger<DotnetWebController> _logger = logger;

    /// <summary>
    /// Lists all Items.
    /// </summary>
    /// <returns>A list of all items</returns>
    /// <response code="200">Returns the item list</response>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpGet]
    [Authorize]
    public async Task<IActionResult> GetV2()
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        var model = await _service.List().ConfigureAwait(false);
        return Ok(model);
    }

    /// <summary>
    /// Fetches a specific Item.
    /// </summary>
    /// <param name="id"></param>
    /// <returns>A newly created item</returns>
    /// <response code="200">Returns the item that was created</response>
    /// <response code="400">If the id format was invalid</response>
    /// <response code="404">If the item was not found</response>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpGet("{id:guid}")]
    [Authorize]
    public async Task<IActionResult> GetV2([FromRoute] Guid id)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        var model = await _service.Fetch(id).ConfigureAwait(false);
        if (model != null)
        {
            return Ok(model);
        }

        return NotFound();
    }

    /// <summary>
    /// Creates an item.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     {
    ///        "name": "Item1"
    ///     }
    ///
    /// </remarks>
    /// <param name="item"></param>
    /// <returns>A success message</returns>
    /// <response code="201">A message that the item has been created</response>
    /// <response code="400">If any format or value was invalid</response>
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpPost]
    [Authorize]
    public IActionResult PostV2([FromBody] DotnetWebRequest item)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        return Created($"{Request.GetDisplayUrl()}/{item}", item);
    }

    /// <summary>
    /// Update an item.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     {
    ///        "id": 1,
    ///        "name": "Item1"
    ///     }
    ///
    /// </remarks>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns>A success message</returns>
    /// <response code="200">A message that the item has been updated</response>
    /// <response code="400">If any format or value was invalid</response>
    /// <response code="404">If the item was not found</response>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpPut("{id:guid}")]
    [Authorize]
    public IActionResult PutV2([FromRoute] Guid id, [FromBody] DotnetWebRequest item)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        return Accepted();
    }

    /// <summary>
    /// Delete an item.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     {
    ///        "id": 1
    ///     }
    ///
    /// </remarks>
    /// <param name="id"></param>
    /// <returns>A success message</returns>
    /// <response code="202">A message that the delete request has been accepted</response>
    /// <response code="404">If the item was not found</response>
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpDelete("{id:guid}")]
    [Authorize]
    public IActionResult DeleteV2([FromRoute] Guid id)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        return Accepted();
    }
}
