using System.Net.Mime;
using Asp.Versioning;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.FileControl;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Model;
using DotnetWeb.Route.Library.Route;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Controllers.v1;

/// <summary>
/// This class represents a DotnetWebController.
/// </summary>
/// <remarks>
/// Represents a controller for a .NET web service.
/// </remarks>
/// <param name="logger">An instance of the ILogger interface for logging.</param>
/// <param name="service">An instance of the IDotnetWebService interface.</param>
/// <param name="fileService">An instance of the IFileService interface.</param>
[ApiVersion(1.0, "latest")]
[HttpLogging(HttpLoggingFields.All)]
public class FileController(ILogger<DotnetWebController> logger, IDotnetWebService service, IFileService fileService) : BaseApiController
{
    private readonly IDotnetWebService _service = service;
    private readonly IFileService _fileService = fileService;
    private readonly ILogger<DotnetWebController> _logger = logger;

    /// <summary>
    /// Retrieves the version 1 representation of the resource.
    /// </summary>
    /// <returns>An <see cref="IActionResult"/> that represents the result of the action.</returns>
    /// <response code="200">Returns the list of resources.</response>
    /// <remarks>
    /// This method requires that the user is authenticated.
    /// </remarks>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpGet]
    [Authorize]
    public async Task<IActionResult> ListV1()
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        var model = await _service.List().ConfigureAwait(false);
        return Ok(model);
    }

    /// <summary>
    /// Retrieve a resource by its unique identifier. Requires authorization.
    /// </summary>
    /// <param name="id">The unique identifier of the resource.</param>
    /// <returns>
    /// An <see cref="IActionResult"/> object representing the result of the operation.
    /// Returns <see cref="StatusCodes.Status200OK"/> if the resource is found and successfully retrieved.
    /// Returns <see cref="StatusCodes.Status400BadRequest"/> if the request is malformed.
    /// Returns <see cref="StatusCodes.Status404NotFound"/> with an error message if the resource is not found.
    /// </returns>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpGet("{id:guid}")]
    [Authorize]
    public async Task<IActionResult> GetV1([FromRoute] Guid id)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        var model = await _service.Fetch(id).ConfigureAwait(false);
        if (model != null)
        {
            return Ok(model);
        }

        return NotFound();
    }

    /// <summary>
    /// Creates an item.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     {
    ///        "name": "Item1"
    ///     }
    ///
    /// </remarks>
    /// <param name="formFile"></param>
    /// <param name="fileName"></param>
    /// <param name="inBrowser"></param>
    /// <param name="mimeType"></param>
    /// <returns>A success message</returns>
    /// <response code="201">A message that the item has been created</response>
    /// <response code="400">If any format or value was invalid</response>
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpPost]
    [Authorize]
    public async Task<IActionResult> PostV1(IFormFile? formFile, [FromQuery] string? fileName = null, [FromQuery] bool? inBrowser = false, [FromQuery] string? mimeType = null)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        if (formFile == null)
        {
            return BadRequest();
        }

        var fileType = FileTypes.Instance().Fetch(FileTypes.Instance().ByMimeType(mimeType));

        var bytes = await UploadedBytes(formFile).ConfigureAwait(false);

        Guid? fileId = string.IsNullOrWhiteSpace(fileName) ? Guid.NewGuid() : null;
        var document = await _fileService.Upload(bytes, fileId, fileName,
            fileType, false, false, false)
            .ConfigureAwait(false);

        var model = ModelWithHeaders(document);

        return Created($"{Request.GetDisplayUrl()}/{fileId}", model);
    }

    /// <summary>
    /// Update an item.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     {
    ///        "id": 1,
    ///        "name": "Item1"
    ///     }
    ///
    /// </remarks>
    /// <param name="id"></param>
    /// <param name="item"></param>
    /// <returns>A success message</returns>
    /// <response code="200">A message that the item has been updated</response>
    /// <response code="400">If any format or value was invalid</response>
    /// <response code="404">If the item was not found</response>
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpPut("{id:guid}")]
    [Authorize]
    public IActionResult PutV1([FromRoute] Guid id, [FromBody] DotnetWebRequest item)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        return Accepted();
    }

    /// <summary>
    /// Delete an item.
    /// </summary>
    /// <param name="id"></param>
    /// <returns>A success message</returns>
    /// <response code="202">A message that the delete request has been accepted</response>
    /// <response code="404">If the item was not found</response>
    [ProducesResponseType(StatusCodes.Status202Accepted)]
    [ProducesResponseType(typeof(IDictionary<string, string>), StatusCodes.Status404NotFound)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpDelete("{id:guid}")]
    [Authorize]
    public IActionResult DeleteV1([FromRoute] Guid id)
    {
        _logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        return Accepted();
    }
}
