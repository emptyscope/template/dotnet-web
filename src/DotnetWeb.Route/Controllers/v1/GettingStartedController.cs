using System.Diagnostics.CodeAnalysis;
using System.Net.Mime;
using Asp.Versioning;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Model;
using DotnetWeb.Route.Library.Route;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.AspNetCore.Mvc;

namespace DotnetWeb.Route.Controllers.v1;

/// <summary>
/// This class represents a DotnetWebController.
/// </summary>
[ApiVersion(1.0, "latest")]
[HttpLogging(HttpLoggingFields.All)]
public class GettingStartedController(ILogger<GettingStartedController> logger)
    : BaseApiController, IConsumer<GettingStarted>
{
    /// <summary>
    /// Creates an item.
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     {
    ///        "name": "Item1"
    ///     }
    ///
    /// </remarks>
    /// <param name="item"></param>
    /// <returns>A success message</returns>
    /// <response code="201">A message that the item has been created</response>
    /// <response code="400">If any format or value was invalid</response>
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [Produces(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [Consumes(MediaTypeNames.Application.Json, MediaTypeNames.Application.Xml, MediaTypeNames.Text.Xml)]
    [HttpPost]
    [Authorize]
    public IActionResult PostV1([FromBody] DotnetWebRequest item)
    {
        logger.Hit(Request.HttpContext.TraceIdentifier, HttpContext.User.Identity?.Name);

        return Created($"{Request.GetDisplayUrl()}/{item}", item);
    }

    /// <summary>
    /// Processes the consumed message of type GettingStarted.
    /// </summary>
    /// <param name="context">The context of the consumed message, containing the message and relevant metadata.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    [SuppressMessage("ReSharper", "ConditionalAccessQualifierIsNonNullableAccordingToAPIContract")]
    public Task Consume(ConsumeContext<GettingStarted> context)
    {
        logger.Hit($"Received Text: {context.Message.Value}", Request?.HttpContext?.TraceIdentifier, HttpContext?.User?.Identity?.Name);
        return Task.CompletedTask;
    }
}
