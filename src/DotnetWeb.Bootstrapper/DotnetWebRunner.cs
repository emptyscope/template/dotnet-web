using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.ParserControl;

namespace DotnetWeb.Bootstrapper;

public class DotnetWebRunner(IDotnetWebService service, IArgumentParser argumentParser) : IRunner
{
    public async Task Run(IList<string> args)
    {
        if (!argumentParser.CanParseBody(args, out var parsed))
        {
            return;
        }

        foreach (var str in parsed)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                continue;
            }

            if (Guid.TryParse(str, out var id))
            {
                await service.Fetch(id).ConfigureAwait(false);
            }
        }
    }
}
