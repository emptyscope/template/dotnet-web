using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common.Library.Common.LogControl;
using DotnetWeb.Common.Library.Common.ReflectionControl;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;

#pragma warning disable 1998

namespace DotnetWeb.Bootstrapper;

public interface IPipeline
{
    public bool DoBeforePipeline(string? path, string method, IDictionary<string, StringValues>? headers);
    public bool DoAfterPipeline(string? path, string method, IDictionary<string, StringValues>? headers);
    public Task BeforeRequest(IDictionary<string, StringValues>? headers, string? body);
    public Task AfterRequest(IDictionary<string, StringValues>? headers, string? body);
}

public class Pipeline(ILogger<Pipeline> logger, ITypeUtility typeUtility) : BasePipeline(typeUtility), IPipeline
{
    public bool DoBeforePipeline(string? path, string? method, IDictionary<string, StringValues>? headers)
    {
        if (string.IsNullOrWhiteSpace(path) || string.IsNullOrWhiteSpace(method))
        {
            return false;
        }

        return IsAcceptableMethod(method)
            && IsAcceptableBeforePath(path)
            && IsAcceptableFileTypeExtension(path)
            && IsAcceptableContentType(GetHeader("Content-Type", headers))
            && IsAcceptableAccept(GetHeader("Accept", headers));
    }

    public bool DoAfterPipeline(string? path, string? method, IDictionary<string, StringValues>? headers)
    {
        if (string.IsNullOrWhiteSpace(path) || string.IsNullOrWhiteSpace(method))
        {
            return false;
        }

        return IsAcceptableMethod(method)
            && IsAcceptableAfterPath(path)
            && IsAcceptableFileTypeExtension(path)
            && IsAcceptableContentType(GetHeader("Content-Type", headers));
    }

    public async Task BeforeRequest(IDictionary<string, StringValues>? headers, string? body)
    {
        logger.Hit(Guid.Empty, Guid.NewGuid());

        if (!string.IsNullOrWhiteSpace(body))
        {
            //TODO: do stuff
        }
    }

    public async Task AfterRequest(IDictionary<string, StringValues>? headers, string? body)
    {
        LoggerMessage.Define(LogLevel.Information, DotnetWebLogEvents.AfterRequest, $"After request {DateTime.UtcNow}");

        if (!string.IsNullOrWhiteSpace(body))
        {
            //TODO: do stuff
        }
    }
}
