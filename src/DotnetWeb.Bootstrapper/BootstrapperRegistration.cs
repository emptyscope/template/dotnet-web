using System.Text;
using Autofac;
using DotnetWeb.Bootstrapper.Library.Bootstrapper;
using DotnetWeb.Common;
using DotnetWeb.Common.Library.Common.CacheControl;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using DotnetWeb.Common.Library.Common.ParserControl;
using DotnetWeb.Common.Library.Common.ProjectControl;
using DotnetWeb.Core;
using DotnetWeb.Repository;
using DotnetWeb.Repository.Library.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.Bootstrapper;

public class BootstrapperRegistration(
    IEnvironmentVariableUtility environmentVariableUtility,
    IConfiguration configuration)
    : IBootstrapperRegistrations
{
    public ContainerBuilder RegisterAutofacDi(ContainerBuilder builder)
    {
        builder.RegisterModule(new SqlAzureAutofacModule(GetSqlAzureRead(), GetSqlAzureWrite()));
        builder.RegisterModule(new DotnetWebAutofacModule());
        return builder;
    }

    public IServiceCollection RegisterMicrosoftDi(IServiceCollection services)
    {
        services.AddSingleton<IEnvironmentVariableUtility>(new EnvironmentVariableUtility());
        services.AddSingleton<IInstanceCache>(new InstanceCache());
        services.AddSingleton<IArgumentParser>(new ArgumentParser());
        services.AddSingleton<IConsoleRunner, ConsoleRunner>();
        services.AddSingleton<IRunner, DotnetWebRunner>();
        services.AddSingleton<IPipeline, Pipeline>();
        services.AddSingleton<IProjectInformationMapper, ProjectInformationMapper>();
        services.AddTransient<IDotnetWebRepository, DotnetWebRepository>();
        services.AddTransient<IDotnetWebService, DotnetWebService>();

        return services;
    }

    public string GetEnvironment()
    {
        var nonConfigurationEnvironments = ValidateEnvironment(new List<string>
            {
                environmentVariableUtility.SafeFetch(DotnetWebConstants.AspnetCoreEnvironmentVariableName) ?? string.Empty
            }
            .Where(i => !string.IsNullOrWhiteSpace(i))
            .Select(i => i.ToUpperInvariant().Trim())
            .Distinct());


        var cliEnvironments = ValidateEnvironment(new List<string>
            {
                configuration.GetValue<string>(DotnetWebConstants
                    .EnvironmentVariableName) ?? string.Empty,
                configuration.GetValue<string>(DotnetWebConstants
                    .DotnetEnvironmentVariableName) ?? string.Empty,
            }
            .Where(i => !string.IsNullOrWhiteSpace(i))
            .Select(i => i.ToUpperInvariant().Trim())
            .Distinct());

        var configurationEnvironments = ValidateEnvironment(new List<string>
            {
                configuration.GetValue<string>(DotnetWebConstants
                    .DotnetEnvironmentVariableName) ?? string.Empty,
                configuration.GetValue<string>(DotnetWebConstants
                    .ApplicationEnvironmentVariable) ?? string.Empty,
                configuration.GetValue<string>(DotnetWebConstants
                    .AspnetCoreEnvironmentVariableName) ?? string.Empty
            }
            .Where(i => !string.IsNullOrWhiteSpace(i))
            .Select(i => i.ToUpperInvariant().Trim())
            .Distinct());

        var environments = ValidateEnvironment(new List<string>
            {
                nonConfigurationEnvironments.Value,
                cliEnvironments.Value,
                configurationEnvironments.Value
            }
            .Where(i => !string.IsNullOrWhiteSpace(i))
            .Select(i => i.ToUpperInvariant().Trim())
            .Distinct());

        return environments.Key switch
        {
            <= 0 => throw new ArgumentException(@"Missing Environment", nameof(configuration)),
            1 => environments.Value,
            _ => throw new ArgumentException(@"Multiple Environments", nameof(configuration)),
        };
    }

    private static KeyValuePair<int, string> ValidateEnvironment(IEnumerable<string>? request)
    {
        if (request == null)
        {
            return new KeyValuePair<int, string>(0, string.Empty);
        }

        var environments = request.ToList();
        switch (environments.Count)
        {
            case <= 0:
                return new KeyValuePair<int, string>(0, string.Empty);
            case 1:
                return new KeyValuePair<int, string>(1, environments.FirstOrDefault() ?? string.Empty);
            default:
            {
                var conflictingEnvironments = environments
                    .Select(i => environments.Where(x => !i.Equals(x, StringComparison.OrdinalIgnoreCase)))
                    .ToList();

                return conflictingEnvironments.Count > 0
                    ? new KeyValuePair<int, string>(conflictingEnvironments.Count,
                        new StringBuilder(conflictingEnvironments.Count).AppendJoin("| ", environments).ToString())
                    : new KeyValuePair<int, string>(1, environments.FirstOrDefault() ?? string.Empty);
            }
        }
    }

    private SqlAzureConnection GetSqlAzureRead() =>
        new(
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariableUrl, ""),
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariableUsername, ""),
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariablePassword, ""),
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariableCatalog, ""));

    private SqlAzureConnection GetSqlAzureWrite() =>
        new(
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariableUrl, ""),
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariableUsername, ""),
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariablePassword, ""),
            environmentVariableUtility.SafeFetch(DotnetWebConstants.DatabaseEnvironmentVariableCatalog, ""), false);
}
