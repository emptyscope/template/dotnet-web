using DotnetWeb.Common.Library.Common.FileControl;
using DotnetWeb.Common.Library.Common.ReflectionControl;
using Microsoft.Extensions.Primitives;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper;

public abstract class BasePipeline(ITypeUtility typeUtility)
{
    protected virtual List<string> AllowedHttpMethodList() =>
    [
        "get",
        "post",
        "put",
        "delete",
        "patch"
    ];

    protected virtual List<string> AllowedHttpContentTypeList() =>
    [
        "html",
        "json",
        "xml"
    ];

    protected virtual List<string> AllowedHttpAcceptList() =>
    [
        "application/json",
        "application/xml",
        "text/xml"
    ];

    protected virtual List<string> AllowedBeforeHttpPathList() =>
    [
        "health",
        "login",
        "ping",
        "index",
        "authorize",
        "swagger"
    ];

    protected virtual List<string> AllowedAfterHttpPathList() => ["*"];

    protected virtual List<string> AllowedHttpFileTypeExtensionList() =>
        [.. typeUtility.GetEnums<string>(typeof(FileTypeExtensions))];

    protected virtual bool IsAcceptableMethod(string httpMethod) => !string.IsNullOrWhiteSpace(httpMethod) && AllowedHttpMethodList().Contains(httpMethod.ToLowerInvariant());

    protected virtual bool IsAcceptableContentType(string? contentType)
    {
        if (string.IsNullOrWhiteSpace(contentType))
        {
            return true;
        }

        return !FirstParamContainsSecond(contentType, AllowedHttpContentTypeList());
    }

    protected virtual bool IsAcceptableAccept(string? accept) => string.IsNullOrWhiteSpace(accept) || FirstParamContainsSecond(AllowedHttpAcceptList(), accept);

    protected virtual bool IsAcceptableBeforePath(string? path)
    {
        if (string.IsNullOrWhiteSpace(path))
        {
            return true;
        }

        if (AllowedAfterHttpPathList().Contains("*"))
        {
            return true;
        }

        return !FirstParamContainsSecond(path, AllowedBeforeHttpPathList());
    }

    protected virtual bool IsAcceptableAfterPath(string? path)
    {
        if (string.IsNullOrWhiteSpace(path))
        {
            return true;
        }

        if (AllowedAfterHttpPathList().Contains("*"))
        {
            return true;
        }

        return !FirstParamContainsSecond(path, AllowedAfterHttpPathList());
    }

    protected virtual bool IsAcceptableFileTypeExtension(string? path)
    {
        if (string.IsNullOrWhiteSpace(path))
        {
            return true;
        }

        return !FirstParamContainsSecond(path, AllowedHttpFileTypeExtensionList());
    }

    protected virtual bool FirstParamContainsSecond(string? val, List<string> list)
    {
        if (string.IsNullOrWhiteSpace(val))
        {
            return false;
        }

        var fixedVal = val.Trim().ToLowerInvariant();
        return list.FirstOrDefault(i => fixedVal.Contains(i, StringComparison.InvariantCultureIgnoreCase)) != null;
    }

    protected virtual bool FirstParamContainsSecond(List<string> list, string? val)
    {
        if (string.IsNullOrWhiteSpace(val))
        {
            return false;
        }

        var fixedVal = val.Trim().ToLowerInvariant();
        return list.FirstOrDefault(i => i.Contains(fixedVal, StringComparison.InvariantCultureIgnoreCase)) != null;
    }

    protected string? GetHeader(string? key, IDictionary<string, StringValues>? headers)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            return null;
        }

        if (headers == null || !headers.ContainsKey(key))
        {
            return null;
        }

        return !headers.TryGetValue(key, out var val) ? null : val.FirstOrDefault(i => !string.IsNullOrWhiteSpace(i));
    }
}
