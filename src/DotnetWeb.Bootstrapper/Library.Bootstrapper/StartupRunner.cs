using Autofac;
using DotnetWeb.Common.Library.Common.EnvironmentControl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper;

public class StartupRunner(IEnvironmentVariableUtility environmentVariableUtility, IConfiguration configuration)
{
    private readonly BootstrapperRegistration _bootstrapper = new(environmentVariableUtility, configuration);

    public void Run(IList<string> args, ContainerBuilder containerBuilder)
    {
        _bootstrapper.RegisterMicrosoftDi(new ServiceCollection());
        var builder = _bootstrapper.RegisterAutofacDi(containerBuilder);
        Run(args, builder.Build());
    }

    public void Run(IList<string> args, IServiceCollection services)
    {
        _bootstrapper.RegisterMicrosoftDi(services);
        var builder = _bootstrapper.RegisterAutofacDi(new ContainerBuilder());
        Run(args, builder.Build());
    }

    private static void Run(IList<string> args, IContainer container)
    {
        using (var scope = container.BeginLifetimeScope())
        {
            RunLoop(args, scope);
        }
    }

    private static void RunLoop(IList<string> args, IComponentContext scope)
    {
        var runners = scope.Resolve<IEnumerable<IRunner>>();

        if (runners == null)
        {
            throw new MissingMemberException(nameof(runners), "No runners registered");
        }

        var list = runners.ToList();

        if (list.Count == 0)
        {
            throw new MissingMemberException(nameof(list), "No runners implemented");
        }

        var tasks = list
            .Select(runner => runner.Run(args))
            .ToList();

        Task.WaitAll([.. tasks]);
    }
}
