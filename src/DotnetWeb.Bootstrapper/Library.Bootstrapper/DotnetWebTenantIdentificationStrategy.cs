using Autofac.Multitenant;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper;

public class DotnetWebTenantIdentificationStrategy : ITenantIdentificationStrategy
{
    public bool TryIdentifyTenant(out object? tenantId)
    {
        tenantId = null;
        try
        {
            return tenantId != null;
        }
        catch (Exception)
        {
            return false;
        }
    }
}
