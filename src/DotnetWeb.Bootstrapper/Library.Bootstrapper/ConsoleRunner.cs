using DotnetWeb.Common.Library.Common.LogControl;
using Microsoft.Extensions.Logging;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper;

public class ConsoleRunner(ILogger<ConsoleRunner> logger) : IConsoleRunner
{
    public async Task Run(IList<string> args, IRunner runner)
    {
        try
        {
            await runner.Run(args).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            logger.Fail(e, "An error occurred while running the application");
        }
    }
}
