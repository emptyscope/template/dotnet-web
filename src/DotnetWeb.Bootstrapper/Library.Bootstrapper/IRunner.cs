namespace DotnetWeb.Bootstrapper.Library.Bootstrapper;

public interface IRunner
{
    Task Run(IList<string> args);
}
