using Autofac;
using Microsoft.Extensions.DependencyInjection;

namespace DotnetWeb.Bootstrapper.Library.Bootstrapper;

public interface IBootstrapperRegistrations
{
    ContainerBuilder RegisterAutofacDi(ContainerBuilder builder);
    IServiceCollection RegisterMicrosoftDi(IServiceCollection services);

}
