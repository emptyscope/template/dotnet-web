using Autofac;
using DotnetWeb.Common.Library.Common.DatabaseControl;
using DotnetWeb.Repository.Library.Repository;

namespace DotnetWeb.Bootstrapper;

public class SqlAzureAutofacModule(SqlAzureConnection read, SqlAzureConnection write) : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<SqlAzureConnection>()
            .As<IConnection>()
            .SingleInstance()
            .Named<IConnection>("sql-read")
            .WithParameter("connection", read);

        builder.RegisterType<SqlAzureConnection>()
            .As<IConnection>()
            .SingleInstance()
            .Named<IConnection>("sql-write")
            .WithParameter("connection", write);
    }
}
