Dotnet-Web Template
====================

[![Gitlab Pipeline Status](https://img.shields.io/gitlab/pipeline-status/emptyscope/template/dotnet-web?style=flat-square&logo=gitlab)](https://gitlab.com/emptyscope/template/dotnet-web/-/pipelines)
[![GitLab Release](https://img.shields.io/gitlab/v/release/emptyscope/template/dotnet-web?style=flat-square&logo=gitlab)](https://gitlab.com/emptyscope/template/dotnet-web/-/releases)
[![Gitlab Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/emptyscope/template/dotnet-web?style=flat-square&logo=gitlab)](https://gitlab.com/emptyscope/template/dotnet-web/-/quality/test_cases)
[![GitLab Issues](https://img.shields.io/gitlab/issues/open/emptyscope/template/dotnet-web?style=flat-square&logo=gitlab)](https://gitlab.com/emptyscope/template/dotnet-web/-/issues)
[![GitLab Merge Requests](https://img.shields.io/gitlab/merge-requests/open/emptyscope/template/dotnet-web?style=flat-square&logo=gitlab)](https://gitlab.com/emptyscope/template/dotnet-web/-/merge_requests)

[![NuGet Version](https://img.shields.io/nuget/v/Emptyscope.DotnetWeb?style=social&logo=nuget)](https://www.nuget.org/packages/Emptyscope.DotnetWeb)

[![GitLab License](https://img.shields.io/gitlab/license/emptyscope/template/dotnet-web?style=flat-square&logo=unlicense&logoColor=white)](https://gitlab.com/emptyscope/template/dotnet-web/-/blob/8ef6d50f8afaa05540bd61ba5118df17b9650077/LICENSE.md)
[![Discord](https://img.shields.io/discord/1177307257881514037?style=social&logo=discord&logoColor=7289DA&color=7289DA&label=discord)](https://discord.gg/wEfGuBzE)
[![X (formerly Twitter) Follow](https://img.shields.io/twitter/follow/mptyscope?style=social&label=@mptyscope)](https://x.com/mptyscope)

Dotnet-Web Template is an open-source C# starter template.

## Goals

1. [x] Provide a simple, clean, and easy-to-understand template for new projects.
2. [x] Use the latest .NET technologies and best practices.
3. [x] Abstract away common boilerplate code, configurations, and hosting environments.
4. [x] Be used during automation and code generation.
    * Use's replacement tokens for project name, namespace, etc.
5. [x] Provide common integrations, patterns, and tools.
6. [x] Be testable and follow test-driven development (TDD) principles.

## Get started

## How to engage, contribute, and give feedback

## Reporting security issues and bugs

## Related projects

## Code of conduct
