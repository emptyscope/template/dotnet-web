#!/usr/bin/env sh

. ./build_functions.sh
# Uncomment below lines if needed
# . <(curl -s https://example.com/script.sh)
# curl --remote-name $GITLAB_URL_PREFIX/$GITLAB_TEMPLATE_PREFIX/REPLACE_EXAMPLE_GITLAB_FOLDERNAME/-/raw/main/$SUB_FOLDERNAME/setup.sh

# Command Options
CommandOptions="
-c,--config <string:FILE>:Use the given configuration file instead of parameters
-l,--list:List all program related files. If used with '--verbose', the full contents are printed
-d,--debug <int:PORT>:Run application and attach debugger
-r,--run <int:PORT>:Run application
-v,--verbose:Turn on verbose output
-n,--dry-run:Don't actually do anything, only show what would be done
-h,--help:Display help
"

# Parse options using getopts (POSIX compliant)
while getopts "c:e:" option; do
    case "$option" in
        c) ConfigurationOption="$OPTARG" ;;
        e) EnvironmentNameOption="$OPTARG" ;;
        *) echo "Invalid option"; exit 1 ;;
    esac
done

# Default Values
if [ -z "$EnvironmentNameOption" ]; then
    EnvironmentName="PRODUCTION"
    ASPNETCORE_ENVIRONMENT="PRODUCTION"
else
    EnvironmentName="$EnvironmentNameOption"
    ASPNETCORE_ENVIRONMENT="$EnvironmentNameOption"
fi

if [ -z "$ConfigurationOption" ]; then
    ConfigurationName="$EnvironmentName"
else
    ConfigurationName="$ConfigurationOption"
fi

# Output environment details
clear
echo "EnvironmentName: $EnvironmentName"
echo "ASPNETCORE_ENVIRONMENT: $ASPNETCORE_ENVIRONMENT"
echo "Configuration: $ConfigurationName"

# Directory cleanup
echo "Removing output directory..."
rm -rf output

echo "Removing input directory..."
rm -rf input

echo "Removing bin directories..."
rm -rf src/**/bin
rm -rf src/hosts/**/bin

echo "Removing Model git directories..."
rm -rf src/DotnetWeb.Model/.git

#cd src/DotnetWeb.Model
#git init

# If needed, include NuGet cache clearing commands here.

# Check Runtime Configuration (replace $( ) with backticks for POSIX compliance)
RuntimeConfiguration=`get_runtime_configuration`

if [ "$RuntimeConfiguration" = "Release" ]; then
    echo "Restoring NuGet packages with Lock File..."
    dotnet restore --configfile nuget.config --use-lock-file --locked-mode
else
    echo "Restoring NuGet packages..."
    dotnet restore --configfile nuget.config --force-evaluate
fi

if [ "$RuntimeConfiguration" != "Release" ]; then
    echo "Formatting project..."
    dotnet format --no-restore --report output/format/
fi

Platform=`get_platform`
echo "Building project... for environment: $EnvironmentName ... with configuration: $RuntimeConfiguration ... on platform: $Platform ..."
dotnet build . --configuration "$RuntimeConfiguration" --verbosity minimal --property:TreatWarningsAsErrors=false --property:consoleloggerparameters=Summary --property:EnvironmentName="$EnvironmentName" --no-restore --nologo
get_success "Building project..."

dotnet test . --logger "html;logfilename=testResults.html" --property:EnvironmentName="$EnvironmentName" --no-build

echo "Publishing host projects..."
echo "----------------------------"

dotnet publish src/hosts/DotnetWeb.AzureFunction/DotnetWeb.AzureFunction.csproj --configuration $( get_runtime_configuration ) --property:EnvironmentName=$EnvironmentName --output output/publish/AzureFunction
get_success "Publishing Azure Function..."

dotnet publish src/hosts/DotnetWeb.ConsoleApplication/DotnetWeb.ConsoleApplication.csproj --configuration $( get_runtime_configuration ) --property:EnvironmentName=$EnvironmentName --output output/publish/ConsoleApplication
get_success "Publishing Console Application..."

dotnet publish src/hosts/DotnetWeb.WebApi/DotnetWeb.WebApi.csproj --configuration $( get_runtime_configuration ) --property:EnvironmentName=$EnvironmentName --output output/publish/WebApi
get_success "Publishing WebApi..."

dotnet pack src/DotnetWeb.Model/DotnetWeb.Model.csproj --configuration $( get_runtime_configuration ) --no-build --nologo --property:EnvironmentName=$EnvironmentName --output output/nuget/
get_success "Packing project..."

if [[ $( get_runtime_configuration ) == "Release" ]]; then
  mkdir -p input/nuget/
  echo $NUGET_SIGNING_CERTIFICATE >> input/nuget/signing-certificate.pfx

#  dotnet nuget sign output/nuget/*.nupkg --certificate-path input/nuget/signing-certificate.pfx -certificate-password $NUGET_SIGNING_PASSWORD --overwrite --output output/nuget/signed/
#  get_success "Creating signed NuGet packages..."

#  rm -r input;

  dotnet nuget verify output/nuget/*.nupkg  --configfile nuget.config
  get_success "Verifying the signed NuGet packages..."
fi

get_display_stats
get_success "Project!"

