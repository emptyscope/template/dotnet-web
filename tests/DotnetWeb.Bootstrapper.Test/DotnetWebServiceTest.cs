using DotnetWeb.Common;
using DotnetWeb.Core;
using DotnetWeb.Model;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;

namespace DotnetWeb.Bootstrapper.Test;

[TestFixture]
public class DotnetWebServiceTest
{
    private DotnetWebService _service;
    private IDotnetWebRepository _repository;
    private ILogger<DotnetWebService> _logger;
    private readonly Random _random;

    public DotnetWebServiceTest()
    {
        _repository = Substitute.For<IDotnetWebRepository>();
        _logger = Substitute.For<ILogger<DotnetWebService>>();
        _service = new DotnetWebService(_repository, _logger);
        _random = new Random();
    }

    [SetUp]
    public void Setup()
    {
        _repository = Substitute.For<IDotnetWebRepository>();
        _logger = Substitute.For<ILogger<DotnetWebService>>();
        _service = new DotnetWebService(_repository, _logger);
    }

    [Test]
    public void GetWithValidInputReturnsValue()
    {
        //arrange
        var id = Guid.NewGuid();
        var expected = new DotnetWebResponse
        {
            Id = id,
            Code = _random.Next(1, 100),
            Item1 = id.ToString()
        };

        _repository.Fetch(id)!.Returns(Task.FromResult(expected));

        //act
        var result = _service.Fetch(id).Result;

        //assert
        _repository.Received(1).Fetch(id);
        Assert.That(result, Is.SameAs(expected));
    }
}
