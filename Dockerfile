﻿#begin
FROM --platform=$BUILDPLATFORM mcr.microsoft.com/dotnet/sdk:9.0-alpine AS build
ARG BUILD_CONFIGURATION=Release
ARG ASPNETCORE_ENVIRONMENT=Production
WORKDIR /source

# Copy source code and publish app
COPY . .
RUN ls -a
RUN /bin/sh build.sh -e $ASPNETCORE_ENVIRONMENT -c $BUILD_CONFIGURATION

FROM mcr.microsoft.com/dotnet/aspnet:9.0-alpine
EXPOSE 5000
WORKDIR /app
COPY --from=build /output/publish/WebApi .
#COPY --link --from=build /output/publish/WebApi .
USER $APP_UID
ENTRYPOINT ["dotnet", "DotnetWeb.WebApi.dll"]
