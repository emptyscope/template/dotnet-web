#!/usr/bin/env sh

console_message() {
    messageOption="$1"
    echo "${messageOption}"

    return 0
}

find_pid() {
    portOption="$1"
    lsof -i tcp:"${portOption}" | awk 'NR!=1 {print $2}'

    return 0
}

kill_port() {
    portOption="$1"
    lsof -i tcp:"${portOption}" | awk 'NR!=1 {print $2}' | xargs kill

    return 0
}

rider_attach_process() {
    portOption="$1"
    slnDirectoryOption="$2"

    find_pid "${portOption}" | xargs rider attach-to-process "${slnDirectoryOption}"
    lsof -i tcp:"${portOption}" | awk 'NR!=1 {print $2}' | xargs kill

    return 0
}

file_exists() {
    fileOption="$1"
    echo "$fileOption"
    if [ -f "$fileOption" ]; then
        return 0
    else
        return 1
    fi
}

get_success() {
    if [ "$?" -eq 0 ]; then
        echo "----------------------------"
        echo "----------------------------"
        echo ""
        echo "Success: $?"
        echo ""
        return 0
    else
        echo "----------------------------"
        echo "----------------------------"
        get_display_stats
        echo "----------------------------"
        echo "----------------------------"
        echo ""
        echo "Failure: $?"
        echo ""
        exit 1
    fi
}

get_platform() {
    case "${OSTYPE}" in
        solaris*) echo "SOLARIS" ;;
        darwin*) echo "OSX" ;;
        linux*) echo "LINUX" ;;
        bsd*) echo "BSD" ;;
        msys*) echo "WINDOWS" ;;
        cygwin*) echo "ALSO WINDOWS" ;;
        *) echo "UNKNOWN: $OSTYPE" ;;
    esac

    get_success
    return "$?"
}

get_display_stats() {
    echo ""
    echo "Environment: ${EnvironmentName}"
    echo "Configuration: ${ConfigurationName}"
    echo "ASPNETCORE_ENVIRONMENT: ${ASPNETCORE_ENVIRONMENT}"
    echo "Configuration: $(get_runtime_configuration)"
    echo "Dotnet: $(dotnet --version)"
    echo "OS: $(get_platform)"
    echo "Architecture: $(get_arch)"
    echo "Branch: $(git rev-parse --abbrev-ref HEAD)"
    echo "Hash: $(git rev-parse --verify HEAD)"
    echo "Date: $(date)"
    echo ""
}

get_arch() {
    uname -m

    get_success
    return "$?"
}

get_runtime_configuration() {
    if [ -z "${EnvironmentName}" ]; then
        echo 'Release'
        return 0
    fi

    case "${EnvironmentName}" in
        LOCAL|BUILD|DEVELOPMENT|QA)
            echo 'Debug' ;;
        *)
            echo 'Release'
    esac

    return 0
}

usage() {
    local options_offset=5
    local descriptions_offset_after_longest_option=5
    local maximum_descriptions_length=120

    echo "Usage: $(basename "$0") [OPTION]..."
    local max_option_length=1
    for option in "${CommandOptions[@]}"; do
        if [ "${#option}" -gt "$max_option_length" ]; then
            max_option_length="${#option}"
        fi
    done
    local descriptions_new_line_offset=$(($max_option_length + $options_offset + $descriptions_offset_after_longest_option))
}
